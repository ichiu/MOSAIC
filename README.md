

=================================================
MOSAIC Matrix-element Oriented SAmplIng Calculator
=================================================

:authors: Masahiro Morinaga
:contact: Masahiro.Morinaga@cern.ch

.. contents:: Table of Contents

------------
Introduction
------------

This tool is for reconstruct an invariant mass of a pair of tau leptons using the Matrix-Element and the transfar functions.
This tool can also reconstruct a four momentum of missing particles, i.e. neutrinos from a tau lepton decay.

This tool can be used, include the header file::

  #include "MOSAIC/MOSAICalculator.h"

or if you are using ``xAOD`` style code, the::

  #include "MOSAIC/xMOSAICalculator.h"

and calling::

  MOSAICalculator * m_mosaic;

or ::

  xMOSAICalculator * m_mosaic;


