// Dear emacs, this is -*- c++ -*- 
/*
 * TauObj 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#include "MOSAIC/TauObj.h"

#ifndef MOSAIC_TAUOBJ_C
#define MOSAIC_TAUOBJ_C

Mosaic::TauObj::TauObj() : FourVector(), panTau(), tauRec() {
  decayType             = Mosaic::ParticleType::None;
  particleType          = Mosaic::ParticleType::None;
  helisityAngle         = 0;
  chargedParticles      = new std::vector< Mosaic::FourVector* >;
  neutralParticles      = new std::vector< Mosaic::FourVector* >;
  neutralOtherParticles = new std::vector< Mosaic::FourVector* >;
}

Mosaic::TauObj::~TauObj(){
  this->Clear();
  delete chargedParticles;
  delete neutralParticles;
  delete neutralOtherParticles;
}
void Mosaic::TauObj::Clear(){
  this->FourVector::clear();
  decayType    = Mosaic::ParticleType::None;
  particleType = Mosaic::ParticleType::None;
  helisityAngle = 0;

  panTau.clear();
  tauRec.clear();

  for(auto& charged : *chargedParticles)      delete charged;
  for(auto& neutral : *neutralParticles)      delete neutral;
  for(auto& neutral : *neutralOtherParticles) delete neutral;
  chargedParticles->clear();
  neutralParticles->clear();
  neutralOtherParticles->clear();
}

bool Mosaic::TauObj::init(std::mt19937& Engine, std::uniform_real_distribution<double>& Uniform){
  bool isOKCharged = false;
  bool isOKNeutral = false;
  if( decayType == Mosaic::ParticleType::None ){
    if( chargedParticles->size() == 1) {
      if( neutralParticles->size() == 0) decayType = Mosaic::ParticleType::mode_1p0n;
      if( neutralParticles->size() == 1) decayType = Mosaic::ParticleType::mode_1p1n;
      if( neutralParticles->size() >= 2) decayType = Mosaic::ParticleType::mode_1pXn;
    }
    if( chargedParticles->size() == 3) {
      if( neutralParticles->size() == 0) decayType = Mosaic::ParticleType::mode_3p0n;
      if( neutralParticles->size() >= 1) decayType = Mosaic::ParticleType::mode_3pXn;
    }
  }
  if( decayType == Mosaic::ParticleType::mode_3p0n || decayType == Mosaic::ParticleType::mode_3pXn ){
    if( chargedParticles->size() == 3) isOKCharged = true;
    particleType = Mosaic::ParticleType::p_a1;
  }else if( decayType == Mosaic::ParticleType::mode_1p0n || decayType == Mosaic::ParticleType::mode_1p1n || decayType == Mosaic::ParticleType::mode_1pXn || decayType == Mosaic::ParticleType::p_Electron || decayType == Mosaic::ParticleType::p_Muon ){
    if( chargedParticles->size() == 1) isOKCharged = true;
    if     ( decayType == Mosaic::ParticleType::mode_1p0n ) particleType = Mosaic::ParticleType::p_Pion;
    else if( decayType == Mosaic::ParticleType::mode_1p1n ) particleType = Mosaic::ParticleType::p_Rho;
    else if( decayType == Mosaic::ParticleType::mode_1pXn ) particleType = Mosaic::ParticleType::p_a1;
    else particleType = decayType;
  }
  std::sort( neutralParticles->begin(),      neutralParticles->end(),      [](const FourVector* n1, const FourVector* n2 ) -> bool { return n1->score() > n2->score() ; }  );
  std::sort( neutralOtherParticles->begin(), neutralOtherParticles->end(), [](const FourVector* n1, const FourVector* n2 ) -> bool { return n1->score() > n2->score() ; }  );
  if( decayType == Mosaic::ParticleType::mode_1p0n || decayType == Mosaic::ParticleType::mode_3p0n || decayType == Mosaic::ParticleType::p_Muon || decayType == Mosaic::ParticleType::p_Electron ){
    isOKNeutral = true;
  }else if(decayType == Mosaic::ParticleType::mode_1p1n || decayType == Mosaic::ParticleType::mode_3pXn ){
    if(neutralParticles->size() == 1){
      isOKNeutral = true;
    }else if(neutralParticles->size() >= 2){
      isOKNeutral = true;
      auto n1 = neutralParticles->at(0);
      auto n2 = neutralParticles->at(1);
      for(auto& neutral : *neutralParticles)      delete neutral;
      neutralParticles->clear();
      
      neutralParticles->emplace_back( new Mosaic::FourVector( n1->px() + n2->px(), n1->py() + n2->py(), n1->pz() + n2->pz(), n1->e() + n2->e(), Mosaic::Constant::MPI, 0.0, std::max(n1->score(), n2->score()) ) );
    }else if(neutralParticles->size() == 0){
      if( neutralOtherParticles->size() > 0){
        isOKNeutral = true;
        neutralParticles->emplace_back( new Mosaic::FourVector( *(neutralOtherParticles->at(0)) ) ); 
      }
    }
  }else if( decayType == Mosaic::ParticleType::mode_1pXn ){
    if(neutralParticles->size() >= 2){
      isOKNeutral = true;
    }else if( neutralParticles->size() == 1){
      if( neutralOtherParticles->size() > 0){
        for(auto& neutral : *(neutralOtherParticles) ){
          if( neutralParticles->at(0)->score() != neutral->score() ){
            isOKNeutral = true;
            neutralParticles->emplace_back( new Mosaic::FourVector(*neutral) );
            break;
          }
        }
      }
    }else if( neutralParticles->size() == 0 ){
      if( neutralOtherParticles->size() > 1){
        isOKNeutral = true;
        neutralParticles->emplace_back( new Mosaic::FourVector( *(neutralOtherParticles->at(0)) ) );
        neutralParticles->emplace_back( new Mosaic::FourVector( *(neutralOtherParticles->at(1)) ) );
      }
    }
  }
  // Check visible
  if( ! IsLep() ){
    if( panTau.pt() > 0.0 && Mosaic::AlgorithmManager::isPanTauBetter( decayType, tauRec.pt() ) ){ this->set( panTau ); }else{ this->set( tauRec ); }
  }
  this->setMass( 
                (  particleType == Mosaic::ParticleType::p_Pion)     ? Mosaic::Constant::MPI : (particleType == Mosaic::ParticleType::p_Rho)  ? Mosaic::Constant::MRHO 
                : (particleType == Mosaic::ParticleType::p_Electron) ? Mosaic::Constant::MEL : (particleType == Mosaic::ParticleType::p_Muon) ? Mosaic::Constant::MMU  
                : (decayType == Mosaic::ParticleType::mode_1pXn)     ? Mosaic::Constant::MA1 : (decayType == Mosaic::ParticleType::mode_3p0n) ? Mosaic::Constant::MA1 
                : (decayType == Mosaic::ParticleType::mode_3pXn)     ? Mosaic::Constant::MOMEGA  : 0.0, 
                this->m() > Mosaic::Constant::MTAU
                );
  if( isOKNeutral && isOKCharged) {
    helisityAngle = 
      (  decayType==Mosaic::ParticleType::mode_1p1n) ? CosPsi_Rho(chargedParticles->at(0)->e(), this->e() ) 
      : (decayType==Mosaic::ParticleType::mode_1pXn) ? CosPsi_1p(this->e(),                 chargedParticles->at(0), neutralParticles->at(0), neutralParticles->at(1)) 
      : (decayType==Mosaic::ParticleType::mode_3p0n) ? CosPsi_3p(this->charge(), this->e(), chargedParticles->at(0), chargedParticles->at(1), chargedParticles->at(2)) 
      : (decayType==Mosaic::ParticleType::mode_3pXn) ? CosPsi_3p(this->charge(), this->e(), chargedParticles->at(0), chargedParticles->at(1), chargedParticles->at(2)) : 0.0; 
  }else{
    helisityAngle = 
      (  decayType==Mosaic::ParticleType::mode_1p1n) ? Uniform(Engine)
      : (decayType==Mosaic::ParticleType::mode_1pXn) ? Uniform(Engine)*2.0 - 1.0 
      : (decayType==Mosaic::ParticleType::mode_3p0n) ? Uniform(Engine)*2.0 - 1.0 
      : (decayType==Mosaic::ParticleType::mode_3pXn) ? Uniform(Engine)*2.0 - 1.0  : 0.0;
  }
  if( helisityAngle != helisityAngle ) {
    helisityAngle = 
      (  decayType==Mosaic::ParticleType::mode_1p1n) ? Uniform(Engine)
      : (decayType==Mosaic::ParticleType::mode_1pXn) ? Uniform(Engine)*2.0 - 1.0 
      : (decayType==Mosaic::ParticleType::mode_3p0n) ? Uniform(Engine)*2.0 - 1.0 
      : (decayType==Mosaic::ParticleType::mode_3pXn) ? Uniform(Engine)*2.0 - 1.0  : 0.0;
  }

  return true;
}


int Mosaic::TauObj::IsLep()    const { return (IsEle() || IsMuon()); }
int Mosaic::TauObj::IsEle()    const { return (decayType==Mosaic::ParticleType::p_Electron) ? true : false; }
int Mosaic::TauObj::IsMuon()   const { return (decayType==Mosaic::ParticleType::p_Muon)     ? true : false; }
int Mosaic::TauObj::nCharged() const { return chargedParticles->size(); }
int Mosaic::TauObj::nNeutral() const { return neutralParticles->size(); }
Mosaic::ParticleType Mosaic::TauObj::DecayType()     const { return decayType; }
Mosaic::ParticleType Mosaic::TauObj::ParticleType()  const { return particleType; }
double Mosaic::TauObj::HelisityAngle()               const { return helisityAngle; }

double Mosaic::TauObj::CosPsi_3p(double charge, double visE, Mosaic::FourVector* pi1, Mosaic::FourVector* pi2, Mosaic::FourVector* pi3)     const { 
  return 
    (charge!=pi1->charge()) ? Mosaic::AlgorithmManager::a1_HelisityAngle(visE, pi1->px(),pi1->py(),pi1->pz(),pi1->e(),pi2->px(),pi2->py(),pi2->pz(),pi2->e(),pi3->px(),pi3->py(),pi3->pz(),pi3->e()) : 
    (charge!=pi2->charge()) ? Mosaic::AlgorithmManager::a1_HelisityAngle(visE, pi2->px(),pi2->py(),pi2->pz(),pi2->e(),pi3->px(),pi3->py(),pi3->pz(),pi3->e(),pi1->px(),pi1->py(),pi1->pz(),pi1->e()) : 
    (charge!=pi3->charge()) ? Mosaic::AlgorithmManager::a1_HelisityAngle(visE, pi3->px(),pi3->py(),pi3->pz(),pi3->e(),pi1->px(),pi1->py(),pi1->pz(),pi1->e(),pi2->px(),pi2->py(),pi2->pz(),pi2->e()) : 0.0 ;
}
double Mosaic::TauObj::CosPsi_1p(double visE, Mosaic::FourVector* pi1, Mosaic::FourVector* pi01, Mosaic::FourVector* pi02)     const { 
  return Mosaic::AlgorithmManager::a1_HelisityAngle(visE, pi1->px(),pi1->py(),pi1->pz(),pi1->e(), pi01->px(),pi01->py(),pi01->pz(),pi01->e(),pi02->px(),pi02->py(),pi02->pz(),pi02->e());
}
double Mosaic::TauObj::CosPsi_Rho(double e, double E)     const { return e/E >= 1.0 ? 0.99999999 : e/E; }
Mosaic::FourVector Mosaic::TauObj::Vis() const { return FourVector( this->px(), this->py(), this->pz(), this->e(), this->m(), this->charge() ); }

#endif // MOSAIC_TAUOBJ_C


