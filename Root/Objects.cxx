// Dear emacs, this is -*- c++ -*- 
/*
 * Objects 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#include "MOSAIC/Objects.h"

#ifndef MOSAIC_OBJECTS_C
#define MOSAIC_OBJECTS_C

Mosaic::FourVector::FourVector(){ this->set(0, 0, 0, 0, 0, 0, 0); }
Mosaic::FourVector::FourVector( const Mosaic::FourVector& v){ this->set(v.px(), v.py(), v.pz(), v.e(), v.m(), v.charge(), v.score()); }
Mosaic::FourVector::FourVector(double px_, double py_, double pz_, double e_){ this->set(px_, py_, pz_, e_, sqrt(e_*e_ - (px_*px_ + py_*py_ + pz_*pz_) ), 0.0, 0.0 ); }
Mosaic::FourVector::FourVector(double px_, double py_, double pz_, double e_, double m_, double charge_, double score_){ this->set(px_, py_, pz_, e_, m_, charge_, score_ ); }
Mosaic::FourVector::~FourVector(){ ; }

void Mosaic::FourVector::clear(){ _px = _py = _pz = _pt = _p2 = _p = _e = _m = _charge = _score = 0.0; } 
void Mosaic::FourVector::set( const FourVector& v ){ this->set(v.px(), v.py(), v.pz(), v.e(), v.m(), v.charge(), v.score()); }
void Mosaic::FourVector::set(double px_, double py_, double pz_, double e_, double m_, double charge_, double score_){
  _charge = charge_;
  _score  = score_;
  _e      = e_;
  _px     = px_;
  _py     = py_;
  _pz     = pz_;
  _pt     = px_*px_ + py_*py_;
  _p2     = _pt + _pz*_pz;
  _p      = sqrt(_p2);
  _pt     = sqrt(_pt);
  _m      = m_;
}
void Mosaic::FourVector::Boost_impl2(const FourVector& b, const double& gamma, const double& bp, const double& C){
  _px += C * b.px();
  _py += C * b.py();
  _pz += C * b.pz();
  _e   = gamma * (e() + bp);
  _pt  = px()*px() + py()*py();
  _p2  = _pt + pz()*pz();
  _p   = sqrt(_p2);
  _pt  = sqrt(_pt);
  setMass();
}
void Mosaic::FourVector::setMass(double m_, bool isOK){ _m = isOK ? ((m_ > 0.0) ? m_ : 0.0) : m(); }
void Mosaic::FourVector::setMass(){
  _m = e()*e() - p2();
  _m = _m <=0 ? 0.0 : sqrt(_m);
}

void Mosaic::FourVector::Boost_impl(const FourVector& b, const double& gamma, const double& gamma2, const double& bp){ Boost_impl2(b, gamma, bp, gamma * e() + gamma2 * bp); }
void Mosaic::FourVector::Boost     (const FourVector& b, const double& gamma, const double& gamma2)                  { Boost_impl (b, gamma, gamma2, this->InnerProduct(b)); }
double Mosaic::FourVector::InnerProduct(const FourVector& v) const { return px()*v.px() + py()*v.py() + pz()*v.pz(); }
double Mosaic::FourVector::px()      const { return _px; }
double Mosaic::FourVector::py()      const { return _py; }
double Mosaic::FourVector::pz()      const { return _pz; }
double Mosaic::FourVector::pt()      const { return _pt; }
double Mosaic::FourVector::e()       const { return _e; }
double Mosaic::FourVector::m()       const { return _m; }
double Mosaic::FourVector::p()       const { return _p; }
double Mosaic::FourVector::p2()      const { return _p2; }
double Mosaic::FourVector::charge()  const { return _charge; }
double Mosaic::FourVector::score()   const { return _score; }
double Mosaic::FourVector::phi()     const { return (px() == 0.0 && py() == 0.0) ? 0.0                : Mosaic::ATan2(py(), px()) ; }
double Mosaic::FourVector::theta()   const { return (px() == 0.0 && py() == 0.0 && pz() == 0.0) ? 0.0 : Mosaic::ATan2(pt(), pz()); }
double Mosaic::FourVector::eta()     const { return -0.5*log((p() - pz())/(p()+pz())); }

Mosaic::MissingEt::MissingEt() { this->clear(); }

Mosaic::Jet::Jet() : FourVector(), N2(0.0), S2(0.0), C2(0.0), Pt_GeV(0.0) { ; }
Mosaic::Jet::Jet( double px_, double py_, double pz_, double e_, double m_, double charge_, double socre_) : 
  FourVector(px_, py_, pz_, e_, m_, charge_, socre_), 
  N2(std::pow( Mosaic::Constant::JetResol_NTerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(this->eta()))) ) , 2)), 
  S2(std::pow( Mosaic::Constant::JetResol_STerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(this->eta()))) ) , 2)), 
  C2(std::pow( Mosaic::Constant::JetResol_CTerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(this->eta()))) ) , 2)), 
  Pt_GeV(this->pt() * Mosaic::Constant::_MeV2GeV) 
{ ; }
Mosaic::Jet::Jet( const Mosaic::FourVector& v) :
  FourVector(v), 
  N2(std::pow( Mosaic::Constant::JetResol_NTerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(v.eta()))) ) , 2)), 
  S2(std::pow( Mosaic::Constant::JetResol_STerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(v.eta()))) ) , 2)), 
  C2(std::pow( Mosaic::Constant::JetResol_CTerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(v.eta()))) ) , 2)), 
  Pt_GeV(v.pt() * Mosaic::Constant::_MeV2GeV) 
{ ; }
Mosaic::Jet::Jet( const Mosaic::Jet& v) :
  FourVector(v.px(), v.py(), v.pz(), v.e(), v.m(), v.charge(), v.score() ), 
  N2(std::pow( Mosaic::Constant::JetResol_NTerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(v.eta()))) ) , 2)), 
  S2(std::pow( Mosaic::Constant::JetResol_STerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(v.eta()))) ) , 2)), 
  C2(std::pow( Mosaic::Constant::JetResol_CTerm.at( Mosaic::enumToSize_t(Mosaic::JetResol_Eta(fabs(v.eta()))) ) , 2)), 
  Pt_GeV(v.pt() * Mosaic::Constant::_MeV2GeV) 
{ ; }


Mosaic::MCMCParameter::MCMCParameter(double _x0, double _xDelta, std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> seq) : xDelta(_xDelta) {
  xCandidate = _x0;
  xPropse    = _x0;
  xMax       = _x0;
  std::seed_seq seed( std::begin(seq), std::end(seq) );
  Engine  = new std::mt19937( seed );
  Uniform = new std::uniform_real_distribution<double> ( -xDelta, xDelta) ;
}
Mosaic::MCMCParameter::~MCMCParameter(){
  delete Engine;
  delete Uniform;
}
void Mosaic::MCMCParameter::ReInit(double Low, double High){ xPropse = (High - Low)/(2.0*xDelta) * ((*Uniform)(*Engine) + xDelta) + Low;}
void Mosaic::MCMCParameter::Sampling(double Low, double High){
  double u = (*Uniform)(*Engine) ;
  xPropse =  (Low < xCandidate + u && xCandidate + u < High) ? xCandidate + u : (Low >= xCandidate + u) ? xDelta + Low +  u : High - xDelta + u;
}
void Mosaic::MCMCParameter::Set(double v)                             { xPropse = v; }
void Mosaic::MCMCParameter::SetC(double v)                            { xCandidate = v; }
void Mosaic::MCMCParameter::Update()                                  { xCandidate = xPropse; }
void Mosaic::MCMCParameter::KeepMax()                                 { xMax = xPropse; }
void Mosaic::MCMCParameter::SetValueCandidate(double v)               { xCandidate = v; }
double Mosaic::MCMCParameter::ValuePropose()                    const { return xPropse    ; }
double Mosaic::MCMCParameter::ValueCandidate()                  const { return xCandidate ; }
double Mosaic::MCMCParameter::ValueMax()                        const { return xMax ; }

Mosaic::MCMCHad::MCMCHad(double _X0, double _MissMass0, double _phi0, double _mass, double _evis, std::vector< std::array< std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs) {
  MOSAIC_UNUSED_VARIABLE( _MissMass0 );
  MOSAIC_UNUSED_VARIABLE( _mass );
  MOSAIC_UNUSED_VARIABLE( _evis );
  EnergyFraction   = new MCMCParameter(_X0,          0.05,  seqs.at(0) );
  DecayPhi         = new MCMCParameter(_phi0,        0.50,  seqs.at(1) );
  EnergyResol      = new MCMCParameter(1.0,          0.10,  seqs.at(2) );
}
Mosaic::MCMCHad::~MCMCHad(){
  delete EnergyFraction;
  delete DecayPhi;
  delete EnergyResol;
}
void Mosaic::MCMCHad::ReInit()                                { EnergyFraction->ReInit(0.0, 1.0);   DecayPhi->ReInit(0.0, Mosaic::Constant::PI2);   }
void Mosaic::MCMCHad::Sampling()                              { EnergyFraction->Sampling(0.0, 1.0); DecayPhi->Sampling(0.0, Mosaic::Constant::PI2); EnergyResol->Sampling(0.01, 1.99); }
void Mosaic::MCMCHad::Update()                                { EnergyFraction->Update(); DecayPhi->Update();  EnergyResol->Update(); }
void Mosaic::MCMCHad::KeepMax()                               { EnergyFraction->KeepMax();DecayPhi->KeepMax(); EnergyResol->KeepMax(); }
double Mosaic::MCMCHad::EnergyFraction_ValuePropose()     const { return EnergyFraction->ValuePropose(); }
double Mosaic::MCMCHad::EnergyFraction_ValueCandidate()   const { return EnergyFraction->ValueCandidate(); }
double Mosaic::MCMCHad::DecayPhi_ValuePropose()           const { return DecayPhi->ValuePropose(); }
double Mosaic::MCMCHad::DecayPhi_ValueCandidate()         const { return DecayPhi->ValueCandidate(); }
double Mosaic::MCMCHad::MissingMass_ValuePropose()        const { return 0.0; }
double Mosaic::MCMCHad::MissingMass_ValueCandidate()      const { return 0.0; }
double Mosaic::MCMCHad::EnergyResolution_ValuePropose()   const { return EnergyResol->ValuePropose(); }
double Mosaic::MCMCHad::EnergyResolution_ValueCandidate() const { return EnergyResol->ValueCandidate(); }

Mosaic::MCMCLep::MCMCLep(double _X0, double _MissMass0, double _phi0, double _mass, double _evis, std::vector< std::array< std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs):
  a(_mass/Mosaic::Constant::MTAU), A( a * a ), C(Mosaic::Constant::MTAU2/_evis/_evis), M(sqrt( Mosaic::Constant::MTAU2 - _mass*_mass)), A_p_1(A + 1.0), A_m_1_2( pow(A - 1.0, 2) ) 
{
  EnergyFraction   = new MCMCParameter(_X0,          0.050,  seqs.at(0) );
  MissingMass      = new MCMCParameter(_MissMass0,   100.0,  seqs.at(1) );
  DecayPhi         = new MCMCParameter(_phi0,        0.500,  seqs.at(2) );
  EnergyResol      = new MCMCParameter(1.0,          0.100,  seqs.at(3) );
}
Mosaic::MCMCLep::~MCMCLep(){
  delete EnergyFraction;
  delete MissingMass;
  delete DecayPhi;
  delete EnergyResol;
}
void Mosaic::MCMCLep::Sampling() {
  for(int i = 0; i < 100; ++i){
    EnergyFraction->Sampling(0.0, 1.0);
    MissingMass->Sampling(0.0, M );
    if( isOK(EnergyFraction->ValuePropose(), MissingMass->ValuePropose() ) ) break;
  }
  DecayPhi->Sampling(0.0, Mosaic::Constant::PI2); 
  EnergyResol->Sampling(0.01, 1.99);
}
void Mosaic::MCMCLep::ReInit() {
  for(int i = 0; i < 100; ++i){
    EnergyFraction->ReInit(0.0, 1.0);
    MissingMass->ReInit(0.0, M );
    if( isOK(EnergyFraction->ValuePropose(), MissingMass->ValuePropose() ) ) break;
  }
  DecayPhi->ReInit(0.0, Mosaic::Constant::PI2); 
}

bool Mosaic::MCMCLep::isOK_impl(double x, double B){  return pow(2.0*x - (A_p_1 + B), 2) < (1.0 - C*x*x)*( A_m_1_2 + pow(1.0 - B, 2) - (1.0 + 2.0*A*B)) ; }
bool Mosaic::MCMCLep::isOK(double x, double b){ return isOK_impl(x, b*b); } 
void Mosaic::MCMCLep::Update()                                { EnergyFraction->Update(); MissingMass->Update(); DecayPhi->Update();  EnergyResol->Update(); }
void Mosaic::MCMCLep::KeepMax()                               { EnergyFraction->KeepMax();MissingMass->KeepMax();DecayPhi->KeepMax(); EnergyResol->KeepMax(); }
double Mosaic::MCMCLep::EnergyFraction_ValuePropose()     const { return EnergyFraction->ValuePropose(); }
double Mosaic::MCMCLep::EnergyFraction_ValueCandidate()   const { return EnergyFraction->ValueCandidate(); }
double Mosaic::MCMCLep::DecayPhi_ValuePropose()           const { return DecayPhi->ValuePropose(); }
double Mosaic::MCMCLep::DecayPhi_ValueCandidate()         const { return DecayPhi->ValueCandidate(); }
double Mosaic::MCMCLep::MissingMass_ValuePropose()        const { return MissingMass->ValuePropose(); }
double Mosaic::MCMCLep::MissingMass_ValueCandidate()      const { return MissingMass->ValueCandidate(); }
double Mosaic::MCMCLep::EnergyResolution_ValuePropose()   const { return EnergyResol->ValuePropose(); }
double Mosaic::MCMCLep::EnergyResolution_ValueCandidate() const { return EnergyResol->ValueCandidate(); }

Mosaic::MCMCRecoilSystem::MCMCRecoilSystem(const std::vector< Mosaic::Jet* >* jets, std::vector< std::array< std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs) : sizeJet(jets->size() ) {
  recoilJets       = new std::vector< Mosaic::Jet * >;
  recoilResolution = new std::vector< Mosaic::MCMCParameter* >;
  recoilJets->clear();
  recoilResolution->clear();
  for(int i = 0; i < sizeJet; ++i) {
    recoilJets->emplace_back( new Mosaic::Jet( jets->at(i)->px(), jets->at(i)->py(), jets->at(i)->pz(), jets->at(i)->e(), jets->at(i)->m(), jets->at(i)->charge(), jets->at(i)->score() ) );
    recoilResolution->emplace_back( new Mosaic::MCMCParameter(1.0, 0.1, seqs.at(i) ) );
  }
  dPx = dPy = 0.0;
  totalProb = 0.0;
}
Mosaic::MCMCRecoilSystem::~MCMCRecoilSystem(){
  for(auto& jet : *recoilJets)       delete jet;
  for(auto& jet : *recoilResolution) delete jet;
  dPx = dPy = 0.0;
  recoilJets->clear();
  recoilResolution->clear();
  delete recoilJets;
  delete recoilResolution;
}

void Mosaic::MCMCRecoilSystem::Sampling(){
  totalProb = 0.0;
  dPx = dPy = 0.0;
  for(int i = 0; i < sizeJet; ++i){
    recoilResolution->at(i)->Sampling(0.50, 1.50);
    double R   = recoilResolution->at(i)->ValuePropose();
    //std::cout <<" Proposed R is " << R << " Prob is " << recoilJets->at(i)->Prob( R ) << " sigma is " << recoilJets->at(i)->getSigma( R * recoilJets->at(i)->pt() / 1000.0 ) * 1000.0 << std::endl;
    totalProb += recoilJets->at(i)->Prob( R );
    dPx       += recoilJets->at(i)->px() * (R - 1.0); // newPt - origPt
    dPy       += recoilJets->at(i)->py() * (R - 1.0); // newPt - origPt
  }
}
void Mosaic::MCMCRecoilSystem::ReInit() { for(auto& jet : *recoilResolution) jet->ReInit(0.50, 1.50 ); }
//void Mosaic::MCMCRecoilSystem::ReInit() { ; }
void Mosaic::MCMCRecoilSystem::Update() { for(auto& jet : *recoilResolution) jet->Update(); }
void Mosaic::MCMCRecoilSystem::KeepMax(){ for(auto& jet : *recoilResolution) jet->KeepMax(); }

double Mosaic::MCMCRecoilSystem::TotalProb() const { return totalProb; }
double Mosaic::MCMCRecoilSystem::DeltaPx()   const { return dPx; }
double Mosaic::MCMCRecoilSystem::DeltaPy()   const { return dPy; }

Mosaic::Angle::Angle(): angle(0.0), Sin(0.0), Cos(0.0), Tan(0.0){ ; }
Mosaic::Angle::Angle(double a): angle(a), Sin(std::sin(a)), Cos(std::cos(a)), Tan(std::tan(a)){ ; }
Mosaic::Angle::Angle(const Angle& a): angle(a.angle), Sin(a.Sin), Cos(a.Cos), Tan(a.Tan){ ; }
Mosaic::Angle::~Angle() { }

Mosaic::AngleSet::AngleSet(): CosP_CosT(0.0), CosP_SinT(0.0), SinP_CosT(0.0), SinP_SinT(0.0){ ; }
Mosaic::AngleSet::AngleSet(Angle P, Angle T): CosP_CosT(P.Cos * T.Cos), CosP_SinT(P.Cos * T.Sin), SinP_CosT(P.Sin * T.Cos), SinP_SinT(P.Sin * T.Sin){ ; }
Mosaic::AngleSet::AngleSet(const AngleSet& a): CosP_CosT(a.CosP_CosT), CosP_SinT(a.CosP_SinT), SinP_CosT(a.SinP_CosT), SinP_SinT(a.SinP_SinT){ ; }
Mosaic::AngleSet::~AngleSet() { }

Mosaic::Spline::Spline(double _a, double _b, double _c, double _d, double _x0 ) : a(_a), b(_b), c(_c), d(_d), x0(_x0){ ; }
Mosaic::Spline::Spline(const Spline& s) : a(s.a), b(s.b), c(s.c), d(s.d), x0(s.x0) { ; } 
Mosaic::Spline::~Spline() { ; }
double Mosaic::Spline::Value_impl(double XX) const { return a + b*XX + XX*XX*(c + d*XX); }
double Mosaic::Spline::Value(double X)       const { return Value_impl(X - x0); }

Mosaic::SplineSet::SplineSet(double _LowX, double _dX, std::initializer_list<double> y) : LowX(_LowX), dX(_dX), SetSize(y.size()){
  std::vector<double> x;
  for(int i = 0, end = y.size(); i < end; ++i) x.emplace_back(LowX + i * dX);
  int n = x.size() - 1;
  std::vector<double> a; a.insert( a.begin(), y.begin(), y.end() );
  std::vector<double> b(n);
  std::vector<double> d(n);
  std::vector<double> h;
  std::vector<double> alpha(n); alpha[0] = 0.0;
  std::vector<double> c(n+1);
  std::vector<double> l(n+1);   l[0] = 1.0;
  std::vector<double> mu(n+1); mu[0] = 0.0;
  std::vector<double> z(n+1);   z[0] = 0.0;
  
  for(int i = 0; i < n; ++i) h.emplace_back(x.at(i+1) - x.at(i) );
  for(int i = 1; i < n; ++i) alpha[i] =  3.0*(a.at(i+1) - a.at(i) )/ h.at(i) - 3.0*(a.at(i) - a.at(i-1))/h.at(i-1) ;
  
  for(int i = 1; i < n; ++i){
    l[i]  = 2.0 * (x.at(i+1) - x.at(i-1)) - h.at(i-1) * mu.at(i-1);
    mu[i] = h.at(i) / l.at(i);
    z[i]  = (alpha.at(i) - h.at(i-1) * z.at(i-1) ) / l.at(i);
  }
  l[n] = 1;
  z[n] = 0;
  c[n] = 0;
  for(int j = n -1 ; j >= 0; --j){
    c[j] = z.at(j) - mu.at(j) * c.at(j+1);
    b[j] = (a.at(j+1) - a.at(j) ) / h.at(j) - h.at(j) * ( c.at(j+1) + 2.0 * c.at(j))/3.0;
    d[j] = (c.at(j+1) - c.at(j) ) / 3.0 / h.at(j);
  }
  for(int i = 0; i < n; ++i) Set.emplace_back(Spline(a.at(i), b.at(i), c.at(i), d.at(i), x.at(i))); 
  Set.emplace_back(Spline(a.at(n-1), b.at(n-1), c.at(n-1), d.at(n-1), x.at(n-1))); 
}
Mosaic::SplineSet::~SplineSet() { ; }
double Mosaic::SplineSet::Value(double x)    const { return Set.at( Bin(x) ).Value(x); }
int    Mosaic::SplineSet::Bin_impl(int s)    const { return (s > SetSize) ? SetSize : (s < 0) ? 0 : s; }
int    Mosaic::SplineSet::Bin(double x)      const { return Bin_impl( int((x - LowX)/dX) ); }

Mosaic::SamplingPoint::SamplingPoint() : _x1(1.0), _x2(1.0), _x12(1.0), _px1(0.0), _py1(0.0), _pz1(0.0), _px2(0.0), _py2(0.0), _pz2(0.0), _mVis(-777.), _mass(-777.), _met(-1e+20), _resol(-1e+20), _me(-1e+20), _partonDF(-1e+20), _prob(-1e+20){ ; }
Mosaic::SamplingPoint::SamplingPoint(double x1_, double x2_, double px1_, double py1_, double pz1_, double px2_, double py2_, double pz2_, double mVis_, double mass_, double prob_) :
  _x1(x1_), _x2(x2_), _x12(_x1*_x2), _px1(px1_), _py1(py1_), _pz1(pz1_), _px2(px2_), _py2(py2_), _pz2(pz2_),  _mVis(mVis_), _mass(mass_), _met(0.0), _resol(0.0), _me(0.0), _partonDF(0.0), _prob(prob_){ ; }
Mosaic::SamplingPoint::SamplingPoint(double x1_, double x2_, double px1_, double py1_, double pz1_, double px2_, double py2_, double pz2_, double mVis_, double mass_, double met_, double me_) :
  _x1(x1_), _x2(x2_), _x12(_x1*_x2),  _px1(px1_), _py1(py1_), _pz1(pz1_), _px2(px2_), _py2(py2_), _pz2(pz2_), _mVis(mVis_), _mass(mass_), _met(met_), _resol(0.0), _me(me_), _partonDF(0.0), _prob(_met + _me){ ; }
Mosaic::SamplingPoint::SamplingPoint(double x1_, double x2_, double px1_, double py1_, double pz1_, double px2_, double py2_, double pz2_, double mVis_, double mass_, double met_, double resol_, double me_) :
  _x1(x1_), _x2(x2_), _x12(_x1*_x2),  _px1(px1_), _py1(py1_), _pz1(pz1_), _px2(px2_), _py2(py2_), _pz2(pz2_), _mVis(mVis_), _mass(mass_), _met(met_), _resol(resol_), _me(me_), _partonDF(0.0), _prob(_met + _resol + _me){ ; }
Mosaic::SamplingPoint::SamplingPoint(double x1_, double x2_, double px1_, double py1_, double pz1_, double px2_, double py2_, double pz2_, double mVis_, double mass_, double met_, double resol_, double partonDF_, double me_) :
  _x1(x1_), _x2(x2_), _x12(_x1*_x2),  _px1(px1_), _py1(py1_), _pz1(pz1_), _px2(px2_), _py2(py2_), _pz2(pz2_), _mVis(mVis_), _mass(mass_), _met(met_), _resol(resol_), _me(me_), _partonDF(partonDF_), _prob(_met + _resol + _me + _partonDF){ ; }
/*
Mosaic::SamplingPoint::SamplingPoint( const Mosaic::SamplingPoint&  p ) : 
  _x1(p._x1), _x2(p._x2), _x12(_x1*_x2),  _px1(p._px1), _py1(p._py1), _pz1(p._pz1), _px2(p._px2), _py2(p._py2), _pz2(p._pz2), _mVis(p._mVis), _mass(p._mass), _met(p._met), _resol(p._resol), _me(p._me), _partonDF(p._partonDF), _prob(_met + _resol + _me + _partonDF){ ; }
*/

Mosaic::SamplingPoint::SamplingPoint(       Mosaic::SamplingPoint&& p ) noexcept {
  _x1       = std::move( p._x1 );
  _x2       = std::move( p._x2 );
  _x12      = std::move( p._x12 );
  _px1      = std::move( p._px1 );
  _py1      = std::move( p._py1 );
  _pz1      = std::move( p._pz1 );
  _px2      = std::move( p._px2 );
  _py2      = std::move( p._py2 );
  _pz2      = std::move( p._pz2 );
  _mVis     = std::move( p._mVis );
  _mass     = std::move( p._mass );
  _met      = std::move( p._met );
  _resol    = std::move( p._resol );
  _me       = std::move( p._me );
  _partonDF = std::move( p._partonDF );
  _prob     = std::move( p._prob );
}


double Mosaic::SamplingPoint::x1()         const { return _x1; }
double Mosaic::SamplingPoint::x2()         const { return _x2; }
double Mosaic::SamplingPoint::x12()        const { return _x12; }
double Mosaic::SamplingPoint::px_mis1()    const { return _px1; }
double Mosaic::SamplingPoint::py_mis1()    const { return _py1; }
double Mosaic::SamplingPoint::pz_mis1()    const { return _pz1; }
double Mosaic::SamplingPoint::px_mis2()    const { return _px2; }
double Mosaic::SamplingPoint::py_mis2()    const { return _py2; }
double Mosaic::SamplingPoint::pz_mis2()    const { return _pz2; }
double Mosaic::SamplingPoint::probMET()    const { return _met; }
double Mosaic::SamplingPoint::probResol()  const { return _resol; }
double Mosaic::SamplingPoint::probPDF()    const { return _partonDF; }
double Mosaic::SamplingPoint::probME()     const { return _me; }
double Mosaic::SamplingPoint::prob()       const { return _prob; }
double Mosaic::SamplingPoint::mVis()       const { return _mVis; }
double Mosaic::SamplingPoint::mass()       const { return _mass; }



#endif // MOSAIC_OBJECTS_C


