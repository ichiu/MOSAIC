// Dear emacs, this is -*- c++ -*- 
/*
 * Mosaicalculator
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#include "MOSAIC/MOSAICalculator.h"

#ifndef MOSAIC_MOSAICALCULATOR_C
#define MOSAIC_MOSAICALCULATOR_C

#include "PDF/Init_Rho.cxx"
#include "PDF/Init_a1.cxx"


MOSAICalculator::MOSAICalculator( std::string m_LHAPDF_Type ) : m_LHAPDFType(m_LHAPDF_Type) {
  outputManager_Spin1   = new Mosaic::OutputManager();
  outputManager_CPEven  = new Mosaic::OutputManager();
  outputManager_CPOdd   = new Mosaic::OutputManager();
  inputManager          = new Mosaic::InputManager();
  
  MOSAICalculator::Init_Spline_Rho();
  MOSAICalculator::Init_Spline_a1();
  
  PDF_spin0_CPEven  = new Mosaic::PDF_Spin0_CPEven();
  PDF_spin0_CPOdd   = new Mosaic::PDF_Spin0_CPOdd();
  PDF_spin1         = new Mosaic::PDF_Spin1();
  PDF_pion          = new Mosaic::PDF_Pion(Mosaic::Constant::MPI2/Mosaic::Constant::MTAU2,  Mosaic::Constant::BRPION);
  PDF_El            = new Mosaic::PDF_Lep (Mosaic::Constant::MEL2/Mosaic::Constant::MTAU2,  Mosaic::Constant::BREL);
  PDF_Mu            = new Mosaic::PDF_Lep (Mosaic::Constant::MMU2/Mosaic::Constant::MTAU2,  Mosaic::Constant::BRMU);
#ifdef MOSAIC_USE_DEVELOP
  PDF_lHAPDF        = new Mosaic::PDF_LHAPDF( m_LHAPDFType );
#endif  
  m_RunMode = "NOMINAL";
}
MOSAICalculator::~MOSAICalculator(){
  delete outputManager_Spin1;
  delete outputManager_CPEven;
  delete outputManager_CPOdd;
  delete inputManager;
  delete PDF_spin0_CPEven;
  delete PDF_spin0_CPOdd;
  delete PDF_spin1;
  delete PDF_pion;
  delete PDF_rho;
  delete PDF_a1;
  delete PDF_El;
  delete PDF_Mu;
#ifdef MOSAIC_USE_DEVELOP
  delete PDF_lHAPDF;
#endif
}

bool MOSAICalculator::Run_Mosaic_Z(){ 
  bool res = false;
  if( m_RunMode.find("NOMINAL") != std::string::npos ){
    if      ( m_LLHType.find("DEFAULT")            != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Nominal>(PDF_spin1,        outputManager_Spin1);
#ifdef MOSAIC_USE_DEVELOP
    else if ( m_LLHType.find("MEONLY")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::MEOnly          , Mosaic::RunningMode::Nominal>(PDF_spin1,        outputManager_Spin1);
    else if( m_LLHType.find("METONLY")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::METOnly         , Mosaic::RunningMode::Nominal>(PDF_spin1,        outputManager_Spin1);
#endif    
  }else if( m_RunMode.find("FAST") != std::string::npos ){
    if     ( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Fast>(PDF_spin1,        outputManager_Spin1);
#ifdef MOSAIC_USE_DEVELOP
    else if( m_LLHType.find("MEONLY")              != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::MEOnly          , Mosaic::RunningMode::Fast>(PDF_spin1,        outputManager_Spin1);
    else if( m_LLHType.find("METONLY")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::METOnly         , Mosaic::RunningMode::Fast>(PDF_spin1,        outputManager_Spin1);
#endif
  }
  return res;
}
bool MOSAICalculator::Run_Mosaic_H(){ 
  bool res = false;
  if( m_RunMode.find("NOMINAL") != std::string::npos ){
    if     ( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Nominal>(PDF_spin0_CPEven,        outputManager_CPEven);
#ifdef MOSAIC_USE_DEVELOP
    else if( m_LLHType.find("MEONLY")              != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::MEOnly          , Mosaic::RunningMode::Nominal>(PDF_spin0_CPEven,        outputManager_CPEven);
    else if( m_LLHType.find("METONLY")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::METOnly         , Mosaic::RunningMode::Nominal>(PDF_spin0_CPEven,        outputManager_CPEven);
#endif
  }else if( m_RunMode.find("FAST") != std::string::npos ){
    if     ( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Fast>(PDF_spin0_CPEven,        outputManager_CPEven);
#ifdef MOSAIC_USE_DEVELOP
    else if( m_LLHType.find("METONLY")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::METOnly         , Mosaic::RunningMode::Fast>(PDF_spin0_CPEven,        outputManager_CPEven);
    else if( m_LLHType.find("MEONLY")              != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::MEOnly          , Mosaic::RunningMode::Fast>(PDF_spin0_CPEven,        outputManager_CPEven);
#endif
  }
  return res;
}
bool MOSAICalculator::Run_Mosaic_A(){ 
  bool res = false;
  if( m_RunMode.find("NOMINAL") != std::string::npos ){
    if     ( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Nominal>(PDF_spin0_CPOdd,        outputManager_CPOdd);
#ifdef MOSAIC_USE_DEVELOP
    else if( m_LLHType.find("MEONLY")              != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::MEOnly          , Mosaic::RunningMode::Nominal>(PDF_spin0_CPOdd,        outputManager_CPOdd);
    else if( m_LLHType.find("METONLY")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::METOnly         , Mosaic::RunningMode::Nominal>(PDF_spin0_CPOdd,        outputManager_CPOdd);
#endif
  }else if( m_RunMode.find("FAST") != std::string::npos ){
    if     ( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Fast>(PDF_spin0_CPOdd,        outputManager_CPOdd);
#ifdef MOSAIC_USE_DEVELOP
    else if( m_LLHType.find("MEONLY")              != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::MEOnly          , Mosaic::RunningMode::Fast>(PDF_spin0_CPOdd,        outputManager_CPOdd);
    else if( m_LLHType.find("METONLY")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::METOnly         , Mosaic::RunningMode::Fast>(PDF_spin0_CPOdd,        outputManager_CPOdd);
#endif
  }
  return res;
}

#ifdef MOSAIC_USE_DEVELOP

bool MOSAICalculator::Run_Mosaic_Z(){ 
  bool res = false;
  if( m_RunMode.find("NOMINAL") != std::string::npos ){
    if     ( m_LLHType.find("PLUS_VIS_RESOLUTION") != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::PlusVisResoltion, Mosaic::RunningMode::Nominal>(PDF_spin1,        outputManager_Spin1);
    else if( m_LLHType.find("RECOILSYSTEM")        != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::RecoilSystem    , Mosaic::RunningMode::Nominal>(PDF_spin1,        outputManager_Spin1);
    else if( m_LLHType.find("PARTONDF")            != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::PartDF_LHAPDF   , Mosaic::RunningMode::Nominal>(PDF_spin1,        outputManager_Spin1);
    else if( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Nominal>(PDF_spin1,        outputManager_Spin1);
  }else if( m_RunMode.find("FAST") != std::string::npos ){
    if     ( m_LLHType.find("PLUS_VIS_RESOLUTION") != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::PlusVisResoltion, Mosaic::RunningMode::Fast>(PDF_spin1,        outputManager_Spin1);
    else if( m_LLHType.find("RECOILSYSTEM")        != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::RecoilSystem    , Mosaic::RunningMode::Fast>(PDF_spin1,        outputManager_Spin1);
    else if( m_LLHType.find("PARTONDF")            != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::PartDF_LHAPDF   , Mosaic::RunningMode::Fast>(PDF_spin1,        outputManager_Spin1);
    else if( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin1,        Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Fast>(PDF_spin1,        outputManager_Spin1);
  }
  return res;
}
bool MOSAICalculator::Run_Mosaic_H(){ 
  bool res = false;
  if( m_RunMode.find("NOMINAL") != std::string::npos ){
    if     ( m_LLHType.find("PLUS_VIS_RESOLUTION") != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::PlusVisResoltion, Mosaic::RunningMode::Nominal>(PDF_spin0_CPEven,        outputManager_CPEven);
    else if( m_LLHType.find("RECOILSYSTEM")        != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::RecoilSystem    , Mosaic::RunningMode::Nominal>(PDF_spin0_CPEven,        outputManager_CPEven);
    else if( m_LLHType.find("PARTONDF")            != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::PartDF_LHAPDF   , Mosaic::RunningMode::Nominal>(PDF_spin0_CPEven,        outputManager_CPEven);
    else if( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Nominal>(PDF_spin0_CPEven,        outputManager_CPEven);
  }else if( m_RunMode.find("FAST") != std::string::npos ){
    if     ( m_LLHType.find("PLUS_VIS_RESOLUTION") != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::PlusVisResoltion, Mosaic::RunningMode::Fast>(PDF_spin0_CPEven,        outputManager_CPEven);
    else if( m_LLHType.find("RECOILSYSTEM")        != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::RecoilSystem    , Mosaic::RunningMode::Fast>(PDF_spin0_CPEven,        outputManager_CPEven);
    else if( m_LLHType.find("PARTONDF")            != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::PartDF_LHAPDF   , Mosaic::RunningMode::Fast>(PDF_spin0_CPEven,        outputManager_CPEven);
    else if( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPEven, Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Fast>(PDF_spin0_CPEven,        outputManager_CPEven);
  }
  return res;
}
bool MOSAICalculator::Run_Mosaic_A(){ 
  bool res = false;
  if( m_RunMode.find("NOMINAL") != std::string::npos ){
    if     ( m_LLHType.find("PLUS_VIS_RESOLUTION") != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::PlusVisResoltion, Mosaic::RunningMode::Nominal>(PDF_spin0_CPOdd,        outputManager_CPOdd);
    else if( m_LLHType.find("RECOILSYSTEM")        != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::RecoilSystem    , Mosaic::RunningMode::Nominal>(PDF_spin0_CPOdd,        outputManager_CPOdd);
    else if( m_LLHType.find("PARTONDF")            != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::PartDF_LHAPDF   , Mosaic::RunningMode::Nominal>(PDF_spin0_CPOdd,        outputManager_CPOdd);
    else if( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Nominal>(PDF_spin0_CPOdd,        outputManager_CPOdd);
  }else if( m_RunMode.find("FAST") != std::string::npos ){
    if     ( m_LLHType.find("PLUS_VIS_RESOLUTION") != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::PlusVisResoltion, Mosaic::RunningMode::Fast>(PDF_spin0_CPOdd,        outputManager_CPOdd);
    else if( m_LLHType.find("RECOILSYSTEM")        != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::RecoilSystem    , Mosaic::RunningMode::Fast>(PDF_spin0_CPOdd,        outputManager_CPOdd);
    else if( m_LLHType.find("PARTONDF")            != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::PartDF_LHAPDF   , Mosaic::RunningMode::Fast>(PDF_spin0_CPOdd,        outputManager_CPOdd);
    else if( m_LLHType.find("DEFAULT")             != std::string::npos ) res = MOSAICalculator::Run_Mosaic<Mosaic::PDF_Spin0_CPOdd, Mosaic::LikelihoodType::Default         , Mosaic::RunningMode::Fast>(PDF_spin0_CPOdd,        outputManager_CPOdd);
  }
  return res;
}

#endif

bool MOSAICalculator::init(){ return inputManager->init() ; }
void MOSAICalculator::setCalibType ( Mosaic::CalibType e ) { m_CalibType  = e; }
void MOSAICalculator::setMETType   ( Mosaic::MET_Type  e ) { m_MET_Type   = e; }
void MOSAICalculator::setUseMET_MPT( bool e )              { m_useMET_MPT = e; }
void MOSAICalculator::setLikelihoodType( std::string e )   { m_LLHType    = e; }
void MOSAICalculator::setRunMode( std::string e )          { m_RunMode    = e; }

void MOSAICalculator::Clear(){
  //outputManager_Spin1->Mosaic::OutputManager::Clear();
  //outputManager_CPEven->Mosaic::OutputManager::Clear();
  //outputManager_CPOdd->Mosaic::OutputManager::Clear();
  inputManager->Mosaic::InputManager::Clear();
}

double MOSAICalculator::Mass_Z( ) const { return outputManager_Spin1->EstimateMass(  inputManager->nJet(), inputManager->Get_Channel(), inputManager->Get_CalibType(), m_RunMode ); }
double MOSAICalculator::Mass_H( ) const { return outputManager_CPEven->EstimateMass( inputManager->nJet(), inputManager->Get_Channel(), inputManager->Get_CalibType(), m_RunMode ); }
double MOSAICalculator::Mass_A( ) const { return outputManager_CPOdd->EstimateMass(  inputManager->nJet(), inputManager->Get_Channel(), inputManager->Get_CalibType(), m_RunMode ); }

#endif // MOSAIC_MOSAICALCULATOR_C
