// Dear emacs, this is -*- c++ -*- 
/*
 * ResolModel
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_RESOLMODEL_C
#define MOSAIC_RESOLMODEL_C

#include "MOSAIC/ResolModel.h"

Mosaic::ResolModel_ETMiss::ResolModel_ETMiss(double obsEx, double obsEy, double obsSumEt, double resolScale, double deltaThresold, double probScale, Mosaic::MET_nJet njet, Mosaic::MET_Type metType, Mosaic::CalibType calibtype):
  METType(metType), nJet(njet), calibType(calibtype), 
  ObsEx(obsEx/1000.), ObsEy(obsEy/1000.), ObsSumEt(obsSumEt/1000.), ObsEt(std::hypot(ObsEx, ObsEy)), ResolScale(resolScale), Significance( ObsEt/sqrt(ObsSumEt) ), 
  Sigma1   ( Mosaic::AlgorithmManager::MET_Resolution(ObsSumEt, calibType, METType, Mosaic::MET_Resol_Par::MET_Sigma1,   nJet)  ), 
  Sigma2   ( Mosaic::AlgorithmManager::MET_Resolution(ObsSumEt, calibType, METType, Mosaic::MET_Resol_Par::MET_Sigma2,   nJet)  ), 
  Fraction ( Mosaic::AlgorithmManager::MET_Resolution(ObsSumEt, calibType, METType, Mosaic::MET_Resol_Par::MET_Fraction, nJet, true) ), 
  Minus_Inv_Sigma1_2_2( - 1.0 / (2.0 * Sigma1 * Sigma1) ), 
  Minus_Inv_Sigma2_2_2( - 1.0 / (2.0 * Sigma2 * Sigma2) ), 
  Coeff_Sigma1(        Fraction / Sigma1 ), 
  Coeff_Sigma2( (1.0 - Fraction)/ Sigma2 ),
  MaxProb( Prob_EtMiss(0.0, 0.0) ), 
  Norm( Mosaic::Constant::LOG_PI*( Fraction*Fraction * Sigma1 + pow(1.0 - Fraction, 2)*Sigma2 + 2.0*Fraction*(1.0 - Fraction)*sqrt( (Sigma1*Sigma1 * Sigma2*Sigma2)/( Sigma1*Sigma1 + Sigma2*Sigma2 ) ) ) ),
//LogNorm(log(Norm)), DeltaThresold2( deltaThresold ), ProbScale( probScale )
  LogNorm(log(Norm)), DeltaThresold2( - deltaThresold ), ProbScale( 1.0 )
{
  /*
  std::cout <<" DeltaThresold2 is " << DeltaThresold2 << std::endl;
  std::cout <<" SumEt is "<< ObsSumEt << "GeV,  Significance is "  << Significance << " , " << ObsEx << " / " << ObsEy << " GeV "<< std::endl;
  std::cout <<" nJet is " << (int)nJet  << " Fraction is " << Fraction << " / Coeff_Sigma1 / 2 is " << Coeff_Sigma1 <<" / " << Coeff_Sigma2 << std::endl;;
  if( metType == Mosaic::MET_Type::MET_TST      ) std::cout <<" MET_TST will be used" << std::endl;
  if( metType == Mosaic::MET_Type::MET_TST_Soft ) std::cout <<" MET_TST_Soft will be used" << std::endl;
  if( metType == Mosaic::MET_Type::MET_MPT      ) std::cout <<" MET_MPT will be used" << std::endl;
  if( metType == Mosaic::MET_Type::MET_Truth    ) std::cout <<" MET_Truth will be used" << std::endl;
  if( metType == Mosaic::MET_Type::MET_Smear    ) std::cout <<" MET_Smear will be used" << std::endl;
  
  */
  ;
}
Mosaic::ResolModel_ETMiss::~ResolModel_ETMiss() { ; }

Mosaic::ResolModel_Muon::ResolModel_Muon(Mosaic::FourVector vis, double sigma) : Px(vis.px()), Py(vis.py()), Pz(vis.pz()), E(vis.e()), P2(vis.p2()), Sigma(sigma), SigmaOverPt(Sigma/vis.pt()), logSigma(log(Sigma)) { ; }
Mosaic::ResolModel_Muon::~ResolModel_Muon() { ; }

Mosaic::ResolModel_Electron::ResolModel_Electron(Mosaic::FourVector vis, double sigma) : Px(vis.px()), Py(vis.py()), Pz(vis.pz()), E(vis.e()), EOverP(E/vis.p()), Sigma(sigma), SigmaOverE(Sigma/E), SigmaOverP(Sigma/vis.p()), logSigma(log(Sigma)) { ; }
Mosaic::ResolModel_Electron::~ResolModel_Electron() { ; }

Mosaic::ResolModel_Tau::ResolModel_Tau(Mosaic::FourVector vis, double sigma) : Px(vis.px()), Py(vis.py()), Pz(vis.pz()), E(vis.e()), EOverP(E/vis.p()), Mass2(vis.m()*vis.m()), Sigma(sigma), SigmaOverE(Sigma/E), SigmaOverP(Sigma/vis.p()), logSigma(log(Sigma)) { ; }
Mosaic::ResolModel_Tau::~ResolModel_Tau() { ; }



#endif // MOSAIC_RESOLMODEL_C


