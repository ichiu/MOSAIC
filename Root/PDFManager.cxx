// Dear emacs, this is -*- c++ -*- 
/*
 * PDFManager 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#include "MOSAIC/PDFManager.h"

#ifndef MOSAIC_PDFMAGAGER_C
#define MOSAIC_PDFMAGAGER_C

Mosaic::PDF_Spin1::PDF_Spin1(){ ; }
Mosaic::PDF_Spin1::~PDF_Spin1(){ ; }
double Mosaic::PDF_Spin1::Prob_Reso_impl(std::pair<double, double>&& L1, std::pair<double, double>&& L2, FourVector&& Tau1, FourVector&& Tau2, FourVector&& Vis1, FourVector&& Vis2) const {
  AlgorithmManager::BoostBack2( Tau1, Tau2, Vis1, Vis2);
  return AlgorithmManager::Prob_impl( AlgorithmManager::PDF_Spin1(std::move(L1), std::move(L2), pow(sin(Mosaic::AlgorithmManager::ATan2(Tau1.pz(), Tau1.pt())) ,2), cos(Vis1.phi() + Vis2.phi()) )  ); 
}
double Mosaic::PDF_Spin1::Prob_Reso(std::pair<double, double>&& L1, std::pair<double, double>&& L2, FourVector&& Mis1, FourVector&& Mis2, FourVector&& Vis1, FourVector&& Vis2) const {
  
  return (L1.first * L1.second * L2.first * L2.second <= 0.0 ) ? -3e+10 : 
    Prob_Reso_impl(std::move(L1), std::move(L2), std::move( FourVector(Vis1.px() + Mis1.px(), Vis1.py() + Mis1.py(), Vis1.pz() + Mis1.pz(), Vis1.e() + Mis1.e() ) ), 
                   std::move( FourVector(Vis2.px() + Mis2.px(), Vis2.py() + Mis2.py(), Vis2.pz() + Mis2.pz(), Vis2.e() + Mis2.e()) ), std::move(Vis1), std::move(Vis2) ); 
}


Mosaic::PDF_Spin0_CPEven::PDF_Spin0_CPEven(){ ; }
Mosaic::PDF_Spin0_CPEven::~PDF_Spin0_CPEven(){ ; }
double Mosaic::PDF_Spin0_CPEven::Prob_Reso(std::pair<double, double>&& L1, std::pair<double, double>&& L2, FourVector&& Mis1, FourVector&& Mis2, FourVector&& Vis1, FourVector&& Vis2) const { 
  if (L1.first * L1.second * L2.first * L2.second <= 0.0 ) return -3e+10 ;
  AlgorithmManager::BoostBack1(Mis1, Mis2, Vis1, Vis2); 
  return AlgorithmManager::Prob_impl( AlgorithmManager::PDF_Spin0_CPEven(std::move(L1), std::move(L2), cos(Vis1.phi() - Vis2.phi()) ) ); 
}

Mosaic::PDF_Spin0_CPOdd::PDF_Spin0_CPOdd(){ ; }
Mosaic::PDF_Spin0_CPOdd::~PDF_Spin0_CPOdd(){ ; }
double Mosaic::PDF_Spin0_CPOdd::Prob_Reso(std::pair<double, double>&& L1, std::pair<double, double>&& L2, FourVector&& Mis1, FourVector&& Mis2, FourVector&& Vis1, FourVector&& Vis2) const { 
  if (L1.first * L1.second * L2.first * L2.second <= 0.0 ) return -3e+10 ;
  AlgorithmManager::BoostBack1(Mis1, Mis2, Vis1, Vis2); 
  return AlgorithmManager::Prob_impl( AlgorithmManager::PDF_Spin0_CPOdd(std::move(L1), std::move(L2), cos(Vis1.phi() - Vis2.phi()) ) );
}

Mosaic::PDF_Lep::PDF_Lep(double _a2, double _BR): a2(_a2), BR(_BR) { ; }
Mosaic::PDF_Lep::~PDF_Lep() { ; }
std::pair<double, double> Mosaic::PDF_Lep::Prob(double X, double Energy, double MisM) { return std::move( AlgorithmManager::PDF_Lep(a2, MisM/Constant::MTAU, X, Energy, BR) ); }
void Mosaic::PDF_Lep::SetLocalBin(const double Y) { localBin = Y; }

Mosaic::PDF_Pion::PDF_Pion(double _a2, double _BR): a2(_a2), BR(_BR) { ; }
Mosaic::PDF_Pion::~PDF_Pion() { ; }
std::pair<double, double> Mosaic::PDF_Pion::Prob(double X, double Energy, double MisM)  {
  MOSAIC_UNUSED_VARIABLE(MisM);
  return std::move( AlgorithmManager::PDF_Pion(a2, X, Energy, BR) ); }
void Mosaic::PDF_Pion::SetLocalBin(const double Y) { localBin = Y; }

Mosaic::PDF_Spline::PDF_Spline( double _lowX, double _dX, double _lowY, double _dY, double _BR, std::initializer_list< SplineSet > _MEP_SplineSet, std::initializer_list< SplineSet > _MEM_SplineSet):
  lowX(_lowX), dX(_dX), lowY(_lowY), dY(_dY), BR(_BR), MEP_SplineSet(_MEP_SplineSet), MEM_SplineSet(_MEM_SplineSet), SetSize(MEP_SplineSet.size()){ ; }
Mosaic::PDF_Spline::~PDF_Spline(){ ; }
double Mosaic::PDF_Spline::BranchingRatio()    const { return BR; }
std::pair<double, double> Mosaic::PDF_Spline::Prob(double X, double Energy, double MisM) {  
  MOSAIC_UNUSED_VARIABLE(Energy);
  MOSAIC_UNUSED_VARIABLE(MisM);
  return std::move( std::make_pair( AlgorithmManager::CheckProb( (MEP_SplineSet.at( localBin )).Value(X) ), AlgorithmManager::CheckProb( (MEM_SplineSet.at( localBin )).Value(X) ) ) );
}
int    Mosaic::PDF_Spline::BinX(const double X)      const { return int((X - lowX)/dX); }
int    Mosaic::PDF_Spline::BinY_impl(const int s)    const { return (s > SetSize) ? SetSize : (s < 0) ? 0 : s; }
int    Mosaic::PDF_Spline::BinY(const double Y)      const { return BinY_impl(  int((Y - lowY)/dY)  ); }
void   Mosaic::PDF_Spline::SetLocalBin(const double Y) { localBin = BinY(Y); }

#ifdef MOSAIC_USE_DEVELOP
Mosaic::PDF_LHAPDF::PDF_LHAPDF( std::string _pdfSetName ) : pdfSetName(_pdfSetName){
  pdf_LHAPDF = LHAPDF::mkPDF( pdfSetName );
}
Mosaic::PDF_LHAPDF::~PDF_LHAPDF( ){
  delete pdf_LHAPDF;
}
double Mosaic::PDF_LHAPDF::get_gg_logWeight(double x1, double x2, double q2){
  //std::cout <<" x1 / x2 / q2 is   "<< x1 << " / "<< x2 <<" / "<< q2 << std::endl;
  
  return ( x1 < 0.0 || x1 > 1.0 || x2 < 0.0 || x2 > 1.0) ? -2e+10 :  Mosaic::AlgorithmManager::ProbLog_impl( log(pdf_LHAPDF->xfxQ2(Mosaic::Constant::pid_gluon, x1, q2)) + log(pdf_LHAPDF->xfxQ2(Mosaic::Constant::pid_gluon, x2, q2)) - log(x1) - log(x2) );
}
#endif
#endif // MOSAIC_PDFMAGAGER_C
