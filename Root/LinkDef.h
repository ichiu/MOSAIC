#ifndef __MOSAIC__
#define __MOSAIC__

#include "MOSAIC/xMOSAICalculator.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ namespace Mosaic;
#pragma link C++ class xMOSAICalculator;

#endif
#endif
