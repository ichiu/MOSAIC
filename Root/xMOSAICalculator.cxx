// Dear emacs, this is -*- c++ -*- 
/*
 * xMosaicalculator
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#include "MOSAIC/xMOSAICalculator.h"

#ifndef MOSAIC_XMOSAICALCULATOR_C
#define MOSAIC_XMOSAICALCULATOR_C

xMOSAICalculator::xMOSAICalculator( const std::string& name ) : asg::AsgTool(name)
{
  declareProperty("CalibType",              m_calibType             = "MC15_50nsec_v1",         "Calibration                    Type : Defaut is MC15_50nsec version 1 tuning" );
  declareProperty("METType",                m_METType               = "MET_TST",                "MissingET                      Type : Defaut is MET_TST" );
  declareProperty("LikelihoodType",         m_LLHType               = "DEFAULT",                "Likelihood                     Type : Defaut is DEFAULT" );
  declareProperty("MET_Final_Name",         m_MET_Final_Name        = "Final",                  "Name of MET Final              Name : Defaut is Final" );
  declareProperty("MET_SoftTerm_Name",      m_MET_SoftTerm_Name     = "PVSoftTrk",              "Name of MET Soft Term          Name : Defaut is PVSoftTrk" );
  declareProperty("LHAPDF_Type",            m_LHAPDF_Type           = "NNPDF23_nlo_as_0118/0",  "Name LHAPDF Set                Name : Defaut is NNPDF23_nlo_as_0119/0" );
  declareProperty("RunMode",                m_RunMode               = "NOMINAL",                "Name Running Mode              Name : Defaut is NOMINAL" );
  declareProperty("JVTKey",                 m_JVTKey                = "Jvt",                    "Name JVT Key for auxdata       Name : Defaut is Jvt" );
  declareProperty("RandomSeed",             m_RandomSeed            = 1,                        "Random Seed                    Int  : Defalt is 1" );
  declareProperty("DeltaMETThresold",       m_DeltaMETThresold      = 9.9e+10,                 "Delta MET Thresold          Double  : Defalt is 9.9e+10" );
  declareProperty("METProbScale",           m_METProbScale          = 100.0,                    "MET Prob Penalty            Double  : Defalt is 100.0" );
  declareProperty("UseBetter_MET",          m_isUseBetter_MET       = true,                     "Using Better MET               Flag : Defaut is True" );
  declareProperty("DoAllVar",               m_isDoAllVar            = false,                    "Calculate All Variables        Flag : Defaut is False" );
  declareProperty("TruthStudy",             m_isTruthStudy          = false,                    "Truth Study                    Flag : Defaut is False" );
  declareProperty("ForceNJetInclusive",     m_isForceNJetInclusive  = false,                    "Force Inclusive nJet           Flag : Defaut is False" );
  //declareProperty("MuonResolutionTool",     m_muonResolutionTool,                               "Muon     Resolution Tool");
  //declareProperty("ElectronResolutionTool", m_elecResolutionTool,                               "Electron Resolution Tool");
  //declareProperty("ResolutionStudyValue",   m_ResolutionStudyValue  = 1.0,                      "Value for Resolution Study   Double : Defalt is 1.0" );
  
}
xMOSAICalculator::xMOSAICalculator(const xMOSAICalculator& other) : asg::AsgTool(other.name() + "_copy") { ; } 

StatusCode xMOSAICalculator::initialize(){
  ATH_MSG_INFO("Initialize xMOSAICalculator");
  
  m_mosaic = new MOSAICalculator( m_LHAPDF_Type );
  std::for_each( std::begin(m_calibType),          std::end(m_calibType),           [](char& c) { c = std::use_facet< std::ctype< char > >( std::locale() ).toupper( c ); } );
  std::for_each( std::begin(m_LLHType),            std::end(m_LLHType),             [](char& c) { c = std::use_facet< std::ctype< char > >( std::locale() ).toupper( c ); } );
  std::for_each( std::begin(m_RunMode),            std::end(m_RunMode),             [](char& c) { c = std::use_facet< std::ctype< char > >( std::locale() ).toupper( c ); } );
  
  if     (m_calibType == "MC15_50NS_V1" ) m_mosaic->inputManager->Set_CalibType( Mosaic::CalibType::MC15_50ns_v1 ); 
  else {
    ATH_MSG_ERROR("Calibration Type is not Known!! : " << m_calibType );
    return StatusCode::FAILURE;
  }
  
  if     ( m_RunMode == "NOMINAL" )      m_mosaic->setRunMode( "NOMINAL" );
  else if( m_RunMode == "FAST" )         m_mosaic->setRunMode( "FAST" );
  
  if     ( m_METType == "MET_TST" )      m_mosaic->inputManager->Set_METType( Mosaic::MET_Type::MET_TST );
  else if( m_METType == "MET_MPT" )      m_mosaic->inputManager->Set_METType( Mosaic::MET_Type::MET_MPT );
  else if( m_METType == "MET_Truth" )    m_mosaic->inputManager->Set_METType( Mosaic::MET_Type::MET_Truth );
  else if( m_METType == "MET_Smear" )    m_mosaic->inputManager->Set_METType( Mosaic::MET_Type::MET_Smear );
  else if( m_METType == "MET_TST_Soft" ) m_mosaic->inputManager->Set_METType( Mosaic::MET_Type::MET_TST_Soft );
  else {
    ATH_MSG_ERROR("MissingET Type is not Known!! : " << m_METType );
    return StatusCode::FAILURE;
  }
  if      ( m_RandomSeed == 0 ) m_RandomSeed = 1;
  else if ( m_RandomSeed  < 0 ) m_RandomSeed = -m_RandomSeed;
  
  m_mosaic->inputManager->Set_Use_Better_MET( m_isUseBetter_MET );
  m_mosaic->inputManager->Set_DoAllVar( m_isDoAllVar );
  m_mosaic->inputManager->Set_TruthStudy( m_isTruthStudy );
  m_mosaic->inputManager->Set_ForceInclusiveNJet( m_isForceNJetInclusive );
  m_mosaic->inputManager->Set_SeedType( m_RandomSeed );
  m_mosaic->inputManager->Set_DeltaMETThresold( m_DeltaMETThresold );
  m_mosaic->inputManager->Set_METProbScale( m_METProbScale );
  
  //auto muon_st_code = m_muonResolutionTool.retrieve() ;
  //auto elec_st_code = m_elecResolutionTool.retrieve() ;
  //isOK_retrieve_muon = ( muon_st_code.isSuccess() ) ? true : false;
  //isOK_retrieve_elec = ( elec_st_code.isSuccess() ) ? true : false;
  //isOK_retrieve_tau  = false;
  m_mosaic->setLikelihoodType( m_LLHType );
  
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::Clear(){
  m_mosaic->Clear();
  m_eta1 = m_eta2 = m_phi1 = m_phi2 = 0.0;
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObj(const xAOD::JetContainer* Jets){
  for(const auto& jet : *Jets ){
    if( Mosaic::AlgorithmManager::DeltaR(m_eta1, m_phi1, jet->eta(), jet->phi() ) < 0.2 ) continue;
    if( Mosaic::AlgorithmManager::DeltaR(m_eta2, m_phi2, jet->eta(), jet->phi() ) < 0.2 ) continue;
    auto jvt   = jet->auxdata< float >( m_JVTKey );
    m_mosaic->inputManager->AddJet(jet->pt(), jet->eta(), jvt, jet->px(), jet->py(), jet->pz(), jet->e(), jet->m(), 0.0);
  }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetTruthMET(const xAOD::MissingET          * xMET){
  m_mosaic->inputManager->SetMissingEt( xMET->mpx(), xMET->mpy(), xMET->sumet() );
  m_mosaic->inputManager->SetMissingEt_SoftTerm( 0.0, 0.0, 0.0 );
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetTruthMET(const xAOD::MissingETContainer * xMETs){
  const auto& xMET = (*xMETs)[ std::string( "NonInt" ) ] ;
  return SetTruthMET( xMET );
}
StatusCode xMOSAICalculator::SetObj(const xAOD::MissingETContainer *xMET){
  const auto& xFinal      = (*xMET)[ std::string( m_MET_Final_Name    ) ] ;
  const auto& xSoft       = (*xMET)[ std::string( m_MET_SoftTerm_Name ) ] ;
  m_mosaic->inputManager->SetMissingEt(          xFinal->mpx(), xFinal->mpy(), xFinal->sumet() ); 
  m_mosaic->inputManager->SetMissingEt_SoftTerm( xSoft->mpx(),  xSoft->mpy(),  xSoft->sumet()  ); 
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObj(const xAOD::MissingET          *xMET){
  m_mosaic->inputManager->SetMissingEt(          xMET->mpx(), xMET->mpy(), xMET->sumet() ); 
  m_mosaic->inputManager->SetMissingEt_SoftTerm( 0.0, 0.0, 0.0 );
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObj(int iTau, const xAOD::TauJet *xTau ){
  if ( xTau == nullptr ) return StatusCode::FAILURE;
  
  int type = -1;
  if( ! xTau->panTauDetail(xAOD::TauJetParameters::PanTauDetails::PanTau_DecayMode, type) ){
    ATH_MSG_DEBUG("PanTau Decay Mode is not found!!!");
    type = (int)Mosaic::ParticleType::None;
  }
  const auto& vtx = xTau->vertex();
  if( vtx == nullptr ) {
    ATH_MSG_DEBUG("Vertex associated to tau is not found!!!");
    return StatusCode::FAILURE;
  }
  
  for(int i = 0, end = xTau->nTracks(); i < end; ++i){
    if(!(xTau->tauTrackLinks().at(i).isValid())) continue;
    const auto& trk = xTau->track(i);
    if( trk == nullptr ) continue;
    const auto& v = trk->p4();
    m_mosaic->inputManager->addCharged(iTau, v.Px(), v.Py(), v.Pz(), v.E(), Mosaic::Constant::MPI, trk->track()->charge() );
  }
  for(int i = 0, end = xTau->nPi0PFOs(); i < end; ++i){
    const auto& pfo = xTau->pi0PFO(i);
    if( pfo == nullptr ) continue;
    if( pfo->pt()*Mosaic::Constant::_MeV2GeV < 3.0 ) continue;
    if( pfo->isAvailable<float>( "centerMag" ) ) { 
      const auto& v = pfo->GetVertexCorrectedFourVec( *vtx );
      m_mosaic->inputManager->addNeutral(iTau, v.Px(), v.Py(), v.Pz(), v.E(), Mosaic::Constant::MPI0, pfo->bdtPi0Score() ) ;
    }else{
      const auto& v = pfo->p4();
      m_mosaic->inputManager->addNeutral(iTau, v.Px(), v.Py(), v.Pz(), v.E(), Mosaic::Constant::MPI0, pfo->bdtPi0Score() ) ;
      ATH_MSG_DEBUG("centerMag is not Available!!!");
    }      
  }
  for(int i = 0, end = xTau->nNeutralPFOs(); i < end; ++i){
    const auto& pfo = xTau->neutralPFO(i);
    if( pfo == nullptr ) continue;
    if( pfo->pt()*Mosaic::Constant::_MeV2GeV < 3.0 ) continue;
    if( pfo->isAvailable<float>( "centerMag" ) ) { 
      const auto& v = pfo->GetVertexCorrectedFourVec( *vtx );
      m_mosaic->inputManager->addOtherNeutral(iTau, v.Px(), v.Py(), v.Pz(), v.E(), Mosaic::Constant::MPI0, pfo->bdtPi0Score() ) ;
    }else{
      const auto& v = pfo->p4();
      m_mosaic->inputManager->addOtherNeutral(iTau, v.Px(), v.Py(), v.Pz(), v.E(), Mosaic::Constant::MPI0, pfo->bdtPi0Score() ) ;
      ATH_MSG_DEBUG("centerMag is not Available!!!");
    }      
  }
  
  const auto& tauRec = xTau->p4();
  //const auto& panTau = xTau->p4( xAOD::TauJetParameters::TauCalibType::PanTauCellBased );
  const auto& panTau = xTau->p4( );// Temporary Solution
  double mVis = xTau->m();
  //if( xTau->isAvailable<float>( "mPanTauCellBased" ) ) mVis = xTau->mPanTauCellBased();
  m_mosaic->inputManager->SetVis_tauRec(iTau, tauRec.Px(), tauRec.Py(), tauRec.Pz(), tauRec.E(), mVis, xTau->charge());
  m_mosaic->inputManager->SetVis_panTau(iTau, panTau.Px(), panTau.Py(), panTau.Pz(), panTau.E(), mVis, xTau->charge());
  m_mosaic->inputManager->SetVis       (iTau, tauRec.Px(), tauRec.Py(), tauRec.Pz(), tauRec.E(), mVis, xTau->charge());
  m_mosaic->inputManager->SetDecayType(iTau, type);
  /*
  if( isOK_retrieve_tau  ) m_mosaic->inputManager->SetVisResolution( iTau );
  else                     m_mosaic->inputManager->SetVisResolution( iTau );
  */
  if     ( iTau == 1 ) { m_eta1 = xTau->eta(); m_phi1 = xTau->phi(); }
  else if( iTau == 2 ) { m_eta2 = xTau->eta(); m_phi2 = xTau->phi(); }
  return StatusCode::SUCCESS;
}  

StatusCode xMOSAICalculator::SetObj(int iTau, const xAOD::Electron *xElectron ){
  if ( xElectron == nullptr ) return StatusCode::FAILURE;
  const auto& v = xElectron->p4();
  m_mosaic->inputManager->SetVis(iTau, v.Px(), v.Py(), v.Pz(), xElectron->e(), Mosaic::Constant::MEL, xElectron->charge());
  m_mosaic->inputManager->SetDecayType(iTau, (int)Mosaic::ParticleType::p_Electron );
  /*
  if( isOK_retrieve_elec ) m_mosaic->inputManager->SetVisResolution( iTau, m_elecResolutionTool->getResolution( *xElectron ) );
  else                     m_mosaic->inputManager->SetVisResolution( iTau );
  */
  if     ( iTau == 1 ) { m_eta1 = xElectron->eta(); m_phi1 = xElectron->phi(); }
  else if( iTau == 2 ) { m_eta2 = xElectron->eta(); m_phi2 = xElectron->phi(); }
  return StatusCode::SUCCESS;
}

StatusCode xMOSAICalculator::SetObj(int iTau, const xAOD::Muon *xMuon ){
  if ( xMuon == nullptr ) return StatusCode::FAILURE;
  const auto& v = xMuon->p4();
  m_mosaic->inputManager->SetVis(iTau, v.Px(), v.Py(), v.Pz(), xMuon->e(), Mosaic::Constant::MMU, xMuon->charge());
  m_mosaic->inputManager->SetDecayType(iTau, (int)Mosaic::ParticleType::p_Muon );
  /*
  if( isOK_retrieve_muon ) m_mosaic->inputManager->SetVisResolution( iTau );
  else                     m_mosaic->inputManager->SetVisResolution( iTau );
  */
  if     ( iTau == 1 ) { m_eta1 = xMuon->eta(); m_phi1 = xMuon->phi(); }
  else if( iTau == 2 ) { m_eta2 = xMuon->eta(); m_phi2 = xMuon->phi(); }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObj(int iTau, const xAOD::TruthParticle *xTruth ){
  if( xTruth == nullptr ) return StatusCode::FAILURE;

  if( xTruth->isTau() ){
    bool isHad = false;
    auto nchild = xTruth->nChildren();
    auto sum    = xTruth->p4();
    int ncharged(0), nneutral(0);
    if( nchild == 0 ) return StatusCode::FAILURE;
    for(size_t i = 0; i < nchild; ++i){
      const auto& this_child = xTruth->child( i );
      if( this_child             == nullptr ) { continue; }
      if      ( this_child->isNeutrino() ) { sum -= this_child->p4(); continue; }
      else  if( this_child->isTau()      ) { continue; }
      else  if( this_child->absPdgId() == 11 || this_child->absPdgId() == 13 ) { 
        const auto& v = this_child->p4();
        m_mosaic->inputManager->SetVis(iTau, v.Px(), v.Py(), v.Pz(), v.E(), Mosaic::Constant::MEL, xTruth->charge());
        m_mosaic->inputManager->SetDecayType(iTau, (this_child->absPdgId() == 11) ? (int)Mosaic::ParticleType::p_Electron : (int)Mosaic::ParticleType::p_Muon );
        break;
      }else if( this_child->isCharged() ){
        const auto& v = this_child->p4();
        m_mosaic->inputManager->addCharged(iTau, v.Px(), v.Py(), v.Pz(), v.E(), Mosaic::Constant::MPI, this_child->charge() );
        isHad = true;
        ++ncharged;
      }else if( this_child->isNeutral() ){
        const auto& v = this_child->p4();
        m_mosaic->inputManager->addNeutral(iTau, v.Px(), v.Py(), v.Pz(), v.E(), Mosaic::Constant::MPI0, 0.0 );
        ++nneutral;
      }else{
        ATH_MSG_DEBUG("This is not Neutrino, Lepton, Pion... !!!");
      }
    }
    if( isHad ){
      m_mosaic->inputManager->SetVis_tauRec(iTau, sum.Px(), sum.Py(), sum.Pz(), sum.E(), sum.M(), xTruth->charge());
      m_mosaic->inputManager->SetVis_panTau(iTau, sum.Px(), sum.Py(), sum.Pz(), sum.E(), sum.M(), xTruth->charge());
      m_mosaic->inputManager->SetVis       (iTau, sum.Px(), sum.Py(), sum.Pz(), sum.E(), sum.M(), xTruth->charge());
      m_mosaic->inputManager->SetDecayType(iTau, Mosaic::AlgorithmManager::TauDecayMode(ncharged, nneutral) );
    }
  }  
  return StatusCode::SUCCESS;
}

StatusCode xMOSAICalculator::SetObjects(const xAOD::Electron * xObj1, const xAOD::Electron * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Electron * xObj1, const xAOD::Muon     * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Electron * xObj1, const xAOD::TauJet   * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Muon     * xObj1, const xAOD::Electron * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Muon     * xObj1, const xAOD::Muon     * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Muon     * xObj1, const xAOD::TauJet   * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::TauJet   * xObj1, const xAOD::Electron * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::TauJet   * xObj1, const xAOD::Muon     * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::TauJet   * xObj1, const xAOD::TauJet   * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::TruthParticle * xObj1, const xAOD::TruthParticle * xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetTruthMET(xMET ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}

StatusCode xMOSAICalculator::SetObjects(const xAOD::IParticle* xObj1, const xAOD::IParticle* xObj2, const xAOD::MissingETContainer *xMET, const xAOD::JetContainer* xJets){
  auto type1 = xObj1->type();
  auto type2 = xObj2->type();
  
  if     ( type1 == xAOD::Type::Electron      && type2 == xAOD::Type::Electron       ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Electron*>      (xObj1),  dynamic_cast<const xAOD::Electron*>      (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Electron      && type2 == xAOD::Type::Muon           ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Electron*>      (xObj1),  dynamic_cast<const xAOD::Muon*>          (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Electron      && type2 == xAOD::Type::Tau            ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Electron*>      (xObj1),  dynamic_cast<const xAOD::TauJet*>        (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Muon          && type2 == xAOD::Type::Electron       ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Muon*>          (xObj1),  dynamic_cast<const xAOD::Electron*>      (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Muon          && type2 == xAOD::Type::Muon           ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Muon*>          (xObj1),  dynamic_cast<const xAOD::Muon*>          (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Muon          && type2 == xAOD::Type::Tau            ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Muon*>          (xObj1),  dynamic_cast<const xAOD::TauJet*>        (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Tau           && type2 == xAOD::Type::Electron       ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::TauJet*>        (xObj1),  dynamic_cast<const xAOD::Electron*>      (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Tau           && type2 == xAOD::Type::Muon           ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::TauJet*>        (xObj1),  dynamic_cast<const xAOD::Muon*>          (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Tau           && type2 == xAOD::Type::Tau            ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::TauJet*>        (xObj1),  dynamic_cast<const xAOD::TauJet*>        (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::TruthParticle && type2 == xAOD::Type::TruthParticle  ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::TruthParticle*> (xObj1),  dynamic_cast<const xAOD::TruthParticle*> (xObj2), xMET, xJets); 
  
  ATH_MSG_WARNING("Unkown Type...");
  return StatusCode::FAILURE;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Electron * xObj1, const xAOD::Electron * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Electron * xObj1, const xAOD::Muon     * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Electron * xObj1, const xAOD::TauJet   * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Muon     * xObj1, const xAOD::Electron * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Muon     * xObj1, const xAOD::Muon     * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::Muon     * xObj1, const xAOD::TauJet   * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::TauJet   * xObj1, const xAOD::Electron * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::TauJet   * xObj1, const xAOD::Muon     * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::TauJet   * xObj1, const xAOD::TauJet   * xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
StatusCode xMOSAICalculator::SetObjects(const xAOD::TruthParticle * xObj1, const xAOD::TruthParticle * xObj2, const xAOD::MissingET *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetTruthMET(xMET ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )               { ATH_MSG_WARNING("mosaic->init()     is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}

StatusCode xMOSAICalculator::SetObjects(const xAOD::IParticle* xObj1, const xAOD::IParticle* xObj2, const xAOD::MissingET          *xMET, const xAOD::JetContainer* xJets){
  auto type1 = xObj1->type();
  auto type2 = xObj2->type();
  
  if     ( type1 == xAOD::Type::Electron      && type2 == xAOD::Type::Electron       ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Electron*>      (xObj1),  dynamic_cast<const xAOD::Electron*>      (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Electron      && type2 == xAOD::Type::Muon           ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Electron*>      (xObj1),  dynamic_cast<const xAOD::Muon*>          (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Electron      && type2 == xAOD::Type::Tau            ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Electron*>      (xObj1),  dynamic_cast<const xAOD::TauJet*>        (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Muon          && type2 == xAOD::Type::Electron       ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Muon*>          (xObj1),  dynamic_cast<const xAOD::Electron*>      (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Muon          && type2 == xAOD::Type::Muon           ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Muon*>          (xObj1),  dynamic_cast<const xAOD::Muon*>          (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Muon          && type2 == xAOD::Type::Tau            ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::Muon*>          (xObj1),  dynamic_cast<const xAOD::TauJet*>        (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Tau           && type2 == xAOD::Type::Electron       ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::TauJet*>        (xObj1),  dynamic_cast<const xAOD::Electron*>      (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Tau           && type2 == xAOD::Type::Muon           ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::TauJet*>        (xObj1),  dynamic_cast<const xAOD::Muon*>          (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::Tau           && type2 == xAOD::Type::Tau            ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::TauJet*>        (xObj1),  dynamic_cast<const xAOD::TauJet*>        (xObj2), xMET, xJets); 
  else if( type1 == xAOD::Type::TruthParticle && type2 == xAOD::Type::TruthParticle  ) return xMOSAICalculator::SetObjects( dynamic_cast<const xAOD::TruthParticle*> (xObj1),  dynamic_cast<const xAOD::TruthParticle*> (xObj2), xMET, xJets); 
  
  ATH_MSG_WARNING("Unkown Type...");
  return StatusCode::FAILURE;
}




/*
StatusCode xMOSAICalculator::SetObjects(const T1 * xObj1, const T2 * xObj2, const xAOD::MissingET *xMET, const xAOD::JetContainer* xJets){
  if(  ! SetObj(    xMET  ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xMET  ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj(    xJets ).isSuccess() ) { ATH_MSG_WARNING("SetObj(    xJets ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 1, xObj1 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 1, xObj1 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! SetObj( 2, xObj2 ).isSuccess() ) { ATH_MSG_WARNING("SetObj( 2, xObj2 ) is FAILURE!! ");  return StatusCode::FAILURE; }
  if(  ! m_mosaic->init() )                      { ATH_MSG_WARNING("mosaic->init()  is FAILURE!! ");  return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
*/
#endif // MOSAIC_XMOSAICALCULATOR_C
