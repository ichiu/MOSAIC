// Dear emacs, this is -*- c++ -*-
/*
 * IOManager 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#include "MOSAIC/IOManager.h"

#ifndef MOSAIC_IOMANAGER_C
#define MOSAIC_IOMANAGER_C

//+++++++++++++++++++++++++++++++++++++++++++++++  InputManager ++++++++++++++++++++++++++++++++++++++++++++++++++++//
Mosaic::InputManager::InputManager():
  vis1(), vis2()
{
  seedType = 1;
  nJet_Jet = -1;
  met_nJet = Mosaic::MET_nJet::nJetInclusive;
  met_Type = Mosaic::MET_Type::MET_TST;
  MET_Used.clear();
  MET_MPT_Final.clear();
  MET_TST_Final.clear();
  MET_TST_SoftTerm.clear();
  calib_Type = Mosaic::CalibType::MC15_50ns_v1;
  isSwap = 0;
  isUseBetter_MET      = false;
  isDoAllVar           = false;
  isForceNJetInclusive = false;
  isTruthStudy         = false;
  resolutionScale      = 1.0;
  deltaMETThresold     = +9.9e+10;
  mETProbScale         = 0;
  cM_1 = cM_2 = 0.0;
  Jets                   = new std::vector< Mosaic::Jet* >;
  this->Clear();
}
Mosaic::InputManager::~InputManager() {
  Mosaic::InputManager::Clear();
}

void Mosaic::InputManager::Clear(){
  nJet_Jet = -1;
  met_nJet = Mosaic::MET_nJet::nJetInclusive;
  met_Type_Used = Mosaic::MET_Type::MET_TST;
  resolutionScale      = 1.0;
  cM_1 = cM_2 = 0.0;
  isSwap = 0;
  vis1.Clear();
  vis2.Clear();
  MET_Used.clear();
  MET_MPT_Final.clear();
  MET_TST_Final.clear();
  MET_TST_SoftTerm.clear();
  
  for(auto& Jet     : *Jets) delete Jet;
  Jets->clear();
}

bool Mosaic::InputManager::init(){
  
  std::seed_seq seq{ (std::uint_least32_t) fabs(this->akaRandom()) };
  Engine  = std::mt19937( seq );
  Uniform = std::uniform_real_distribution<double> ( 0.0, 1.0 ) ;
  
  vis1.init(Engine, Uniform);
  vis2.init(Engine, Uniform);
  cM_1 = (vis1.e() + vis2.e() + vis1.pz() + vis2.pz())/Mosaic::Constant::ECM_13TeV_2;
  cM_2 = (vis1.e() + vis2.e() - vis1.pz() - vis2.pz())/Mosaic::Constant::ECM_13TeV_2;
  
  if     ( vis1.charge() == 1 && vis2.charge() == -1){ Mosaic::InputManager::Swap12(); }
  else if( vis1.charge() ==      vis2.charge()      ){ 
    if( vis1.pt() > vis2.pt() ) Mosaic::InputManager::Swap12(); 
  }
  
  // nJet Setting
  if     ( nJet_Jet == 0 ) met_nJet = Mosaic::MET_nJet::nJet0;
  else if( nJet_Jet >= 1 ) met_nJet = Mosaic::MET_nJet::nJet1;
  else                     met_nJet = Mosaic::MET_nJet::nJetInclusive;

  //std::cout << "-------------------------- IsForceNJetInclusive is " << IsForceNJetInclusive() << std::endl;
  if( IsForceNJetInclusive() ) { 
    //std::cout <<" This is ForceInclusiveNJet Setting "<< std::endl;
    met_nJet = Mosaic::MET_nJet::nJet0;
  }
  // sumEt correction
  double tmp_sumEt = MET_TST_Final.SumEt() - vis1.pt() - vis2.pt();
  //std::cout <<" SumEt is " << tmp_sumEt << " = "<< MET_TST_Final.SumEt() << " - " << vis1.pt() << " - " << vis2.pt() << std::endl;
  tmp_sumEt =  ( tmp_sumEt <= 1000.0 ) ? 1000. : tmp_sumEt;
  MET_TST_Final.set(MET_TST_Final.MEx(), MET_TST_Final.MEy(), tmp_sumEt);
  double exMis_MPT = vis1.px() + vis2.px();
  double eyMis_MPT = vis1.py() + vis2.py();
  for(auto& Jet : *Jets){
    exMis_MPT += Jet->px();
    eyMis_MPT += Jet->py();
  }
  exMis_MPT = -exMis_MPT;
  eyMis_MPT = -eyMis_MPT;
  double sumEt_MPT = tmp_sumEt;
  MET_MPT_Final.set(exMis_MPT, eyMis_MPT, sumEt_MPT);

  //std::cout <<" MOSAIC::MET_MPT is " << MET_MPT_Final.MEx() << " / "<< MET_MPT_Final.MEy() <<" ----> "<< std::hypot(MET_MPT_Final.MEx(), MET_MPT_Final.MEy()) << std::endl;
  
  if( isTruthStudy ){
    met_Type_Used = met_Type;
    MET_Used      = MET_TST_Final;
  }else if ( isUseBetter_MET ){
    auto nJet = met_nJet;
    double sig1_TST = Mosaic::AlgorithmManager::MET_Resolution(MET_TST_Final.SumEt(), calib_Type, Mosaic::MET_Type::MET_TST, Mosaic::MET_Resol_Par::MET_Sigma1,   nJet);
    double sig2_TST = Mosaic::AlgorithmManager::MET_Resolution(MET_TST_Final.SumEt(), calib_Type, Mosaic::MET_Type::MET_TST, Mosaic::MET_Resol_Par::MET_Sigma2,   nJet);
    double frac_TST = Mosaic::AlgorithmManager::MET_Resolution(MET_TST_Final.SumEt(), calib_Type, Mosaic::MET_Type::MET_TST, Mosaic::MET_Resol_Par::MET_Fraction, nJet, true);
    
    double sig1_MPT = Mosaic::AlgorithmManager::MET_Resolution(MET_TST_Final.SumEt(), calib_Type, Mosaic::MET_Type::MET_MPT, Mosaic::MET_Resol_Par::MET_Sigma1,   nJet);
    double sig2_MPT = Mosaic::AlgorithmManager::MET_Resolution(MET_TST_Final.SumEt(), calib_Type, Mosaic::MET_Type::MET_MPT, Mosaic::MET_Resol_Par::MET_Sigma2,   nJet);
    double frac_MPT = Mosaic::AlgorithmManager::MET_Resolution(MET_TST_Final.SumEt(), calib_Type, Mosaic::MET_Type::MET_MPT, Mosaic::MET_Resol_Par::MET_Fraction ,nJet, true);
    
    double resol_at0_TST = frac_TST/sig1_TST + (1.0-frac_TST)/sig2_TST;
    double resol_at0_MPT = frac_MPT/sig1_MPT + (1.0-frac_MPT)/sig2_MPT;
    if( resol_at0_TST < resol_at0_MPT ){// This is OK, See 2D plot!!!
      met_Type_Used = Mosaic::MET_Type::MET_MPT; 
      MET_Used      = MET_MPT_Final;
    }else{
      met_Type_Used = Mosaic::MET_Type::MET_TST; 
      MET_Used      = MET_TST_Final;
    }
  }else{
    if( met_Type == Mosaic::MET_Type::MET_MPT ){
      met_Type_Used = Mosaic::MET_Type::MET_MPT; 
      MET_Used      = MET_MPT_Final;
    }else if( met_Type == Mosaic::MET_Type::MET_TST_Soft ){
      met_Type_Used = Mosaic::MET_Type::MET_TST_Soft; 
      MET_Used      = MET_TST_SoftTerm;
    }else{
      met_Type_Used = Mosaic::MET_Type::MET_TST; 
      MET_Used      = MET_TST_Final;
    }
  }
  return true;
}
void Mosaic::InputManager::Swap12(){
  vis1.swap( vis2 );
  isSwap = 1;
}
void Mosaic::InputManager::Set_METType( Mosaic::MET_Type mettype){ met_Type = mettype; }
void Mosaic::InputManager::Set_CalibType( Mosaic::CalibType this_Type ) { calib_Type = this_Type; }
void Mosaic::InputManager::Set_Use_Better_MET(bool i){ isUseBetter_MET = i; }
void Mosaic::InputManager::Set_DoAllVar(bool i){ isDoAllVar = i; }
void Mosaic::InputManager::Set_TruthStudy(bool i){ isTruthStudy = i; }
void Mosaic::InputManager::Set_SeedType(int _seedType){ seedType = _seedType; }
void Mosaic::InputManager::Set_ForceInclusiveNJet(bool i){ isForceNJetInclusive = i; }
void Mosaic::InputManager::Set_ResolutionScale(double v){ resolutionScale = v; }
void Mosaic::InputManager::Set_DeltaMETThresold(double v){ deltaMETThresold = v; }
void Mosaic::InputManager::Set_METProbScale(double v){ mETProbScale = v; }
void Mosaic::InputManager::SetMissingEt(double exmiss, double eymiss, double sumet, double mu) { MET_TST_Final.set(exmiss, eymiss, sumet); agvPerX = mu; }
void Mosaic::InputManager::SetMissingEt_SoftTerm(double exmiss, double eymiss, double sumet)   { MET_TST_SoftTerm.set(exmiss, eymiss, sumet); }
void Mosaic::InputManager::SetDecayType(int iTau, int t){ 
  if      (iTau==1) vis1.decayType = (t > 0 ) ? (Mosaic::ParticleType)t : Mosaic::ParticleType::None;
  else if (iTau==2) vis2.decayType = (t > 0 ) ? (Mosaic::ParticleType)t : Mosaic::ParticleType::None;
}
void Mosaic::InputManager::SetVis_panTau(int iTau, double px, double py, double pz, double e, double m, double charge ) {
  if      (iTau==1) vis1.panTau.set(px, py, pz, e, m, charge); 
  else if (iTau==2) vis2.panTau.set(px, py, pz, e, m, charge); 
}
void Mosaic::InputManager::SetVis_tauRec(int iTau, double px, double py, double pz, double e, double m, double charge ) {
  if      (iTau==1) vis1.tauRec.set(px, py, pz, e, m, charge); 
  else if (iTau==2) vis2.tauRec.set(px, py, pz, e, m, charge); 
}
void Mosaic::InputManager::SetVis(int iTau, double px, double py, double pz, double e, double m, double charge ) {
  if      (iTau==1) vis1.set(px, py, pz, e, m, charge); 
  else if (iTau==2) vis2.set(px, py, pz, e, m, charge); 
}

void Mosaic::InputManager::addCharged(int iTau, double px, double py, double pz, double e, double m, double charge){
  if      (iTau==1) vis1.chargedParticles->emplace_back(new FourVector(px, py, pz, e, m, charge) ) ;
  else if (iTau==2) vis2.chargedParticles->emplace_back(new FourVector(px, py, pz, e, m, charge) ) ;
}
void Mosaic::InputManager::addNeutral(int iTau, double px, double py, double pz, double e, double m, double score){
  if      (iTau==1) vis1.neutralParticles->emplace_back(new FourVector(px, py, pz, e, m, 0.0, score) ) ;
  else if (iTau==2) vis2.neutralParticles->emplace_back(new FourVector(px, py, pz, e, m, 0.0, score) ) ;
}

void Mosaic::InputManager::addOtherNeutral(int iTau, double px, double py, double pz, double e, double m, double score){
  if       (iTau==1) vis1.neutralOtherParticles->emplace_back(new FourVector(px, py, pz, e, m, 0.0, score) ) ;
  else if  (iTau==2) vis2.neutralOtherParticles->emplace_back(new FourVector(px, py, pz, e, m, 0.0, score) ) ;
}
void Mosaic::InputManager::AddJet(double pt, double eta, double jvt, double px, double py, double pz, double e, double m, double charge){
  if( pt * Mosaic::Constant::_MeV2GeV > Mosaic::Constant::pt_min_JVT ){
    if     ( pt * Mosaic::Constant::_MeV2GeV > Mosaic::Constant::pt_max_JVT )                  { ++nJet_Jet; Jets->emplace_back( new Mosaic::Jet(px, py, pz, e, m, charge) ); }
    else if( fabs(eta) < Mosaic::Constant::eta_max_JVT && jvt > Mosaic::Constant::jvt_min_JVT) { ++nJet_Jet; Jets->emplace_back( new Mosaic::Jet(px, py, pz, e, m, charge) ); }
  }
}
bool Mosaic::InputManager::IsForceNJetInclusive()               const { return isForceNJetInclusive; }
bool Mosaic::InputManager::DoAllVar()                           const { return isDoAllVar; }
int Mosaic::InputManager::IsSwap()                              const { return isSwap; }
Mosaic::MET_nJet    Mosaic::InputManager::nJet()                const { return met_nJet; }
Mosaic::MET_Type    Mosaic::InputManager::Get_MET_Type()        const { return met_Type_Used; }
Mosaic::CalibType   Mosaic::InputManager::Get_CalibType()       const { return calib_Type; }
Mosaic::ChannelType Mosaic::InputManager::Get_Channel()         const { return (vis1.IsLep() && vis2.IsLep()) ? Mosaic::ChannelType::LEPLEP : ( (vis1.IsLep() && !vis2.IsLep()) || (!vis1.IsLep() && vis2.IsLep()) ) ? Mosaic::ChannelType::LEPHAD : Mosaic::ChannelType::HADHAD; }
Mosaic::MissingEt   Mosaic::InputManager::MET_USED()            const { return MET_Used; }
double Mosaic::InputManager::ObsMu()                            const { return agvPerX; }
double Mosaic::InputManager::akaRandom()                        const { return pow( pow(fabs(vis1.phi() - vis2.phi()),2)*2015.0 + pow(fabs(vis1.eta() - vis2.eta()),2)*13000.0, seedType ) ; }
double Mosaic::InputManager::ResolutionScale()                  const { return resolutionScale; }
double Mosaic::InputManager::DeltaMETThresold()                 const { return deltaMETThresold; }
double Mosaic::InputManager::mVis()                             const { return Mosaic::AlgorithmManager::InvMass( vis1.px()+vis2.px(), vis1.py()+vis2.py(), vis1.pz()+vis2.pz(), vis1.e()+vis2.e() ); }
double Mosaic::InputManager::Resoltion_Vis1()                   const { return 10.0; }
double Mosaic::InputManager::Resoltion_Vis2()                   const { return 10.0; }
double Mosaic::InputManager::CM_1()                             const { return cM_1; }
double Mosaic::InputManager::CM_2()                             const { return cM_2; }
//double Mosaic::InputManager::METProbScale()                     const { return 1.0 / ( 1.0 + exp( - mETProbScale * MET_Used.Sig()) + 5.0 ) ; } // Something like Sigmoid form 
double Mosaic::InputManager::METProbScale()                     const { return 1.0 ; }
  

Mosaic::FourVector Mosaic::InputManager::Vis1() const { return vis1.Vis(); }
Mosaic::FourVector Mosaic::InputManager::Vis2() const { return vis2.Vis(); }


std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> Mosaic::InputManager::get_seed_seq() { return Mosaic::AlgorithmManager::get_seed_seq(Engine, Uniform); }



//+++++++++++++++++++++++++++++++++++++++++++++++  OutputManager ++++++++++++++++++++++++++++++++++++++++++++++++++++//


Mosaic::OutputManager::OutputManager(){ Clear(); }
Mosaic::OutputManager::~OutputManager(){ Clear(); }

void Mosaic::OutputManager::Clear(){   
  isSwap = 0;
  _MET_Sigma1 = _MET_Sigma2 = _MET_Frac = 0.0;
  Mass_SigmaCorrLow   = std::make_pair(0.0, 0.0);
  Mass_SigmaCorrHigh  = std::make_pair(0.0, 0.0);
  Mass_SigmaCorrComb1 = std::make_pair(0.0, 0.0);
  Mass_SigmaCorrComb2 = std::make_pair(0.0, 0.0);
  
  
  
  Mass_Mtt_Sigma = std::make_pair(0.0, 0.0);
  Mass_Mtt_Mean  = std::make_pair(0.0, 0.0);
  Mass_Mtt_Medi  = std::make_pair(0.0, 0.0);
  Mass_Mtt_Mode  = std::make_pair(0.0, 0.0);
  Mass_X12_Mean  = std::make_pair(0.0, 0.0);
  Mass_X12_Medi  = std::make_pair(0.0, 0.0);
  Mass_X12_Mode  = std::make_pair(0.0, 0.0);
  Mass_Mis_Mean  = std::make_pair(0.0, 0.0);
  Mass_Mis_Medi  = std::make_pair(0.0, 0.0);
  Mass_Mis_Mode  = std::make_pair(0.0, 0.0);
  Mis1_Mean = std::make_pair(Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) );
  Mis1_Medi = std::make_pair(Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) );
  Mis1_Mode = std::make_pair(Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) );
  Mis2_Mean = std::make_pair(Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) );
  Mis2_Medi = std::make_pair(Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) );
  Mis2_Mode = std::make_pair(Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), Mosaic::FourVector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) );

  std::vector< Mosaic::SamplingPoint >( std::move( samplingPoints ) ).swap(samplingPoints);
  samplingPoints.clear();
  
}
void Mosaic::OutputManager::SetIsSwap(int f) { isSwap = f; }
void Mosaic::OutputManager::SetMETParameter(double s1, double s2, double f){ _MET_Sigma1 = s1; _MET_Sigma2 = s2; _MET_Frac = f; }
double Mosaic::OutputManager::MET_Sigma1()   const { return _MET_Sigma1; }
double Mosaic::OutputManager::MET_Sigma2()   const { return _MET_Sigma2; }
double Mosaic::OutputManager::MET_Fraction() const { return _MET_Frac; }
std::pair<Mosaic::FourVector, Mosaic::FourVector> Mosaic::OutputManager::getMisVector( std::pair<double, double> px, std::pair<double, double> py, std::pair<double, double> pz ){ 
  return std::make_pair( FourVector(px.first, py.first, pz.first, sqrt(px.first*px.first + py.first*py.first + pz.first*pz.first), 0.0, 0.0, 0.0),
                         FourVector(px.second, py.second, pz.second, sqrt(px.second*px.second + py.second*py.second + pz.second*pz.second), 0.0, 0.0, 0.0) );
}
std::pair<double, double> Mosaic::OutputManager::get_InvMass(Mosaic::FourVector vis1, Mosaic::FourVector vis2, std::pair<Mosaic::FourVector, Mosaic::FourVector> mis1, std::pair<Mosaic::FourVector, Mosaic::FourVector> mis2){
  return std::make_pair(Mosaic::AlgorithmManager::InvMass( vis1.px() + vis2.px() + mis1.first.px() + mis2.first.px(),
                                                           vis1.py() + vis2.py() + mis1.first.py() + mis2.first.py(),
                                                           vis1.pz() + vis2.pz() + mis1.first.pz() + mis2.first.pz(),
                                                           vis1.e()  + vis2.e()  + mis1.first.e()  + mis2.first.e() ),
                        Mosaic::AlgorithmManager::InvMass( vis1.px() + vis2.px() + mis1.second.px() + mis2.second.px(),
                                                           vis1.py() + vis2.py() + mis1.second.py() + mis2.second.py(),
                                                           vis1.pz() + vis2.pz() + mis1.second.pz() + mis2.second.pz(),
                                                           vis1.e()  + vis2.e()  + mis1.second.e()  + mis2.second.e() )
                        );
}

double Mosaic::OutputManager::get_mass(int isWeight, bool isGeV, std::pair<double, double> p) const { return ( isGeV ) ? ( (isWeight == 0) ? p.first*Mosaic::Constant::_MeV2GeV : p.second*Mosaic::Constant::_MeV2GeV ) : ((isWeight == 0) ? p.first : p.second) ; }
double Mosaic::OutputManager::GetMass_mtt_Mean(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_Mtt_Mean); }
double Mosaic::OutputManager::GetMass_mtt_Medi(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_Mtt_Medi); }
double Mosaic::OutputManager::GetMass_mtt_Mode(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_Mtt_Mode); }
double Mosaic::OutputManager::GetMass_X12_Mean(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_X12_Mean); }
double Mosaic::OutputManager::GetMass_X12_Medi(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_X12_Medi); }
double Mosaic::OutputManager::GetMass_X12_Mode(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_X12_Mode); }
double Mosaic::OutputManager::GetMass_Mis_Mean(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_Mis_Mean); }
double Mosaic::OutputManager::GetMass_Mis_Medi(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_Mis_Medi); }
double Mosaic::OutputManager::GetMass_Mis_Mode(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_Mis_Mode); }
double Mosaic::OutputManager::GetMass_SigmaCorrLow(int isWeight, bool isGeV)   const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_SigmaCorrLow); }
double Mosaic::OutputManager::GetMass_SigmaCorrHigh(int isWeight, bool isGeV)  const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_SigmaCorrHigh); }
double Mosaic::OutputManager::GetMass_SigmaCorrComb1(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_SigmaCorrComb1); }
double Mosaic::OutputManager::GetMass_SigmaCorrComb2(int isWeight, bool isGeV) const { return Mosaic::OutputManager::get_mass( isWeight, isGeV, Mass_SigmaCorrComb2); }

void Mosaic::OutputManager::EstimateMass_mtt_Mean( std::vector<Mosaic::SamplingPoint>& ResultPoints, double sumOf_Weight){
  auto ret = Mosaic::AlgorithmManager::Mean1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.mass() ; } , sumOf_Weight);
  Mass_Mtt_Mean  = ret.first;
  Mass_Mtt_Sigma = ret.second;
}
void Mosaic::OutputManager::EstimateMass_mtt_Medi( std::vector<Mosaic::SamplingPoint>& ResultPoints, double sumOf_Weight){
  Mass_Mtt_Medi  = Mosaic::AlgorithmManager::Medi1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.mass() ; } , sumOf_Weight);
}
void Mosaic::OutputManager::EstimateMass_mtt_Mode(std::vector<Mosaic::SamplingPoint>& ResultPoints){
  Mass_Mtt_Mode  = Mosaic::AlgorithmManager::Mode1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.mass() ; } );
}
void Mosaic::OutputManager::EstimateMass_X12_Mean( std::vector<Mosaic::SamplingPoint>& ResultPoints, double mVis, double sumOf_Weight){
  auto ret       = Mosaic::AlgorithmManager::Mean1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.x12() ; } , sumOf_Weight);
  auto x12_Mean  = ret.first;
  Mass_X12_Mean  = std::make_pair( mVis/sqrt(x12_Mean.first), mVis/sqrt(x12_Mean.second) );
}
void Mosaic::OutputManager::EstimateMass_X12_Medi( std::vector<Mosaic::SamplingPoint>& ResultPoints, double mVis, double sumOf_Weight){
  auto x12_Medi  = Mosaic::AlgorithmManager::Medi1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.x12() ; } , sumOf_Weight);
  Mass_X12_Medi  = std::make_pair( mVis/sqrt(x12_Medi.first), mVis/sqrt(x12_Medi.second) );
}
void Mosaic::OutputManager::EstimateMass_X12_Mode( std::vector<Mosaic::SamplingPoint>& ResultPoints, double mVis){
  auto x12_Mode  = Mosaic::AlgorithmManager::Mode1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.x12() ; } );
  Mass_X12_Mode  = std::make_pair( mVis/sqrt(x12_Mode.first), mVis/sqrt(x12_Mode.second) );
}
void Mosaic::OutputManager::EstimateMass_Mis_Mean( std::vector<Mosaic::SamplingPoint>& ResultPoints, FourVector vis1, FourVector vis2, double sumOf_Weight){
  auto px1_Mean  = Mosaic::AlgorithmManager::Mean1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.px_mis1() ; } , sumOf_Weight).first;
  auto py1_Mean  = Mosaic::AlgorithmManager::Mean1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.py_mis1() ; } , sumOf_Weight).first;
  auto pz1_Mean  = Mosaic::AlgorithmManager::Mean1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.pz_mis1() ; } , sumOf_Weight).first;
  auto px2_Mean  = Mosaic::AlgorithmManager::Mean1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.px_mis2() ; } , sumOf_Weight).first;
  auto py2_Mean  = Mosaic::AlgorithmManager::Mean1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.py_mis2() ; } , sumOf_Weight).first;
  auto pz2_Mean  = Mosaic::AlgorithmManager::Mean1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.pz_mis2() ; } , sumOf_Weight).first;
  Mis1_Mean      = Mosaic::OutputManager::getMisVector(px1_Mean, py1_Mean, pz1_Mean);
  Mis2_Mean      = Mosaic::OutputManager::getMisVector(px2_Mean, py2_Mean, pz2_Mean);
  Mass_Mis_Mean  = Mosaic::OutputManager::get_InvMass(vis1, vis2, Mis1_Mean, Mis2_Mean);
}
void Mosaic::OutputManager::EstimateMass_Mis_Medi( std::vector<Mosaic::SamplingPoint>& ResultPoints, FourVector vis1, FourVector vis2, double sumOf_Weight){
  auto px1_Medi  = Mosaic::AlgorithmManager::Medi1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.px_mis1() ; } , sumOf_Weight);
  auto py1_Medi  = Mosaic::AlgorithmManager::Medi1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.py_mis1() ; } , sumOf_Weight);
  auto pz1_Medi  = Mosaic::AlgorithmManager::Medi1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.pz_mis1() ; } , sumOf_Weight);
  auto px2_Medi  = Mosaic::AlgorithmManager::Medi1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.px_mis2() ; } , sumOf_Weight);
  auto py2_Medi  = Mosaic::AlgorithmManager::Medi1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.py_mis2() ; } , sumOf_Weight);
  auto pz2_Medi  = Mosaic::AlgorithmManager::Medi1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.pz_mis2() ; } , sumOf_Weight);
  Mis1_Medi      = Mosaic::OutputManager::getMisVector(px1_Medi, py1_Medi, pz1_Medi);
  Mis2_Medi      = Mosaic::OutputManager::getMisVector(px2_Medi, py2_Medi, pz2_Medi);
  Mass_Mis_Medi  = Mosaic::OutputManager::get_InvMass(vis1, vis2, Mis1_Medi, Mis2_Medi);
}
void Mosaic::OutputManager::EstimateMass_Mis_Mode( std::vector<Mosaic::SamplingPoint>& ResultPoints, FourVector vis1, FourVector vis2){
  auto px1_Mode  = Mosaic::AlgorithmManager::Mode1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.px_mis1() ; } );
  auto py1_Mode  = Mosaic::AlgorithmManager::Mode1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.py_mis1() ; } );
  auto pz1_Mode  = Mosaic::AlgorithmManager::Mode1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.pz_mis1() ; } );
  auto px2_Mode  = Mosaic::AlgorithmManager::Mode1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.px_mis2() ; } );
  auto py2_Mode  = Mosaic::AlgorithmManager::Mode1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.py_mis2() ; } );
  auto pz2_Mode  = Mosaic::AlgorithmManager::Mode1(ResultPoints, [](const Mosaic::SamplingPoint& p) -> double { return p.pz_mis2() ; } );
  Mis1_Mode      = Mosaic::OutputManager::getMisVector(px1_Mode, py1_Mode, pz1_Mode);
  Mis2_Mode      = Mosaic::OutputManager::getMisVector(px2_Mode, py2_Mode, pz2_Mode);
  Mass_Mis_Mode  = Mosaic::OutputManager::get_InvMass(vis1, vis2, Mis1_Mode, Mis2_Mode);
}
double Mosaic::OutputManager::EstimateMass( Mosaic::MET_nJet njet, Mosaic::ChannelType channel, Mosaic::CalibType CalibType, std::string runMode ) const {
  double val = 0.0;
  if( runMode.find("NOMINAL") != std::string::npos ){
    if( CalibType == Mosaic::CalibType::MC15_50ns_v1){
      if       ( channel == Mosaic::ChannelType::LEPHAD ){
        if      ( njet == Mosaic::MET_nJet::nJet0 ){
          //val = (-0.243168321749* M_Mis_Medi(0, true)+1.24316832175*(0.0539538471934*M_Medi(1, true)+0.946046152807*(2.33434355397*M_Medi(0)-1.33434355397*M_Mis_Medi(1))));
          val = (-0.243168321749*GetMass_Mis_Medi(0, true)+1.24316832175*(0.0539538471934*GetMass_mtt_Medi(1, true)+0.946046152807*(2.33434355397*GetMass_mtt_Medi(0, true)-1.33434355397*GetMass_Mis_Medi(1, true))));
        }else{
          //val =  (7.89156141958*M_Medi(1)-6.89156141958*M_Mis_Medi(1));
          val =  (7.89156141958*GetMass_mtt_Medi(1, true)-6.89156141958*GetMass_Mis_Medi(1, true));
        }
      }else if ( channel == Mosaic::ChannelType::HADHAD ){
        if      ( njet == Mosaic::MET_nJet::nJet0 ){
          //val = (-0.018420626371*M_X12_Medi(0, true)+1.01842062637*(-0.881235499629*M_Mean(1, true)+1.88123549963*M_X12_Mean(0, true))) ;
          val = (-0.018420626371*GetMass_X12_Medi(0, true)+1.01842062637*(-0.881235499629*GetMass_mtt_Mean(1, true)+1.88123549963*GetMass_X12_Mean(0, true))) ;
        }else{
          //val = (-2.07632288001*M_Mean(0)+3.07632288001*(1.07988271537*M_Medi(0)-0.079882715368*M_X12_Mode(1)));
          val = (-2.07632288001*GetMass_mtt_Mean(0, true)+3.07632288001*(1.07988271537*GetMass_mtt_Medi(0, true)-0.079882715368*GetMass_X12_Mode(1, true)));
        }
      }
    }
  }else if( runMode.find("FAST") != std::string::npos ){
    val = GetMass_mtt_Medi(0, true);
  }
  if( val <= 0.0 ) val = GetMass_mtt_Medi(0, true);
  
  return val * Mosaic::Constant::_GeV2MeV;
}
std::vector<double> Mosaic::OutputManager::Get_mtt_Ms(){
  std::vector<double> ms{ GetMass_mtt_Mean(0), GetMass_mtt_Medi(0), GetMass_mtt_Mode(0), GetMass_mtt_Mean(1), GetMass_mtt_Medi(1), GetMass_mtt_Mode(1)};
  return ms;
}
std::vector<double> Mosaic::OutputManager::Get_X12_Ms(){
  std::vector<double> ms{ GetMass_X12_Mean(0), GetMass_X12_Medi(0), GetMass_X12_Mode(0), GetMass_X12_Mean(1), GetMass_X12_Medi(1), GetMass_X12_Mode(1)};
  return ms;
}
std::vector<double> Mosaic::OutputManager::Get_Mis_Ms(){
  std::vector<double> ms{ GetMass_Mis_Mean(0), GetMass_Mis_Medi(0), GetMass_Mis_Mode(0), GetMass_Mis_Mean(1), GetMass_Mis_Medi(1), GetMass_Mis_Mode(1)};
  return ms;
}
std::vector<double> Mosaic::OutputManager::Get_Corr_Ms(){
  std::vector<double> ms{ GetMass_SigmaCorrLow(0), GetMass_SigmaCorrLow(1), GetMass_SigmaCorrHigh(0), GetMass_SigmaCorrHigh(1), 
      GetMass_SigmaCorrComb1(0), GetMass_SigmaCorrComb1(1), GetMass_SigmaCorrComb2(0), GetMass_SigmaCorrComb2(1) };
  return ms;
}


bool Mosaic::OutputManager::SetOutputInfo( Mosaic::InputManager * inputManager,  std::vector<Mosaic::SamplingPoint>&& ResultPoints, Mosaic::RunningMode::Nominal ){
  //bool Mosaic::OutputManager::SetOutputInfo( Mosaic::InputManager * inputManager,  Mosaic::RunningMode::Nominal ){
  auto temp      = std::move( ResultPoints );
  ResultPoints   = std::move( samplingPoints );
  samplingPoints = std::move( temp );
  if( samplingPoints.size() < 2 ) {
    return false;
  }
  double sumOf_Weight = std::accumulate( std::begin(samplingPoints), std::end(samplingPoints), 0.0, [](double lhs, const Mosaic::SamplingPoint& p) -> double { return lhs + p.prob(); }  );
  if( inputManager->DoAllVar() ){
    
    auto vis1      = inputManager->Vis1();
    auto vis2      = inputManager->Vis2();
    double mVis    = Mosaic::AlgorithmManager::InvMass( vis1.px()+vis2.px(), vis1.py()+vis2.py(), vis1.pz()+vis2.pz(), vis1.e()+vis2.e() );

    Mosaic::OutputManager::EstimateMass_mtt_Mean( samplingPoints, sumOf_Weight );
    Mosaic::OutputManager::EstimateMass_mtt_Medi( samplingPoints, sumOf_Weight );
    Mosaic::OutputManager::EstimateMass_mtt_Mode( samplingPoints );
    Mosaic::OutputManager::EstimateMass_X12_Mean( samplingPoints, mVis, sumOf_Weight );
    Mosaic::OutputManager::EstimateMass_X12_Medi( samplingPoints, mVis, sumOf_Weight );
    Mosaic::OutputManager::EstimateMass_X12_Mode( samplingPoints, mVis );
    Mosaic::OutputManager::EstimateMass_Mis_Mean( samplingPoints, vis1, vis2, sumOf_Weight );
    Mosaic::OutputManager::EstimateMass_Mis_Medi( samplingPoints, vis1, vis2, sumOf_Weight );
    Mosaic::OutputManager::EstimateMass_Mis_Mode( samplingPoints, vis1, vis2 );

  }else{
    auto channel = inputManager->Get_Channel();
    auto njet    = inputManager->nJet();
    if( inputManager->Get_CalibType() == Mosaic::CalibType::MC15_50ns_v1){
      if       ( channel == Mosaic::ChannelType::LEPHAD ){
        if      ( njet == Mosaic::MET_nJet::nJet0 ){
          Mosaic::OutputManager::EstimateMass_mtt_Medi( samplingPoints, sumOf_Weight );
          Mosaic::OutputManager::EstimateMass_Mis_Medi( samplingPoints, inputManager->Vis1(), inputManager->Vis2(), sumOf_Weight );
        }else{
          Mosaic::OutputManager::EstimateMass_mtt_Medi( samplingPoints, sumOf_Weight );
          Mosaic::OutputManager::EstimateMass_Mis_Medi( samplingPoints, inputManager->Vis1(), inputManager->Vis2(), sumOf_Weight );
        }
      }else if ( channel == Mosaic::ChannelType::HADHAD ){
        if      ( njet == Mosaic::MET_nJet::nJet0 ){
          Mosaic::OutputManager::EstimateMass_mtt_Mean( samplingPoints, sumOf_Weight );
          Mosaic::OutputManager::EstimateMass_X12_Mean( samplingPoints, inputManager->mVis(), sumOf_Weight );
          Mosaic::OutputManager::EstimateMass_X12_Medi( samplingPoints, inputManager->mVis(), sumOf_Weight );
        }else{
          Mosaic::OutputManager::EstimateMass_mtt_Mean( samplingPoints, sumOf_Weight );
          Mosaic::OutputManager::EstimateMass_mtt_Medi( samplingPoints, sumOf_Weight );
          Mosaic::OutputManager::EstimateMass_X12_Mode( samplingPoints, inputManager->mVis() );
        }
      }
    }
  }
  return true;
}

bool Mosaic::OutputManager::SetOutputInfo( Mosaic::InputManager * inputManager,  std::vector<Mosaic::SamplingPoint>&& ResultPoints, Mosaic::RunningMode::Fast ){
//bool Mosaic::OutputManager::SetOutputInfo( Mosaic::InputManager * inputManager, Mosaic::RunningMode::Fast ){
  MOSAIC_UNUSED_VARIABLE( inputManager );
  auto temp      = std::move( ResultPoints );
  ResultPoints   = std::move( samplingPoints );
  samplingPoints = std::move( temp );
  if( samplingPoints.size() < 2 ) {
    return false;
  }
  double sumOf_Weight = std::accumulate( std::begin(samplingPoints), std::end(samplingPoints), 0.0, [](double lhs, const Mosaic::SamplingPoint& p) -> double { return lhs + p.prob(); }  );
  Mosaic::OutputManager::EstimateMass_mtt_Mean( samplingPoints, sumOf_Weight );
  Mosaic::OutputManager::EstimateMass_mtt_Medi( samplingPoints, sumOf_Weight );
  Mosaic::OutputManager::EstimateMass_mtt_Mode( samplingPoints );
  
  Mass_SigmaCorrLow   = std::make_pair(Mosaic::OutputManager::getMass_SigmaCorrLow( Mass_Mtt_Mode.first,  Mass_Mtt_Sigma.first), 
                                       Mosaic::OutputManager::getMass_SigmaCorrLow( Mass_Mtt_Mode.second, Mass_Mtt_Sigma.second) );
  
  Mass_SigmaCorrHigh  = std::make_pair(Mosaic::OutputManager::getMass_SigmaCorrHigh( Mass_Mtt_Mode.first,  Mass_Mtt_Sigma.first), 
                                       Mosaic::OutputManager::getMass_SigmaCorrHigh( Mass_Mtt_Mode.second, Mass_Mtt_Sigma.second) );
  
  Mass_SigmaCorrComb1 = std::make_pair(Mosaic::OutputManager::getMass_SigmaCorrComb1( Mass_Mtt_Mode.first,  Mass_Mtt_Sigma.first), 
                                       Mosaic::OutputManager::getMass_SigmaCorrComb1( Mass_Mtt_Mode.second, Mass_Mtt_Sigma.second) );
  
  Mass_SigmaCorrComb2 = std::make_pair(Mosaic::OutputManager::getMass_SigmaCorrComb2( Mass_Mtt_Mode.first,  Mass_Mtt_Sigma.first), 
                                       Mosaic::OutputManager::getMass_SigmaCorrComb2( Mass_Mtt_Mode.second, Mass_Mtt_Sigma.second) );
  
  
  return true;
}
const std::vector< Mosaic::SamplingPoint >& Mosaic::OutputManager::getSamplingPoint(){ return samplingPoints; }


double Mosaic::OutputManager::getMass_SigmaCorrLow( double mode, double sigma){
  double x = sigma / mode;
  double temp = 0.5 * 0.51 * ( 1.0 + std::erf( ( x - 0.25 ) / ( std::sqrt(x) * 0.65 ) ));
  return mode / ( 1.0 - temp);
}
double Mosaic::OutputManager::getMass_SigmaCorrHigh( double mode, double sigma){
  double x = sigma / mode;
  double temp = 0.5 * 1.29 * ( 1.0 + std::erf( ( x - 0.58 ) / ( std::sqrt(x) * 1.42 ) ));
  return mode / ( 1.0 - temp);
}
double Mosaic::OutputManager::getMass_SigmaCorrComb1( double mode, double sigma){
  double x = sigma / mode;
  double temp = ( 1.0 + std::erf( ( x - 0.30 ) / ( std::sqrt(x) * 0.60 ) ));
  return ( mode/1000. - 0.5 * temp * 60.0 ) / ( 1.0 - 0.5 * temp ) * 1000.;
}
double Mosaic::OutputManager::getMass_SigmaCorrComb2( double mode, double sigma){
  double x = sigma / mode;
  double temp = ( 1.0 + std::erf( ( x - 0.30 ) / ( std::sqrt(x) * 0.60 ) ));
  return ( mode/1000. - 0.5 * temp * 40.0 ) / ( 1.0 - 0.5 * temp ) * 1000.;
}




#endif // MOSAIC_IOMANAGER_C
