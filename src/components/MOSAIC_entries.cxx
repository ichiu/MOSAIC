#include "GaudiKernel/DeclareFactoryEntries.h"

#include "MOSAIC/xMOSAICalculator.h"

DECLARE_TOOL_FACTORY( xMOSAICalculator )

DECLARE_FACTORY_ENTRIES( MOSAIC )
{
  DECLARE_TOOL( xMOSAICalculator );
}
