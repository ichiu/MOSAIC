// Dear emacs, this is -*- c++ -*- 
/*
 * MOSAICalculator
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_MOSAICALCULATOR_H
# error Do Not include this file directly, include MOSAICalculator.h instead
#endif

template<typename MCMCTau1, typename MCMCTau2, typename PDF1, typename PDF2, typename PDFDecayME, typename ResolModel_Vis1, typename ResolModel_Vis2, typename LLHType, typename RunMode> 
bool MOSAICalculator::run(PDF1* pdf1, PDF2* pdf2, PDFDecayME* pDFDecayME, Mosaic::OutputManager* outputManager){
  bool isOK = false;
  
  outputManager->Clear();
  auto MET_Used = inputManager->MET_USED();
  Mosaic::ResolModel_ETMiss   * ETMissModel = new Mosaic::ResolModel_ETMiss(MET_Used.MEx(), MET_Used.MEy(), MET_Used.SumEt(), 
                                                                            inputManager->ResolutionScale(), inputManager->DeltaMETThresold(), inputManager->METProbScale(), 
                                                                            inputManager->nJet(), inputManager->Get_MET_Type(), inputManager->Get_CalibType() );
  ResolModel_Vis1             * Vis1Model   = new ResolModel_Vis1(inputManager->Vis1(), inputManager->Resoltion_Vis1());
  ResolModel_Vis2             * Vis2Model   = new ResolModel_Vis2(inputManager->Vis2(), inputManager->Resoltion_Vis2());

#ifdef MOSAIC_USE_DEVELOP  
  Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2>   * PDFManager  = new Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2>(inputManager, pdf1, pdf2, pDFDecayME, ETMissModel, Vis1Model, Vis2Model, PDF_lHAPDF);
#else
  Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2>   * PDFManager  = new Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2>(inputManager, pdf1, pdf2, pDFDecayME, ETMissModel, Vis1Model, Vis2Model);
#endif
  Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, Mosaic::Constant::N_Chain> * MarkovChainSet 
    = new Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, Mosaic::Constant::N_Chain>(inputManager, PDFManager);
  outputManager->SetIsSwap( inputManager->IsSwap() );
  outputManager->SetMETParameter( ETMissModel->Sigma1, ETMissModel->Sigma2, ETMissModel->Fraction );
  
  MarkovChainSet->init();
  
  if( MarkovChainSet->process_main()       ) {
    if( MarkovChainSet->process_additional() ) {
      isOK = outputManager->SetOutputInfo( inputManager, std::move( MarkovChainSet->Results() ), RunMode() );
    }
  }
  
  delete MarkovChainSet;
  delete ETMissModel;
  delete PDFManager;
  return isOK;
}

template<typename PDFDecayME, typename LLHType, typename RunMode>
bool MOSAICalculator::Run_Mosaic(PDFDecayME * pdfDecayME, Mosaic::OutputManager * outputManager){
  Mosaic::ParticleType Type1 = inputManager->vis1.ParticleType();
  Mosaic::ParticleType Type2 = inputManager->vis2.ParticleType();
  if ( Type1 == Mosaic::ParticleType::None       || Type2 == Mosaic::ParticleType::None       ) return false;
  if ( Type1 == Mosaic::ParticleType::mode_other || Type2 == Mosaic::ParticleType::mode_other ) return false;
  
  bool isOK = false;
  if     ( Type1 == Mosaic::ParticleType::p_Electron && Type2 == Mosaic::ParticleType::p_Electron ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCLep, Mosaic::PDF_Lep,    Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Electron, Mosaic::ResolModel_Electron, LLHType, RunMode >(PDF_El,   PDF_El,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Electron && Type2 == Mosaic::ParticleType::p_Muon     ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCLep, Mosaic::PDF_Lep,    Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Electron, Mosaic::ResolModel_Muon,     LLHType, RunMode >(PDF_El,   PDF_Mu,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Electron && Type2 == Mosaic::ParticleType::p_Pion     ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCHad, Mosaic::PDF_Lep,    Mosaic::PDF_Pion   ,PDFDecayME, Mosaic::ResolModel_Electron, Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_El,   PDF_pion, pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Electron && Type2 == Mosaic::ParticleType::p_Rho      ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCHad, Mosaic::PDF_Lep,    Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Electron, Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_El,   PDF_rho,  pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Electron && Type2 == Mosaic::ParticleType::p_a1       ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCHad, Mosaic::PDF_Lep,    Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Electron, Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_El,   PDF_a1,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Muon     && Type2 == Mosaic::ParticleType::p_Electron ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCLep, Mosaic::PDF_Lep,    Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Muon,     Mosaic::ResolModel_Electron, LLHType, RunMode >(PDF_Mu,   PDF_El,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Muon     && Type2 == Mosaic::ParticleType::p_Muon     ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCLep, Mosaic::PDF_Lep,    Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Muon,     Mosaic::ResolModel_Muon,     LLHType, RunMode >(PDF_Mu,   PDF_Mu,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Muon     && Type2 == Mosaic::ParticleType::p_Pion     ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCHad, Mosaic::PDF_Lep,    Mosaic::PDF_Pion   ,PDFDecayME, Mosaic::ResolModel_Muon,     Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_Mu,   PDF_pion, pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Muon     && Type2 == Mosaic::ParticleType::p_Rho      ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCHad, Mosaic::PDF_Lep,    Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Muon,     Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_Mu,   PDF_rho,  pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Muon     && Type2 == Mosaic::ParticleType::p_a1       ) isOK = MOSAICalculator::run < Mosaic::MCMCLep, Mosaic::MCMCHad, Mosaic::PDF_Lep,    Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Muon,     Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_Mu,   PDF_a1,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Pion     && Type2 == Mosaic::ParticleType::p_Electron ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCLep, Mosaic::PDF_Pion,   Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Electron, LLHType, RunMode >(PDF_pion, PDF_El,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Pion     && Type2 == Mosaic::ParticleType::p_Muon     ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCLep, Mosaic::PDF_Pion,   Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Muon,     LLHType, RunMode >(PDF_pion, PDF_Mu,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Pion     && Type2 == Mosaic::ParticleType::p_Pion     ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Pion,   Mosaic::PDF_Pion   ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_pion, PDF_pion, pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Pion     && Type2 == Mosaic::ParticleType::p_Rho      ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Pion,   Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_pion, PDF_rho,  pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Pion     && Type2 == Mosaic::ParticleType::p_a1       ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Pion,   Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_pion, PDF_a1,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Rho      && Type2 == Mosaic::ParticleType::p_Electron ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCLep, Mosaic::PDF_Spline, Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Electron, LLHType, RunMode >(PDF_rho,  PDF_El,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Rho      && Type2 == Mosaic::ParticleType::p_Muon     ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCLep, Mosaic::PDF_Spline, Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Muon,     LLHType, RunMode >(PDF_rho,  PDF_Mu,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Rho      && Type2 == Mosaic::ParticleType::p_Pion     ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Spline, Mosaic::PDF_Pion   ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_rho,  PDF_pion, pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Rho      && Type2 == Mosaic::ParticleType::p_Rho      ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Spline, Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_rho,  PDF_rho,  pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_Rho      && Type2 == Mosaic::ParticleType::p_a1       ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Spline, Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_rho,  PDF_a1,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_a1       && Type2 == Mosaic::ParticleType::p_Electron ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCLep, Mosaic::PDF_Spline, Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Electron, LLHType, RunMode >(PDF_a1,   PDF_El,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_a1       && Type2 == Mosaic::ParticleType::p_Muon     ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCLep, Mosaic::PDF_Spline, Mosaic::PDF_Lep    ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Muon,     LLHType, RunMode >(PDF_a1,   PDF_Mu,   pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_a1       && Type2 == Mosaic::ParticleType::p_Pion     ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Spline, Mosaic::PDF_Pion   ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_a1,   PDF_pion, pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_a1       && Type2 == Mosaic::ParticleType::p_Rho      ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Spline, Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_a1,   PDF_rho,  pdfDecayME, outputManager);
  else if( Type1 == Mosaic::ParticleType::p_a1       && Type2 == Mosaic::ParticleType::p_a1       ) isOK = MOSAICalculator::run < Mosaic::MCMCHad, Mosaic::MCMCHad, Mosaic::PDF_Spline, Mosaic::PDF_Spline ,PDFDecayME, Mosaic::ResolModel_Tau,      Mosaic::ResolModel_Tau,      LLHType, RunMode >(PDF_a1,   PDF_a1,   pdfDecayME, outputManager);
  return isOK;




}
