// Dear emacs, this is -*- c++ -*- 
/*
 * IOManager 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_IOMAGAGER_H
#define MOSAIC_IOMAGAGER_H

#include "MOSAIC/AlgorithmManager.h"
#include "MOSAIC/Objects.h"
#include "MOSAIC/TauObj.h"

namespace Mosaic {
  
  class InputManager {
  public:
    InputManager();
    ~InputManager();
    void Clear();
    void Swap12();
    bool init();
    void Set_Use_Better_MET(bool i);
    void Set_Use_MPT(bool i);
    void Set_DoAllVar(bool i);
    void Set_ForceInclusiveNJet(bool i);
    void Set_TruthStudy(bool i);
    void Set_ResolutionScale(double v);
    void Set_DeltaMETThresold(double v);
    void Set_METProbScale(double v);
    void Set_SeedType( int _seedType );
    void Set_CalibType( Mosaic::CalibType this_Type );
    void Set_METType( Mosaic::MET_Type mettype);
    void SetMissingEt(double exmiss, double eymiss, double sumet, double mu = 20.);
    void SetMissingEt_SoftTerm(double exmiss, double eymiss, double sumet);
    void SetDecayType   (int iTau, int t);
    void SetVis_panTau  (int iTau, double px, double py, double pz, double e, double m, double charge );
    void SetVis_tauRec  (int iTau, double px, double py, double pz, double e, double m, double charge );
    void SetVis         (int iTau, double px, double py, double pz, double e, double m, double charge ) ;
    void addCharged     (int iTau, double px, double py, double pz, double e, double m, double charge);
    void addNeutral     (int iTau, double px, double py, double pz, double e, double m, double score);
    void addOtherNeutral(int iTau, double px, double py, double pz, double e, double m, double score);
    void AddJet(double pt, double eta, double jvf, double px, double py, double pz, double e, double m, double charge);
    //void SetVisResolution(int iTau );
    //void SetVisResolution(int iTau , double resol );
    
    bool DoAllVar()     const ;
    bool IsForceNJetInclusive() const;
    int IsSwap()        const ;
    int Used_MET_Type() const;
    Mosaic::ChannelType Get_Channel()        const ;
    Mosaic::MET_nJet nJet()                  const ;
    Mosaic::MET_Type Get_MET_Type()          const ;
    Mosaic::CalibType Get_CalibType()        const ;
    double CosPsi_Rho(double e, double E)    const ;
    double ObsMu() const ;
    double akaRandom() const ;
    double Resoltion_Vis1() const;
    double Resoltion_Vis2() const;
    double DeltaMETThresold() const ;
    double mVis() const ;
    double ResolutionScale() const;
    double CM_1() const;
    double CM_2() const;
    double METProbScale() const;
    double CosPsi_3p(double charge, double visE, Mosaic::FourVector* pi1, Mosaic::FourVector* pi2, Mosaic::FourVector* pi3)     const ;
    double CosPsi_1p(double visE, Mosaic::FourVector* pi1, Mosaic::FourVector* pi01, Mosaic::FourVector* pi02)     const ;
    std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> get_seed_seq() ;
    Mosaic::FourVector Vis1() const;
    Mosaic::FourVector Vis2() const;
    Mosaic::MissingEt MET_USED() const;
    inline std::vector< Mosaic::Jet * > * Get_RecoilJets() const { return Jets; }
    
    Mosaic::TauObj vis1;
    Mosaic::TauObj vis2;
    
  private:
    bool isUse_MPT;
    bool isUseBetter_MET;
    bool isDoAllVar;
    bool isForceNJetInclusive;
    bool isTruthStudy;
    int seedType;
    int isSwap;
    int nJet_Jet;
    Mosaic::MET_nJet met_nJet;
    Mosaic::MET_Type met_Type;
    Mosaic::MET_Type met_Type_Used;
    Mosaic::CalibType calib_Type;
    
    Mosaic::MissingEt MET_Used;
    Mosaic::MissingEt MET_TST_Final;
    Mosaic::MissingEt MET_TST_SoftTerm;
    Mosaic::MissingEt MET_MPT_Final;
    
    double agvPerX;
    double resolutionScale, deltaMETThresold, mETProbScale;
    double cM_1, cM_2;

    std::mt19937 Engine;
    std::uniform_real_distribution<double> Uniform;
    
    std::vector< Mosaic::Jet* >* Jets;
  };
  
  class OutputManager {
  public:
    OutputManager();
    ~OutputManager();
    
    void Clear();
    void SetIsSwap(int f);
    void SetMETParameter(double s1, double s2, double f);
    double EstimateMass( Mosaic::MET_nJet njet, Mosaic::ChannelType channel, Mosaic::CalibType CalibType = Mosaic::CalibType::MC15_50ns_v1, std::string runMode = "NOMINAL" ) const ;
    

    double get_mass(int isWeight, bool isGeV, std::pair<double, double> p) const;
    double GetMass_mtt_Mean(int isWeight, bool isGeV = false) const ;
    double GetMass_mtt_Medi(int isWeight, bool isGeV = false) const ;
    double GetMass_mtt_Mode(int isWeight, bool isGeV = false) const ;
    double GetMass_X12_Mean(int isWeight, bool isGeV = false) const ;
    double GetMass_X12_Medi(int isWeight, bool isGeV = false) const ;
    double GetMass_X12_Mode(int isWeight, bool isGeV = false) const ;
    double GetMass_Mis_Mean(int isWeight, bool isGeV = false) const ;
    double GetMass_Mis_Medi(int isWeight, bool isGeV = false) const ;
    double GetMass_Mis_Mode(int isWeight, bool isGeV = false) const ;
    double GetMass_SigmaCorrLow(int isWeight, bool isGeV = false) const ;
    double GetMass_SigmaCorrHigh(int isWeight, bool isGeV = false) const ;
    double GetMass_SigmaCorrComb1(int isWeight, bool isGeV = false) const ;
    double GetMass_SigmaCorrComb2(int isWeight, bool isGeV = false) const ;
    
    double getMass_SigmaCorrLow( double mode, double sigma); 
    double getMass_SigmaCorrHigh( double mode, double sigma); 
    double getMass_SigmaCorrComb1( double mode, double sigma); 
    double getMass_SigmaCorrComb2( double mode, double sigma); 
    
    
    void EstimateMass_mtt_Mean( std::vector<Mosaic::SamplingPoint>& ResultPoints, double sumOf_Weight);
    void EstimateMass_mtt_Medi( std::vector<Mosaic::SamplingPoint>& ResultPoints, double sumOf_Weight);
    void EstimateMass_mtt_Mode( std::vector<Mosaic::SamplingPoint>& ResultPoints);
    void EstimateMass_X12_Mean( std::vector<Mosaic::SamplingPoint>& ResultPoints, double mVis, double sumOf_Weight);
    void EstimateMass_X12_Medi( std::vector<Mosaic::SamplingPoint>& ResultPoints, double mVis, double sumOf_Weight);
    void EstimateMass_X12_Mode( std::vector<Mosaic::SamplingPoint>& ResultPoints, double mVis);
    void EstimateMass_Mis_Mean( std::vector<Mosaic::SamplingPoint>& ResultPoints, FourVector vis1, FourVector vis2, double sumOf_Weight);
    void EstimateMass_Mis_Medi( std::vector<Mosaic::SamplingPoint>& ResultPoints, FourVector vis1, FourVector vis2, double sumOf_Weight);
    void EstimateMass_Mis_Mode( std::vector<Mosaic::SamplingPoint>& ResultPoints, FourVector vis1, FourVector vis2);
    
    double MET_Sigma1() const ;
    double MET_Sigma2() const ;
    double MET_Fraction() const ;
    std::pair<Mosaic::FourVector, Mosaic::FourVector> getMisVector( std::pair<double, double> px, std::pair<double, double> py, std::pair<double, double> pz );
    std::pair<double, double> get_InvMass(Mosaic::FourVector vis1, Mosaic::FourVector vis2, std::pair<Mosaic::FourVector, Mosaic::FourVector> mis1, std::pair<Mosaic::FourVector, Mosaic::FourVector> mis2);
    
    std::vector<double> Get_mtt_Ms();
    std::vector<double> Get_X12_Ms();
    std::vector<double> Get_Mis_Ms();
    std::vector<double> Get_Corr_Ms();


    bool SetOutputInfo( Mosaic::InputManager * inputManager,  std::vector<Mosaic::SamplingPoint>&& ResultPoints, Mosaic::RunningMode::Nominal );
    //bool SetOutputInfo( Mosaic::InputManager * inputManager, Mosaic::RunningMode::Nominal );
    bool SetOutputInfo( Mosaic::InputManager * inputManager,  std::vector<Mosaic::SamplingPoint>&& ResultPoints, Mosaic::RunningMode::Fast );
    //bool SetOutputInfo( Mosaic::InputManager * inputManager,   Mosaic::RunningMode::Fast );
    
    const std::vector< Mosaic::SamplingPoint >& getSamplingPoint();
    
  private:
    int isSwap;
    double _MET_Sigma1, _MET_Sigma2, _MET_Frac;
    std::pair<double, double> Mass_SigmaCorrLow;
    std::pair<double, double> Mass_SigmaCorrHigh;
    std::pair<double, double> Mass_SigmaCorrComb1;
    std::pair<double, double> Mass_SigmaCorrComb2;
    
    std::pair<double, double> Mass_Mtt_Sigma;
    std::pair<double, double> Mass_Mtt_Mean;
    std::pair<double, double> Mass_Mtt_Medi;
    std::pair<double, double> Mass_Mtt_Mode;
    std::pair<double, double> Mass_X12_Mean;
    std::pair<double, double> Mass_X12_Medi;
    std::pair<double, double> Mass_X12_Mode;
    std::pair<double, double> Mass_Mis_Mean;
    std::pair<double, double> Mass_Mis_Medi;
    std::pair<double, double> Mass_Mis_Mode;
    std::pair<Mosaic::FourVector, Mosaic::FourVector> Mis1_Mean;
    std::pair<Mosaic::FourVector, Mosaic::FourVector> Mis1_Medi;
    std::pair<Mosaic::FourVector, Mosaic::FourVector> Mis1_Mode;
    std::pair<Mosaic::FourVector, Mosaic::FourVector> Mis2_Mean;
    std::pair<Mosaic::FourVector, Mosaic::FourVector> Mis2_Medi;
    std::pair<Mosaic::FourVector, Mosaic::FourVector> Mis2_Mode;
    
  public:    
    std::vector< Mosaic::SamplingPoint > samplingPoints;
    
    
  };
}

#endif // MOSAIC_IOMAGAGER_H
  
