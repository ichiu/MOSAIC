// Dear emacs, this is -*- c++ -*- 
/*
 * Constant_h 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_CONSTANT_H
#define MOSAIC_CONSTANT_H

#define ISMeV

#include <array>

namespace Mosaic {
  enum struct MET_Type       : std::size_t { MET_TST       = 0, MET_MPT, MET_TST_Soft, MET_Truth, MET_Smear,  N };
  enum struct MET_Resol_Par  : std::size_t { MET_Sigma1    = 0, MET_Sigma2, MET_Fraction, N };
  enum struct MET_Resol_PPar : std::size_t { Par0          = 0, Par1, Par2, N };
  enum struct MET_nJet       : std::size_t { nJet0         = 0, nJet1,  nJetInclusive , N };
  enum struct Jet_Eta        : std::size_t { jEta08        = 0, jEta12, jEta21, jEta45, N };
  
  enum struct CalibType      : std::size_t { MC15_50ns_v1  = 0, MC15_25ns_v1 = 1, N };
  enum struct TuningType     : std::size_t { HighMass_2015 = 0, LowMass_2015, N };
  enum struct ParticleType   : std::size_t { mode_1p0n     = 0, mode_1p1n, mode_1pXn, mode_3p0n, mode_3pXn, mode_other, p_Electron, p_Muon, p_Pion, p_Pi0, p_Rho, p_a1, None, N };
  enum struct ChannelType    : std::size_t { LEPLEP        = 0, LEPHAD, HADHAD, N };
  template <class T> 
    constexpr typename std::underlying_type<T>::type enumToSize_t(T t) { return static_cast<typename std::underlying_type<T>::type>(t); }


    // Jet Resolution 
  inline Mosaic::Jet_Eta JetResol_Eta(double feta){
      if     (feta < 0.8) return Mosaic::Jet_Eta::jEta08 ;
      else if(feta < 1.2) return Mosaic::Jet_Eta::jEta12 ;
      else if(feta < 2.1) return Mosaic::Jet_Eta::jEta21 ;
      return                     Mosaic::Jet_Eta::jEta45 ;
    }

  namespace LikelihoodType {
    struct Default { }; // ME + MET
    struct MEOnly { }; // ME + MET
    struct METOnly { }; // ME + MET
    struct PlusVisResoltion { }; // ME + MET + Visible Resolution
    struct RecoilSystem { }; // ME + MET + RecoilSystem Resolution
    struct PartDF_LHAPDF { }; // ME + MET + LHAPDF
  }
  namespace RunningMode {
    struct Nominal { }; // Nominal Mode
    struct Fast { }; // Fast Mode
  }
  namespace Constant {
    constexpr int pid_gluon                      = 21;
    constexpr double ECM_13TeV_2                 = 13.0 * 1000. * 1000. * 2.0;
    constexpr double _GeV2MeV                    = 1000.;
    constexpr double _MeV2GeV                    = 1.0/1000.;
    constexpr double PI                          = 3.1415926535;
    constexpr double PI2                         = PI*2.0;
    constexpr double PI_2                        = PI*0.5;
    constexpr double PI_2_M                      = -PI*0.5;
    constexpr double INV_SQ2_PI                  = 0.3989422804014;
    constexpr double INV_2PI                     = 1.0/(PI2);
    constexpr double LOG_INV_PI2                 = -0.91893853320467267;
    constexpr double LOG_PI                      = 1.1447298858494002;
    constexpr double LOG_PI2                     = 1.8378770664093453;
    constexpr double SQRT2                       = 1.4142135624;
    constexpr double SQRTPIOVER2                 = 1.2533141373;
    constexpr double SQRTPI                      = 1.772004514666935;
    
    constexpr double BREL                        = 0.1784;
    constexpr double BRMU                        = 0.1736;
    constexpr double BRPION                      = 0.1106;
    
    constexpr double MTAU                        = 1776.84     ;
    constexpr double MEL                         = 0.510998928 ;
    constexpr double MMU                         = 105.6583715 ;
    constexpr double MPI                         = 139.57      ;
    constexpr double MPI0                        = 134.97      ;
    constexpr double MRHO                        = 770.0       ;
    constexpr double MA1                         = 1230.0      ;
    constexpr double MOMEGA                      = 1420.0      ;
    constexpr double CTatBetaGamma               =  87.11/MTAU * 0.001;
    
    
    constexpr double MEL2                        = MEL * MEL;
    constexpr double MMU2                        = MMU * MMU;
    constexpr double MPI2                        = MPI * MPI;
    constexpr double MPI02                       = MPI0 * MPI0;
    constexpr double aPI                         = MPI / MTAU;
    constexpr double MTAU2                       = MTAU  * MTAU;
    constexpr double MTAU3                       = MTAU2 * MTAU;
    constexpr double MTAU3_                      = MTAU3 * _MeV2GeV * _MeV2GeV * _MeV2GeV;
    constexpr double MTAU2_4                     = MTAU2*4.0;
    constexpr double MTAU_099                    = MTAU * 0.99;
    constexpr double INV_MTAU_2                  = 0.5/MTAU;
    
    constexpr double aPI_p_1                     = 1.0 + aPI;
    constexpr double aPI_m_1                     = 1.0 - aPI;
    
    constexpr double gTauV                       = -0.03660;
    constexpr double gTauA                       = -0.50204;
    constexpr double gTauPlus                    = (gTauV + gTauA)*0.5;
    constexpr double gTauMinus                   = (gTauV - gTauA)*0.5;
    constexpr double gTauPM                      = gTauPlus * gTauMinus;
    constexpr double gTauPlus2                   = gTauPlus * gTauPlus;
    constexpr double gTauMinus2                  = gTauMinus * gTauMinus;
    
    constexpr double Z_MASS                      = 91.1876 ;
    constexpr double Z_WIDTH                     = 2.4952  ;
    constexpr double Z_MASS2                     = Z_MASS*Z_MASS;
    constexpr double Z_WIDTH2                    = Z_WIDTH*Z_WIDTH;
    constexpr double Z_WM2                       = Z_MASS2 * Z_WIDTH2;
    
    constexpr int MaxLag                         = 200;
    constexpr int MaxACT                         = 500;
    
    constexpr double ATLAS_Bz                    = 1.99414;
    constexpr double ATLAS_A                     = -0.299792458 * ATLAS_Bz;
    constexpr int    N_Chain                     = 4;
    constexpr int    N_MIN_STEP                  = 20000;
    constexpr int    N_MIN_STEP_PerChain         = N_MIN_STEP/N_Chain;
    constexpr int    N_MIN_STEP_PerChain_250     = N_MIN_STEP_PerChain/250;
    constexpr int    N_MAX_STEP_PerChain         = N_MIN_STEP_PerChain * 1.2;
    constexpr int    N_Additional_Check          = 25;
    constexpr int    N_Additional_Step           = 1000;
    constexpr int    N_Additional_Step_250       = N_Additional_Step/250;
    constexpr int    SeedLength                  = 100;
    constexpr int    N_InitTry                   = 200;
    
    constexpr double R_GelmanRubin_Loop             = 1e-6;
    constexpr double InitalValueThresoldForSamplingLow = -100.0;
    constexpr double InitalValueThresoldForSamplingHigh = -20.0;
    
    constexpr double LARGEVALUE_Mis              = 1e+10;
    constexpr double LARGEVALUE_SV               = 2e+3;
    constexpr double LARGEVALUE                  = 1e+10;
    
    constexpr double MET_RefFinal_Param_Mean_0   = -1.38028;
    constexpr double MET_RefFinal_Param_Mean_1   = -0.05258;
    constexpr double MET_RefFinal_Param_Mean_2   =  0.84153;

    constexpr double MET_RefFinal_Param_Sigma_0  =  5.7337;
    constexpr double MET_RefFinal_Param_Sigma_1  =  0.1108;
    constexpr double MET_RefFinal_Param_Sigma_2  =  0.7222;

    constexpr double MET_RefFinal_Param_Gamma_0  = -0.59379;
    constexpr double MET_RefFinal_Param_Gamma_1  =  0.55224;
    constexpr double MET_RefFinal_Param_Gamma_2  =  0.14547;

    constexpr double MET_RefFinal_Param_Offset_0 =  0.09636;
    constexpr double MET_RefFinal_Param_Offset_1 =  2.423e-05;
    constexpr double MET_RefFinal_Param_Offset_2 =  1.34425;

    constexpr double PtVal_1p0n                  =  25.0 * _GeV2MeV;
    constexpr double PtVal_1p1n                  =  50.0 * _GeV2MeV;
    constexpr double PtVal_1pXn                  =  50.0 * _GeV2MeV;
    constexpr double PtVal_3p0n                  =  75.0 * _GeV2MeV;
    constexpr double PtVal_3pXn                  =  75.0 * _GeV2MeV;
    constexpr double FracMassSampling            =  0.5;
    constexpr double MAX_MASSSAMPLING            =  10.0*1000.0*1000.0;// 10TeV
    constexpr double pt_min_JVT                  = 20.0;
    constexpr double pt_max_JVT                  = 50.0;
    constexpr double eta_max_JVT                 = 2.4;
    constexpr double jvt_min_JVT                 = 0.64;
    
    
    
    
    typedef std::array< double,               enumToSize_t(MET_Resol_PPar::N) > Array_MET_Resol_PPar;
    typedef std::array< Array_MET_Resol_PPar, enumToSize_t(MET_nJet::N)       > Array_MET_Resol_Par;
    typedef std::array< Array_MET_Resol_Par,  enumToSize_t(MET_Resol_Par::N)  > Array_MET_Resol;
    typedef std::array< Array_MET_Resol,      enumToSize_t(MET_Type::N)       > Array_MET;
    typedef std::array< Array_MET,            enumToSize_t(CalibType::N)      > Array_MET_All;

    /*
    constexpr Array_MET_Resol_Par MET_TST_Sigma1 {{
      { 3.71942e+01, -3.44014e+01, -1.30189e-03 }, // nJet  = 0
      { 1.55636e+01, -1.28264e+01 ,-4.00206e-03 }, // nJet  = 1
      { 1.51502e+01, -1.24675e+01, -4.21471e-03 }, // nJet >= 2
      { 1.65080e+01, -1.38072e+01, -3.71105e-03 }  // nJet Inclsive
      }};
    constexpr Array_MET_Resol_Par MET_TST_Sigma2 {{
      { 1.27078e+02, -1.21322e+02, -1.33209e-03 }, // nJet  = 0
      { 3.27970e+01, -2.97873e+01, -6.73190e-03 }, // nJet  = 1
      { 3.30829e+01, -2.76170e+01, -6.83110e-03 }, // nJet >= 2
      { 3.16511e+01, -2.62503e+01, -7.37146e-03 }  // nJet Inclsive
      }};
    constexpr Array_MET_Resol_Par MET_TST_Fraction {{
      {  8.45670e-03,  2.07980e-01, -1.77538e-02 }, // nJet  = 0
      {  7.22927e-02,  9.97025e-02, -6.53052e-03 }, // nJet  = 1
      {  9.02859e-02,  1.23716e-01, -3.83476e-02 }, // nJet >= 2
      {  9.16522e-02,  1.22977e-01, -3.95831e-02 }  // nJet Inclsive
      }};
    
    constexpr Array_MET_Resol_Par MET_TST_Sigma1 {{
      {  -2.90167e+02 ,   2.92992e+02 ,   1.40285e-04 }, // nJet  = 0
      {   1.61380e+01 ,  -1.37601e+01 ,  -4.03146e-03 }, // nJet  = 1
      {   1.61380e+01 ,  -1.37601e+01 ,  -4.03146e-03 }, // nJet >= 2
      {   1.61380e+01 ,  -1.37601e+01 ,  -4.03146e-03 }  // nJet Inclsive
      }};
    constexpr Array_MET_Resol_Par MET_TST_Sigma2 {{
      {   1.59850e+03 ,  -1.59278e+03 ,  -9.70264e-05  }, // nJet  = 0
      {   3.57354e+01 ,  -3.06368e+01 ,  -5.36912e-03  }, // nJet  = 1
      {   3.57354e+01 ,  -3.06368e+01 ,  -5.36912e-03  }, // nJet >= 2
      {   3.57354e+01 ,  -3.06368e+01 ,  -5.36912e-03  }  // nJet Inclsive
      }};
    constexpr Array_MET_Resol_Par MET_TST_Fraction {{
      {   1.50791e-03 ,   2.29573e-01 ,  -1.31639e-02  }, // nJet  = 0
      {  -2.02074e-03 ,   1.12788e-01 ,  -2.91117e-04  }, // nJet  = 1
      {  -2.02074e-03 ,   1.12788e-01 ,  -2.91117e-04  }, // nJet >= 2
      {  -2.02074e-03 ,   1.12788e-01 ,  -2.91117e-04  }  // nJet Inclsive
      }};
     */
    
    constexpr Array_MET_Resol_Par MET_TST_Sigma1_MC15_50ns_v1 {{
        { {-2.90208e+02,   2.92951e+02,   1.38010e-04 } }, // nJet  = 0
        { { 1.39347e+01,  -1.17393e+01,  -5.09457e-03 } }, // nJet  = 1
        { { 1.39347e+01,  -1.17393e+01,  -5.09457e-03 } }, // Other 
      }};
    constexpr Array_MET_Resol_Par MET_TST_Sigma2_MC15_50ns_v1 {{
      { { 1.59838e+03,  -1.59290e+03,  -9.86763e-05 } }, // nJet  = 0
      { { 3.55611e+01,  -3.05389e+01,  -5.39623e-03 } }, // nJet  = 1
      { { 3.55611e+01,  -3.05389e+01,  -5.39623e-03 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_TST_Fraction_MC15_50ns_v1 {{
      { { 3.14084e-03,   2.39054e-01,  -1.39546e-02 } }, // nJet  = 0
      { { 1.05361e-01,   2.47972e-02,  -2.22847e-02 } }, // nJet  = 1
      { { 1.05361e-01,   2.47972e-02,  -2.22847e-02 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_MPT_Sigma1_MC15_50ns_v1 {{
      { {-2.90039e+02,   2.93105e+02,   4.76410e-05 } }, // nJet  = 0
      { { 1.41418e+01,  -1.19241e+01,  -4.35911e-03 } }, // nJet  = 1
      { { 1.41418e+01,  -1.19241e+01,  -4.35911e-03 } }, // Other 
      }};
    constexpr Array_MET_Resol_Par MET_MPT_Sigma2_MC15_50ns_v1 {{
      { { 1.76738e+01,  -1.04491e+01,  -8.79671e-03 } }, // nJet  = 0
      { { 3.32265e+01,  -2.63648e+01,  -4.99367e-03 } }, // nJet  = 1
      { { 3.32265e+01,  -2.63648e+01,  -4.99367e-03 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_MPT_Fraction_MC15_50ns_v1 {{
      { { 3.78012e-01,  -2.27296e-01,  -8.62075e-03 } }, // nJet  = 0
      { {-2.65291e+00,   2.76240e+00,   7.03012e-05 } }, // nJet  = 1
      { {-2.65291e+00,   2.76240e+00,   7.03012e-05 } }, // Other
      }};
    
    constexpr Array_MET_Resol_Par MET_TST_Soft_Sigma1_MC15_50ns_v1 {{
        { { 8.77649e+00 , -5.33584e+00 , -2.33474e-02 } }, // nJet  = 0
        { { 8.77649e+00 , -5.33584e+00 , -2.33474e-02 } }, // nJet  = 1
        { { 8.77649e+00 , -5.33584e+00 , -2.33474e-02 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_TST_Soft_Sigma2_MC15_50ns_v1 {{
        { { 2.02870e+01 , -1.08588e+01 , -3.41941e-02 } }, // nJet  = 0
        { { 2.02870e+01 , -1.08588e+01 , -3.41941e-02 } }, // nJet  = 1
        { { 2.02870e+01 , -1.08588e+01 , -3.41941e-02 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_TST_Soft_Fraction_MC15_50ns_v1 {{
        { { 1.75776e-01 ,  2.16514e-01 , -5.73283e-02 } }, // nJet  = 0
        { { 1.75776e-01 ,  2.16514e-01 , -5.73283e-02 } }, // nJet  = 1
        { { 1.75776e-01 ,  2.16514e-01 , -5.73283e-02 } }, // Other
      }};
    // Truth
    constexpr Array_MET_Resol_Par MET_Truth_Sigma1_MC15_50ns_v1 {{
        { { 1.0, 0.0, 0.0 } }, // nJet  = 0
        { { 1.0, 0.0, 0.0 } }, // nJet  = 1
        { { 1.0, 0.0, 0.0 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_Truth_Sigma2_MC15_50ns_v1 {{
        { { 0.01, 0.0, 0.0 } }, // nJet  = 0
        { { 0.01, 0.0, 0.0 } }, // nJet  = 1
        { { 0.01, 0.0, 0.0 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_Truth_Fraction_MC15_50ns_v1 {{
        { { 1.0, 0.0, 0.0 } }, // nJet  = 0
        { { 1.0, 0.0, 0.0 } }, // nJet  = 1
        { { 1.0, 0.0, 0.0 } }, // Other
      }};
    // Smear
    constexpr Array_MET_Resol_Par MET_Smear_Sigma1_MC15_50ns_v1 {{
        { { 15.0, 0.0, 0.0 } }, // nJet  = 0
        { { 15.0, 0.0, 0.0 } }, // nJet  = 1
        { { 15.0, 0.0, 0.0 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_Smear_Sigma2_MC15_50ns_v1 {{
        { { 0.01, 0.0, 0.0 } }, // nJet  = 0
        { { 0.01, 0.0, 0.0 } }, // nJet  = 1
        { { 0.01, 0.0, 0.0 } }, // Other
      }};
    constexpr Array_MET_Resol_Par MET_Smear_Fraction_MC15_50ns_v1 {{
        { { 1.0, 0.0, 0.0 } }, // nJet  = 0
        { { 1.0, 0.0, 0.0 } }, // nJet  = 1
        { { 1.0, 0.0, 0.0 } }, // Other
      }};
    
    constexpr Array_MET MET_Resol_MC15_50ns_v1 {{
        { { MET_TST_Sigma1_MC15_50ns_v1, MET_TST_Sigma2_MC15_50ns_v1, MET_TST_Fraction_MC15_50ns_v1 } }, // MET_TST,
        { { MET_MPT_Sigma1_MC15_50ns_v1, MET_MPT_Sigma2_MC15_50ns_v1, MET_MPT_Fraction_MC15_50ns_v1 } }, // MET_MPT,
        { { MET_TST_Soft_Sigma1_MC15_50ns_v1, MET_TST_Soft_Sigma2_MC15_50ns_v1, MET_TST_Soft_Fraction_MC15_50ns_v1 } }, // MET_TST_Soft,
        { { MET_Truth_Sigma1_MC15_50ns_v1, MET_Truth_Sigma2_MC15_50ns_v1, MET_Truth_Fraction_MC15_50ns_v1 } }, // MET_Truth,
        { { MET_Smear_Sigma1_MC15_50ns_v1, MET_Smear_Sigma2_MC15_50ns_v1, MET_Smear_Fraction_MC15_50ns_v1 } }, // MET_Truth,
        }};
    constexpr Array_MET_All MET_Resol_Table {{ MET_Resol_MC15_50ns_v1, MET_Resol_MC15_50ns_v1, }};
    
    
    // Jet Resolution Parameter 
    constexpr std::array<double, enumToSize_t(Jet_Eta::N) > JetResol_NTerm{ { 3.330, 3.040, 3.340, 2.900 } };// Noise term
    constexpr std::array<double, enumToSize_t(Jet_Eta::N) > JetResol_STerm{ { 0.710, 0.690, 0.610, 0.460 } };
    constexpr std::array<double, enumToSize_t(Jet_Eta::N) > JetResol_CTerm{ { 0.030, 0.036, 0.044, 0.053 } };
    
    
    
  }
}

#endif // MOSAIC_CONSTANT_H
