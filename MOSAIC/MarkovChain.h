// Dear emacs, this is -*- c++ -*- 
/*
 * MarkovChain
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_MARCOVCHAIN_H
#define MOSAIC_MARCOVCHAIN_H

#include <array>
#include <vector>
#include <iterator>
#include <iostream>
#include <random>

#include "MOSAIC/PDFManager.h"
#include "MOSAIC/IOManager.h"
#include "MOSAIC/Constant.h"
#include "MOSAIC/Objects.h"

namespace Mosaic {
  template <typename Lambda>           void unroller(uint_<0>, Lambda lmd) ;
  template <size_t N, typename Lambda> void unroller(uint_<N>, Lambda lmd) ;
  
  
  template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode > 
  class MarkovChain {
  public:
    MarkovChain( std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> seed, Mosaic::InputManager *InputManager, Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pDFManager ) ;
    ~MarkovChain() ; 
    void process_impl() ; 
    void init();
    void Clear();
    void Sampling( Mosaic::LikelihoodType::Default );
    void Update  ( Mosaic::LikelihoodType::Default );
    void ReInit  ( Mosaic::LikelihoodType::Default );
#ifdef MOSAIC_USE_DEVELOP
    void Sampling( Mosaic::LikelihoodType::MEOnly );
    void Update  ( Mosaic::LikelihoodType::MEOnly );
    void ReInit  ( Mosaic::LikelihoodType::MEOnly );
    void Sampling( Mosaic::LikelihoodType::METOnly );
    void Update  ( Mosaic::LikelihoodType::METOnly );
    void ReInit  ( Mosaic::LikelihoodType::METOnly );
    void Sampling( Mosaic::LikelihoodType::PlusVisResoltion );
    void Sampling( Mosaic::LikelihoodType::RecoilSystem );
    void Sampling( Mosaic::LikelihoodType::PartDF_LHAPDF );
    void Update  ( Mosaic::LikelihoodType::PlusVisResoltion );
    void Update  ( Mosaic::LikelihoodType::RecoilSystem );
    void Update  ( Mosaic::LikelihoodType::PartDF_LHAPDF );
    void ReInit  ( Mosaic::LikelihoodType::PlusVisResoltion );
    void ReInit  ( Mosaic::LikelihoodType::RecoilSystem );
    void ReInit  ( Mosaic::LikelihoodType::PartDF_LHAPDF );
#endif
    int    Get_nAccept()         const ;
    bool   IsNotEmpty()          const ;
    double UniRndm()             const ;
    double GetAccepRatet()       const ;
    std::vector< Mosaic::SamplingPoint >&& results( Mosaic::RunningMode::Nominal );
    std::vector< Mosaic::SamplingPoint >&& results( Mosaic::RunningMode::Fast );
    template<size_t N_LOOP> void process( uint_<N_LOOP> ) ;
    
    int nStep;
    int nButStep;
    int nOKStep;
    int nAccept;
    
    std::mt19937 *Engine;
    std::uniform_real_distribution<double> *Uniform;
    
    Mosaic::SamplingPoint LLCandidate;
    std::vector< SamplingPoint > LLs;
    MCMCTau1 *Tau1;
    MCMCTau2 *Tau2;
    Mosaic::MCMCRecoilSystem * recoilSystem;
    Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pdfManager;
  };
  
  template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN> 
  class MarkovChainSet {
  public:
    MarkovChainSet(Mosaic::InputManager *InputManager, Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pDFManager ) ;
    ~MarkovChainSet() ;
    
    void setChain(Mosaic::InputManager *InputManager, Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pDFManager, uint_<N_CHAIN>) ;
    void isNotEmpty_impl( uint_<N_CHAIN> ) ;
    void init_impl( uint_<N_CHAIN> ) ;
    void clear( uint_<N_CHAIN> ) ;
    void results( uint_<N_CHAIN>, std::vector< SamplingPoint>& ret) ;
    void makeGelmanInput( uint_<N_CHAIN>,  std::array< std::tuple<double, double, int>, N_CHAIN >& mean_vars) ;
    
    template<size_t I> void setChain(Mosaic::InputManager *InputManager, Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pDFManager, uint_<I>) ;
    template<size_t I> void isNotEmpty_impl( uint_<I> ) ;
    template<size_t I> void clear( uint_<I> ) ;
    template<size_t I> void init_impl( uint_<I> );
    template<size_t I> void results( uint_<I>, std::vector< SamplingPoint>& ret) ;
    template<size_t I> void makeGelmanInput( uint_<I>,        std::array< std::tuple<double, double, int>, N_CHAIN >& mean_vars ) ;
    template<size_t I> void process_addt_impl( uint_<I> );
    template<size_t I> void process_main_impl( uint_<I> );
    
    std::vector< SamplingPoint> Results( ) ;
    double IsConverge();
    void init();
    void process_addt_impl( uint_<N_CHAIN> );
    void process_main_impl( uint_<N_CHAIN> );
    bool process_additional();
    bool process_main();
    
    bool m_isConverge;
    int nCurrentStep;
    std::array< MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode> *, N_CHAIN> *Chains;
  };
}

#include "MOSAIC/MarkovChain.icc"
  
#endif // MOSAIC_MARCOVCHAIN_H
  
