// Dear emacs, this is -*- c++ -*- 
/*
 * MOSAICalculator
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_MOSAICALCULATOR_H
#define MOSAIC_MOSAICALCULATOR_H

#include "MOSAIC/IOManager.h"
#include "MOSAIC/MarkovChain.h"
#include "MOSAIC/PDFManager.h"
#include "MOSAIC/ResolModel.h"
#include "MOSAIC/Constant.h"
#include "MOSAIC/Objects.h"

#include <string>
#include <iostream>
#include <iomanip>
#include <random>
#include <vector>

class MOSAICalculator {
 public:
  MOSAICalculator( std::string m_LHAPDF_Type );
  virtual ~MOSAICalculator();
  
  void Clear();
  void Init_Spline_Rho();
  void Init_Spline_a1();
  bool Run_Mosaic_Z();
  bool Run_Mosaic_H();
  bool Run_Mosaic_A();
  bool init();
  void setRunMode( std::string = "NOMINAL");
  void setCalibType ( Mosaic::CalibType e );
  void setMETType   ( Mosaic::MET_Type  e );
  void setUseMET_MPT( bool e );
  void setLikelihoodType( std::string llhtype );
  double Mass_Z( ) const ;
  double Mass_H( ) const ;
  double Mass_A( ) const ;
  
  template<typename MCMCTau1, typename MCMCTau2, typename PDF1, typename PDF2, typename PDFDecayME, typename ResolModel_Vis1, typename ResolModel_Vis2, typename LLHType, typename RunMode> 
  bool run(PDF1* pdf1, PDF2* pdf2, PDFDecayME* pDFDecayME, Mosaic::OutputManager* outputManager);
  
  template<typename PDFDecayME, typename LLHType, typename RunMode>
  bool Run_Mosaic(PDFDecayME * pdfDecayME, Mosaic::OutputManager * outputManager);
  
  Mosaic::InputManager        *inputManager;
  Mosaic::OutputManager       *outputManager_Spin1;
  Mosaic::OutputManager       *outputManager_CPEven;
  Mosaic::OutputManager       *outputManager_CPOdd;
  
 private:
  const std::string           m_LHAPDFType;
  bool                        m_useMET_MPT; 
  std::string                 m_LLHType;
  std::string                 m_RunMode;
  Mosaic::CalibType           m_CalibType;
  Mosaic::MET_Type            m_MET_Type;
  
  Mosaic::PDF_Spin0_CPEven    *PDF_spin0_CPEven;
  Mosaic::PDF_Spin0_CPOdd     *PDF_spin0_CPOdd;
  Mosaic::PDF_Spin1           *PDF_spin1;
  Mosaic::PDF_Pion            *PDF_pion;
  Mosaic::PDF_Spline          *PDF_rho;
  Mosaic::PDF_Spline          *PDF_a1;
  Mosaic::PDF_Lep             *PDF_El;
  Mosaic::PDF_Lep             *PDF_Mu;
#ifdef MOSAIC_USE_DEVELOP
  Mosaic::PDF_LHAPDF          *PDF_lHAPDF;
#endif
};

#include "MOSAIC/MOSAICalculator.icc"

#endif // MOSAIC_MOSAICALCULATOR_H
