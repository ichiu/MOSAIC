// Dear emacs, this is -*- c++ -*- 
/*
 * TauObj 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_TAUOBJ_H
#define MOSAIC_TAUOBJ_H

#include <vector>
#include "MOSAIC/Constant.h"
#include "MOSAIC/Objects.h"
#include "MOSAIC/AlgorithmManager.h"


namespace Mosaic {
  class TauObj : public FourVector {
  public:
    TauObj();
    //TauObj( const TauObj&  obj) = default;
    ~TauObj();
    
    friend void swap(TauObj& lhs, TauObj& rhs) noexcept { lhs.swap(rhs); }
    void swap(TauObj& obj) noexcept {
      FourVector tmp = static_cast< Mosaic::FourVector > ( obj );
      obj.set( static_cast< Mosaic::FourVector > ( *this ) );
      this->set( tmp );
      
      std::swap(decayType,             obj.decayType);
      std::swap(particleType,          obj.particleType);
      std::swap(helisityAngle,         obj.helisityAngle);
      std::swap(panTau,                obj.panTau);
      std::swap(tauRec,                obj.tauRec);
      std::swap(chargedParticles,      obj.chargedParticles);
      std::swap(neutralParticles,      obj.neutralParticles);
      std::swap(neutralOtherParticles, obj.neutralOtherParticles);
    }
    void Clear();
    bool init(std::mt19937& Engine, std::uniform_real_distribution<double>& Uniform);
    int IsLep()    const ;
    int IsEle()    const ;
    int IsMuon()   const ;
    int nCharged() const ;
    int nNeutral() const ;
    Mosaic::ParticleType DecayType()    const ;
    Mosaic::ParticleType ParticleType() const ;
    double HelisityAngle() const ;
    double CosPsi_Rho(double e, double E)     const ;
    double CosPsi_1p(double visE,                Mosaic::FourVector* pi1, Mosaic::FourVector* pi01, Mosaic::FourVector* pi02)   const ;
    double CosPsi_3p(double charge, double visE, Mosaic::FourVector* pi1, Mosaic::FourVector* pi2, Mosaic::FourVector* pi3)     const ;
    
    Mosaic::FourVector Vis() const ;
    
  public:
    Mosaic::ParticleType decayType;
    Mosaic::ParticleType particleType;
    
    double helisityAngle;
    double resolVis;
    
    Mosaic::FourVector panTau;
    Mosaic::FourVector tauRec;
    
    std::vector< Mosaic::FourVector* >* chargedParticles;
    std::vector< Mosaic::FourVector* >* neutralParticles;
    std::vector< Mosaic::FourVector* >* neutralOtherParticles;
    
    
  };
}

#endif // MOSAIC_TAUOBJ_H
