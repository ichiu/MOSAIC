// Dear emacs, this is -*- c++ -*- 
/*
 * Objects 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_OBJECTS_H
#define MOSAIC_OBJECTS_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <vector>
#include <cassert>
#include <random>
#include <functional>

#include "MOSAIC/Constant.h"

#define MOSAIC_UNUSED_VARIABLE(x) (void)(x)

namespace Mosaic {
  inline double ATan2(const double& y, const double& x) { return (x != 0) ? std::atan2(y,x) : (y == 0) ? 0.0 : (y > 0) ? M_PI/2.0 : -M_PI/2.0 ; }

  class FourVector {
  public:
    FourVector();
    FourVector( const Mosaic::FourVector& v);
    FourVector(double px_, double py_, double pz_, double e_ );
    FourVector(double px_, double py_, double pz_, double e_, double m_, double charge_ = 0.0, double socre_ = 0.0);
    virtual ~FourVector();
    FourVector& operator=(const FourVector& v){ this->set(v); return *this; } 
    //void operator=(const FourVector& v){ this->set(v); }
   
    void clear();
    void set( const FourVector& v );
    void set(double px_, double py_, double pz_, double e_, double m_, double charge_ = 0.0, double score_ = 0.0);
    void Boost_impl2(const FourVector& b, const double& gamma, const double& bp, const double& C);
    void setMass(double m_, bool isOK);
    void setMass();
    void Boost_impl(const FourVector& b, const double& gamma, const double& gamma2, const double& bp);
    void Boost     (const FourVector& b, const double& gamma, const double& gamma2) ;
    double InnerProduct(const FourVector& v) const ;
    double px()      const ;
    double py()      const ;
    double pz()      const ;
    double pt()      const ;
    double e()       const ;
    double m()       const ;
    double p()       const ;
    double p2()      const ;
    double charge()  const ;
    double score()   const ;
    double phi()     const ;
    double theta()   const ;
    double eta()     const ;
  private:
    double _px, _py, _pz, _pt, _p2, _p, _e, _m, _charge, _score;
    
  };
  class MissingEt {
  public:
    MissingEt();
    ~MissingEt() = default;
    MissingEt& operator = (const MissingEt& v){ this->set(v.MEx(), v.MEy(), v.SumEt()); return *this; }
    
    inline void clear() { _MEx = _MEy = _SumEt = 0.0; }
    inline void set(double mex, double mey, double sumet) { _MEx = mex; _MEy = mey; _SumEt = sumet; }
    inline double MEx()   const { return _MEx; }
    inline double MEy()   const { return _MEy; }
    inline double SumEt() const { return _SumEt; }
    inline double Sig()   const { return std::hypot( MEx(), MEy() ) / 1000. /sqrt( _SumEt / 1000. ); }
  private:
    double _MEx, _MEy, _SumEt; 
    
  };
  class Jet : public FourVector {
  public:
    Jet();
    Jet( double px_, double py_, double pz_, double e_, double m_, double charge_ = 0.0, double socre_ = 0.0);
    Jet( const Mosaic::FourVector& v);
    Jet( const Mosaic::Jet& v);
    ~Jet() = default;

    inline Mosaic::FourVector getShiftedVector_impl(double Px, double Py, double Pz, double M){ return FourVector(Px, Py, Pz, sqrt(Px*Px + Py*Py + Pz*Pz + M*M), M); }
    inline Mosaic::FourVector getShiftedVector(double R){ return getShiftedVector_impl(px()*R, py()*R, pz()*R, m()); }
    inline double getSigma(double newPt_GeV)                const { return sqrt( N2/newPt_GeV/newPt_GeV + S2/newPt_GeV + C2 ) * newPt_GeV; }
    inline double Prob_impl(double newPt_GeV, double sigma) const { return -0.5 * pow((newPt_GeV - Pt_GeV)/sigma, 2) - Mosaic::Constant::LOG_PI2 - log(sigma);  }
    inline double Prob(double R)                            const { return Prob_impl(Pt_GeV * R, getSigma( Pt_GeV * R ) ); }
  private:
    const double N2, S2, C2, Pt_GeV;
  };
  
  class MCMCParameter {
  public:
    MCMCParameter(double _x0, double _xDelta, std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> seq);
    ~MCMCParameter();
    void ReInit(double Low, double High);
    void Sampling(double Low, double High);
    void Set(double v);
    void SetC(double v);
    void Update();
    void KeepMax();
    void SetValueCandidate(double v);
    double ValuePropose()   const ;
    double ValueCandidate() const ;
    double ValueMax()       const ;
  private:
    const double xDelta;
    double xCandidate, xPropse, xMax;
    std::mt19937 *Engine;
    std::uniform_real_distribution<double> *Uniform;
    
  };
  
  class MCMCHad {
  public:
    MCMCHad(double _X0, double _MissMass0, double _phi0, double _mass, double _evis, std::vector< std::array< std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs);
    ~MCMCHad();
    
    void ReInit();
    void Sampling();
    void Update();
    void Keep();
    void KeepMax();
    double EnergyFraction_ValuePropose()     const;
    double EnergyFraction_ValueCandidate()   const;
    double DecayPhi_ValuePropose()           const;
    double DecayPhi_ValueCandidate()         const;
    double MissingMass_ValuePropose()        const;
    double MissingMass_ValueCandidate()      const;
    double EnergyResolution_ValuePropose()   const;
    double EnergyResolution_ValueCandidate() const;
  private:
    MCMCParameter  *EnergyFraction, *DecayPhi, *EnergyResol;
  };
  
  class MCMCLep {
  public:
    MCMCLep(double _X0, double _MissMass0, double _phi0, double _mass, double _evis, std::vector< std::array< std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs);
    ~MCMCLep();
    
    void ReInit();
    void Sampling() ;
    bool isOK_impl(double x, double B);
    bool isOK(double x, double b);
    void Update();
    void Keep();
    void KeepMax();
    double EnergyFraction_ValuePropose()     const;
    double EnergyFraction_ValueCandidate()   const;
    double DecayPhi_ValuePropose()           const;
    double DecayPhi_ValueCandidate()         const;
    double MissingMass_ValuePropose()        const;
    double MissingMass_ValueCandidate()      const;
    double EnergyResolution_ValuePropose()   const;
    double EnergyResolution_ValueCandidate() const;
  private:
    const double a, A, C, M, A_p_1, A_m_1_2;
    MCMCParameter  *EnergyFraction, *MissingMass, *DecayPhi, *EnergyResol;
  };
  
  class MCMCRecoilSystem {
  public:
    MCMCRecoilSystem(const std::vector< Mosaic::Jet* >* jets, std::vector< std::array< std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs);
    ~MCMCRecoilSystem();

    void Sampling();
    void ReInit();
    void Update();
    void KeepMax();
    double TotalProb() const ;
    double DeltaPx()   const ;
    double DeltaPy()   const ;
    
  private:
    const int sizeJet;
    double totalProb, dPx, dPy;
    std::vector< Mosaic::Jet*           > * recoilJets;
    std::vector< Mosaic::MCMCParameter* > * recoilResolution;
    
  };
  
  class Spline {
  public:
    Spline(double _a, double _b, double _c, double _d, double _x0);
    Spline(const Spline& s);
    ~Spline();
    
    double Value_impl(double XX) const ;
    double Value(double X)       const ;
  private:
    const double a, b, c, d, x0;
  };
  
  class SplineSet {
  public:
    SplineSet(double _LowX, double _dX, std::initializer_list<double> y); 
    ~SplineSet();
    
    double Value(double x)    const ;
    int    Bin_impl(int s)    const ;
    int    Bin(double x)      const ;
  private:
    const double LowX, dX;
    const int SetSize;
    std::vector<Spline> Set;
  };
  
  class Angle {
  public:
    Angle();
    Angle(double a);
    Angle(const Angle& a);
    virtual ~Angle();
    
    const double angle, Sin, Cos, Tan;
  };
  
  class AngleSet {
  public:
    AngleSet();
    AngleSet(Angle P, Angle T);
    AngleSet(const AngleSet& a);
    ~AngleSet();
    
    const double CosP_CosT, CosP_SinT, SinP_CosT, SinP_SinT;
  };
  
  class SamplingPoint {
  public:
    SamplingPoint();
    SamplingPoint(double x1_, double x2_, double px1_, double py1_, double pz1_, double px2_, double py2_, double pz2_, double mVis_, double mass_, double prob_);
    SamplingPoint(double x1_, double x2_, double px1_, double py1_, double pz1_, double px2_, double py2_, double pz2_, double mVis_, double mass_, double met_, double me_);
    SamplingPoint(double x1_, double x2_, double px1_, double py1_, double pz1_, double px2_, double py2_, double pz2_, double mVis_, double mass_, double met_, double resol_, double me_);
    SamplingPoint(double x1_, double x2_, double px1_, double py1_, double pz1_, double px2_, double py2_, double pz2_, double mVis_, double mass_, double met_, double resol_, double me_, double partonDF_);
    SamplingPoint( const Mosaic::SamplingPoint&  p ) = delete;
    SamplingPoint(       Mosaic::SamplingPoint&& p ) noexcept;
    ~SamplingPoint() = default;
    SamplingPoint& operator=( const SamplingPoint&  p ) = delete;
    SamplingPoint& operator=(       SamplingPoint&& p ) noexcept {
      _x1       = std::move( p._x1 );
      _x2       = std::move( p._x2 );
      _x12      = std::move( p._x12 );
      _px1      = std::move( p._px1 );
      _py1      = std::move( p._py1 );
      _pz1      = std::move( p._pz1 );
      _px2      = std::move( p._px2 );
      _py2      = std::move( p._py2 );
      _pz2      = std::move( p._pz2 );
      _mVis     = std::move( p._mVis );
      _mass     = std::move( p._mass );
      _met      = std::move( p._met );
      _resol    = std::move( p._resol );
      _me       = std::move( p._me );
      _partonDF = std::move( p._partonDF );
      _prob     = std::move( p._prob );
      return *this;
    }
      
    double x1()       const;
    double x2()         const;
    double x12()       const;
    double px_mis1()   const;
    double py_mis1()   const;
    double pz_mis1()   const;
    double px_mis2()   const;
    double py_mis2()   const;
    double pz_mis2()   const;
    double probMET()   const;
    double probME()    const;
    double probResol() const;
    double probPDF()   const;
    double prob()      const;
    double mVis()      const;
    double mass()      const;
  private:
    double  _x1, _x2, _x12, _px1, _py1, _pz1, _px2, _py2, _pz2, _mVis, _mass, _met, _resol, _me, _partonDF, _prob;
  };

  
}

#endif // MOSAIC_OBJECTS_H

  

