// Dear emacs, this is -*- c++ -*- 
/*
 * AlgorithmManager 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_ALGORITHMMANAGER_H
#define MOSAIC_ALGORITHMMANAGER_H


#include <math.h>
#include <iostream>
#include <iterator>
#include <set>
#include <algorithm>
#include "MOSAIC/Constant.h"
#include "MOSAIC/Objects.h"

#define MOSAIC_UNUSED_VARIABLE(x) (void)(x)

namespace Mosaic {
  template <size_t N> struct uint_{ };
 

  namespace AlgorithmManager {
    inline int TauDecayMode(int ncharged, int nneutral){ 
      if     ( ncharged == 1 ) return (nneutral == 0) ? 0 : (nneutral == 1) ? 1 : 2;
      else if( ncharged == 3 ) return (nneutral == 0) ? 3 : 4;
      return 5;
    }
    //inline double Prob_impl(double p)                                 { return (p!=p) ? 1e-10 : (p<=0.0)? 0.0 : p; }
    inline double DeltaPhi_impl(double dphi)                          { return (dphi > M_PI) ? 2.0*M_PI - dphi : dphi; }
    inline double DeltaPhi(double phi1, double phi2)                  { return DeltaPhi_impl(fabs(phi1 - phi2)); }
    inline double DeltaEta(double eta1, double eta2)                  { return fabs(eta1 - eta2); }
    inline double DeltaR(double eta1, double phi1, double eta2, double phi2) { return std::hypot(DeltaEta(eta1, eta2), DeltaPhi(phi1, phi2)); }
    inline double ATan2(double y, double x)                           { return (x != 0) ? atan2(y,x) : (y == 0) ? 0.0 : (y > 0) ? M_PI/2.0 : -M_PI/2.0 ; }
    inline double Prob_impl_Thresold(double p, double thrhld)         { return (p!=p) ? -1e+10 : (p<=thrhld)? -1e+10 : log(p); }
    inline double Prob_impl(double p)                                 { return (p!=p) ? -1e+10 : (p<=0.0)? -1e+10 : log(p); }
    inline double ProbLog_impl(double p)                              { return (p!=p) ? -1e+10 : p ; }
    inline double CheckProb(double p)                                 { return (p!=p) ? 0.0 : ( p <= 0.0) ? 0.0 : p ; }
    //inline std::pair<double, double>&& make_pair(double t1, double t2) { std::cout << t1 << " / " << t2 << std::endl;std::pair<double, double> p{t1, t2}; return std::move(p); }
    
    inline double MET_Resolution_impl2(bool isCheckFraction, double v){ return  ( ! isCheckFraction ) ? v : ( v <= 0.0 ) ? 1.0E-04 : ( v >= 1.0 ) ? 0.99999 : v ; }
    inline double MET_Resolution_impl(bool isCheckFraction, double x, double p0, double p1, double p2){ return MET_Resolution_impl2(isCheckFraction, p0 + p1* exp( x * p2) ); }
    inline double MET_Resolution(double sumEt,  std::array<double, Mosaic::enumToSize_t(Mosaic::MET_Resol_PPar::N)> par, bool isCheckFraction = false){ 
      return MET_Resolution_impl(isCheckFraction, sumEt, 
                                     std::get< Mosaic::enumToSize_t( Mosaic::MET_Resol_PPar::Par0 ) >(par),  
                                     std::get< Mosaic::enumToSize_t( Mosaic::MET_Resol_PPar::Par1 ) >(par),  
                                     std::get< Mosaic::enumToSize_t( Mosaic::MET_Resol_PPar::Par2 ) >(par) );
    }
    inline double MET_Resolution(double sumEt, Mosaic::CalibType calibType, Mosaic::MET_Type met_Type, Mosaic::MET_Resol_Par met_resol_par, Mosaic::MET_nJet nJet, bool isCheckFraction = false){ 
      return Mosaic::AlgorithmManager::MET_Resolution(sumEt,Mosaic::Constant::MET_Resol_Table.at(Mosaic::enumToSize_t(calibType)).at(Mosaic::enumToSize_t(met_Type)).at(Mosaic::enumToSize_t(met_resol_par)).at(Mosaic::enumToSize_t(nJet)),isCheckFraction);
    }
    // Taken from Truth Smearing Function, 
    inline double ElectronResolution_impl(double e, double S, double C){ return sqrt( 0.3*0.3 + S*S*e + C*C*e*e ) * Mosaic::Constant::_GeV2MeV; }
    inline double ElectronResolution( FourVector vis ){ return ElectronResolution_impl(vis.e()*Mosaic::Constant::_MeV2GeV, fabs(vis.eta()) < 1.4 ? 0.10 : 0.15, fabs(vis.eta()) < 1.4 ? 0.01 : 0.015); }

    // Taken from Truth Smearing Function, 
    inline double MuonResolution_impl2(double sigma_ID, double sigma_MS){ return fabs(sigma_ID*sigma_MS/sqrt(sigma_ID*sigma_ID + sigma_MS*sigma_MS)) * Mosaic::Constant::_GeV2MeV; }
    inline double MuonResolution_impl (double pt, double a1, double a2, double b0, double b1, double b2){ return MuonResolution_impl2(pt * sqrt(a1*a1 + a2*a2*pt*pt), pt * sqrt(b0*b0/pt/pt + b1*b1 + b2*b2*pt*pt)); }
    inline double MuonResolution_impl_central(double pt) { return MuonResolution_impl(pt, 0.01607, 0.000307, 0.24000, 0.02676, 0.00012); }
    inline double MuonResolution_impl_forward(double pt) { return MuonResolution_impl(pt, 0.03000, 0.000387, 0.00000, 0.03880, 0.00016); }
    inline double MuonResolution( FourVector vis ){ return fabs( vis.eta() ) < 1.05 ? MuonResolution_impl_central( vis.pt()*Mosaic::Constant::_MeV2GeV ) : MuonResolution_impl_forward( vis.pt()*Mosaic::Constant::_MeV2GeV ); }
    
    // Taken from Truth Smearing Function, 
    inline double TauResolution_impl(double e, double param) { return e * fabs( sqrt(param*param/e + 0.03*0.03) ) * Mosaic::Constant::_GeV2MeV; }
    inline double TauResolution( FourVector vis_tauRec, FourVector vis_panTau, int nprong ){ 
      MOSAIC_UNUSED_VARIABLE(vis_panTau); 
      return TauResolution_impl(vis_tauRec.e() * Mosaic::Constant::_MeV2GeV, (nprong == 1) ? 0.62 : (nprong == 3) ? 0.76 : 0.70 ); 
    }
    inline std::pair<double,double>&& Pair_Zero()                     { return std::move( std::make_pair(0.0, 0.0) ); }
    
    // Taken from Truth Smearing Function
    inline double TruthMETResolution( double sumet ){ MOSAIC_UNUSED_VARIABLE(sumet) ; ;return 0.15; }
    
    
    inline double Momentum(double px, double py, double pz){ return sqrt(px*px + py*py + pz*pz); }
    inline double TauMass_impl(double TauP,  double VisE, double MisP){ return sqrt( pow(VisE+MisP, 2) - TauP*TauP); }
    inline double TauMass(double TauP, double TauPx, double TauPy, double TauPz, double VisPx, double VisPy, double VisPz, double VisE){ return TauMass_impl(TauP, VisE, Momentum(TauPx-VisPx,TauPy-VisPy,TauPz-VisPz)); }
    inline double InvMass (double px, double py, double pz, double e) { return sqrt(e*e - px*px - py*py - pz*pz); }
    inline double InvMass2(double px, double py, double pz, double e) { return (e*e - px*px - py*py - pz*pz); }
    
    inline double TauP(double x, double VisE)                         { return sqrt(pow(VisE/x,2) - Mosaic::Constant::MTAU2); }
    inline double MisE(double x, double VisE)                         { return VisE * (1.0/x - 1.0); }
    inline double MisP_impl(double e, double m)                       { return (e > m) ? sqrt(e*e - m*m) : m; }
    inline double MisP(double VisE, double x, double mMis)            { return MisP_impl(MisE(x, VisE), mMis); }
    inline double dThetaVisMis_SafeGurd(double v)                                                         { return (v==0.0) ? 0.00001 : (v!=v) ? Mosaic::Constant::PI_2 : v; }
    inline double dThetaVisMis_impl_impl2(double A, double B)                                             { return (A - B >  0.0 ) ? sqrt((A - B) * 20000000000.)/100000. : sqrt(A * 20000000000.)/100000. ; }
    inline double dThetaVisMis_impl_impl1(double A, double B, double VisP, double VisM)                   { return (A - B >= 1.0) ? dThetaVisMis_impl_impl2(0.5*pow(VisM/VisP,2) , B) : acos(A - B);}
    inline double dThetaVisMis_impl_Lep2(double VisP, double VisE, double X, double VisM, double MisM, double A) { return dThetaVisMis_impl_impl1( VisE*VisE*(1.0-X)/(VisP*A), (0.5*X*(Mosaic::Constant::MTAU2-VisM*VisM - MisM*MisM)/(VisP*A)),VisP,VisM); }
    inline double dThetaVisMis_impl_Lep(double VisP, double VisE, double X, double VisM, double MisM)     { return dThetaVisMis_impl_Lep2(VisP, VisE, X, VisM, MisM, sqrt(VisE*VisE*(1.0 - X)*(1.0 - X) - X*X*MisM*MisM)); }
    
    inline double dThetaVisMis_impl_Had(double VisP, double VisE, double X, double VisM)                  { return dThetaVisMis_impl_impl1(VisE/VisP, 0.5*X*(Mosaic::Constant::MTAU2 - VisM*VisM)/ ( VisP*VisE*(1.0 - X) ) , VisP, VisM); }
    inline double dThetaVisMis_impl(double VisP, double VisE, double VisM, double X, double MisM)         { return (MisM==0) ? dThetaVisMis_impl_Had(VisP, VisE, X, VisM) : dThetaVisMis_impl_Lep(VisP, VisE, X, VisM, MisM); }
    inline double dThetaVisMis(double VisP, double VisE, double VisM, double X, double MisM)              { return dThetaVisMis_SafeGurd( dThetaVisMis_impl(VisP, VisE, VisM, X, MisM) ); }
    inline double dThetaVisMis(double TauP, double VisP, double MisP)                                     { return acos( (TauP*TauP - VisP*VisP - MisP*MisP)/(2.0 * VisP * MisP) ); }
    
    inline double Cos_dThetaTauTau(double Px1, double Py1, double Pz1, double P1, double Px2, double Py2, double Pz2, double P2){ return (Px1*Px2 + Py1*Py2 + Pz1*Pz2)/(P1 * P2); }
    inline double Cos_dThetaTauVis_impl(double VisE2, double X, double MisM2, double VisM2){ return (VisE2 + 0.5*X*(MisM2 - Mosaic::Constant::MTAU2 - VisM2))/sqrt((VisE2 - VisM2)*(VisE2 - X*X*Mosaic::Constant::MTAU2)); }
    inline double Cos_dThetaTauVis(double VisE, double X, double MisM, double VisM){ return Cos_dThetaTauVis_impl(VisE*VisE, X, MisM*MisM, VisM*VisM); }
    
    inline double MisPx(const AngleSet& AA, const Angle& VisPhi,   const Angle& tauPhi, const Angle& dTVM, double MisP){ return ( (AA.CosP_CosT * tauPhi.Cos - VisPhi.Sin * tauPhi.Sin) * dTVM.Sin + AA.CosP_SinT * dTVM.Cos ) * MisP ; }
    inline double MisPy(const AngleSet& AA, const Angle& VisPhi,   const Angle& tauPhi, const Angle& dTVM, double MisP){ return ( (AA.SinP_CosT * tauPhi.Cos + VisPhi.Cos * tauPhi.Sin) * dTVM.Sin + AA.SinP_SinT * dTVM.Cos ) * MisP ; }
    inline double MisPz(                    const Angle& VisTheta, const Angle& tauPhi, const Angle& dTVM, double MisP){ return ( VisTheta.Cos * dTVM.Cos - VisTheta.Sin * dTVM.Sin * tauPhi.Cos ) * MisP; }
    inline FourVector MisVector(const AngleSet& AA, const Angle& VisPhi, const Angle& VisTheta, const Angle& tauPhi, const Angle& dTVM, double MisP, double MisM){ 
      return FourVector(MisPx(AA, VisPhi, tauPhi, dTVM, MisP), MisPy(AA, VisPhi, tauPhi, dTVM, MisP), MisPz(VisTheta, tauPhi, dTVM, MisP), (MisM == 0.0) ? MisP : hypot(MisP, MisM) , MisM, 0.0, 0.0 );
    }
    inline FourVector MisVector(const AngleSet& AA, const Angle& VisPhi, const Angle& VisTheta, const Angle& tauPhi,  double X, double MisM, double VisP, double VisE,  double VisM){
      return MisVector(AA, VisPhi, VisTheta, tauPhi, Angle( dThetaVisMis(VisP, VisE, VisM, X, MisM) ), MisP(VisE, X, MisM), MisM);
    }
    
    inline FourVector MisVector(const Angle& VisPhi, const Angle& VisTheta, const Angle& tauPhi, const Angle& dTVM, double MisP, double MisM){ 
      return MisVector(AngleSet(VisPhi, VisTheta), VisPhi, VisTheta, tauPhi, dTVM, MisP, MisM);
    }
    inline FourVector MisVector(double VisPhi, double VisTheta, double X, double Phi, double MisM, double VisP, double VisE, double VisM){ 
      return MisVector(Angle(VisPhi), Angle(VisTheta), Angle(Phi), Angle( dThetaVisMis(VisP, VisE, VisM, X, MisM) ), MisP(VisE, X, MisM), MisM);
    }
    
    // Leptonic PDF
    inline double Beta(double x, double m, double E)                                     { return sqrt(1.0 - pow(x*m/E,2) ); }
    inline double Beta(double x, double mTE2)                                            { return sqrt(1.0 - x*x*mTE2 ); }
    inline double Lambda(double a, double b, double c)                                   { return a*a + b*b + c*c -2.0*(a*b + b*c + c*a); }
    inline double Lambda2(double a, double b)                                            { return 1.0 + a*a + b*b -2.0*(a + b + a*b); }
    inline double LambdaSqrt(double a, double b, double c)                               { return sqrt( (a + b + c)*(a - b +c)*(a + b - c)*(a - b - c) ); }
    inline double LepCos(double x, double a2, double b2, double beta)                    { return (2.0*x - 1.0 - a2 - b2) / (beta*LambdaSqrt(1.0, a2, b2)); }
    
    inline bool CosCheck(double Cos)                                                     { return (Cos >= -1.0 && Cos <= 1.0) ? true : false; }
    inline bool BetaCheck(double beta)                                                   { return (beta >  0.0 && beta < 1.0) ? true : false; }
    inline bool LambdaCheck(double l)                                                    { return (l > 0.0) ? true : false; }
    inline bool LepPDFCheck(double p)                                                    { return (p >= 0.0) ? true : false; }
    inline bool LepCheck_impl(double Cos, double beta, double l)                         { return (BetaCheck(beta) && CosCheck(Cos) && LambdaCheck(l)) ? true : false;  }
    inline bool LepCheck(double x, double a2, double b2,double beta, double l)           { return LepCheck_impl(LepCos(x,a2,b2,beta), beta, l); }
    
    inline double PDF_Lep_impl_A(double a2, double b2, double b, double x, double mTE2, double beta) { return  (2.0 - x * mTE2 * (1.0 + a2 - b2)) * b * Mosaic::Constant::MTAU3_ / (8.0 * pow(beta, 3))  ; }
    inline double PDF_Lep_impl_B(double a2, double b2)                                               { return  a2 * (a2 + b2 - 2.0) - (2.0*b2 + 1.0)*(b2 - 1.0); }
    inline double PDF_Lep_impl_C(double a2, double b2, double x, double beta)                        { return (2.0 * x  - (1.0 + a2 - b2))*(a2 + 2.0*b2 - 1.0)/beta  ; }
    inline std::pair<double,double> PDF_Lep_impl2(double ABR, double B, double C)                                               { return std::make_pair( CheckProb(ABR * (B + C)),  CheckProb(ABR * (B - C)) ) ; }
    inline std::pair<double,double> PDF_Lep_impl1(double a2, double b2, double b, double x,double mTE2, double beta, double BR) { return PDF_Lep_impl2(PDF_Lep_impl_A(a2, b2, b, x, mTE2, beta)*BR, PDF_Lep_impl_B(a2, b2),PDF_Lep_impl_C(a2, b2, x, beta) ) ; }
    inline std::pair<double,double> PDF_Lep_impl(double a2, double b2, double b, double x, double mTE2, double beta, double BR) { return ( ! LepCheck(x, a2, b2, beta, Lambda2(a2, b2)) ) ? std::move(std::make_pair(1.0e-20, 1.0e-20)) : PDF_Lep_impl1(a2, b2, b, x, mTE2, beta, BR) ; }
    inline std::pair<double,double> PDF_Lep_impl(double a2, double b2, double b, double x, double mTE2, double BR)              { return PDF_Lep_impl(a2, b2, b, x, mTE2, Beta(x, mTE2), BR  ); }
    inline std::pair<double,double> PDF_Lep(double a2, double b, double x, double Energy, double BR)                            { return PDF_Lep_impl(a2, b*b, b, x,  Mosaic::Constant::MTAU2/Energy/Energy, BR) ; }
    
    // Pion 1p0n PDF
    inline double PDF_Pion_impl_A(double a2,  double x, double mTE2)                             { return  (1.0 - 0.5* x * mTE2 * (1.0 + a2) ); }
    inline double PDF_Pion_impl_B(double a2,  double beta)                                       { return  (1.0 - a2) * pow(beta, 3); }
    inline double PDF_Pion_impl_C(double a2,  double x, double beta)                             { return  (2.0*x - 1.0 - a2)/(1.0 - a2) / beta; }
    inline std::pair<double,double> PDF_Pion_impl3(double ABBR, double C)                                { return std::make_pair( CheckProb(ABBR * (1.0 + C)), CheckProb(ABBR * (1.0 - C) ) ) ; }
    inline std::pair<double,double> PDF_Pion_impl2(double a2,double x,double mTE2,double beta,double BR) { return (BetaCheck(beta)) ?  PDF_Pion_impl3(PDF_Pion_impl_A(a2, x, mTE2)/PDF_Pion_impl_B(a2, beta)*BR, PDF_Pion_impl_C(a2, x, beta)) : std::move(std::make_pair(1e-20, 1e-20)) ; }
    inline std::pair<double,double> PDF_Pion_impl(double a2, double x, double mTE2, double BR)           { return PDF_Pion_impl2(a2, x, mTE2, Beta(x, mTE2), BR) ; }
    inline std::pair<double,double> PDF_Pion(double a2,  double x, double Energy, double BR)             { return PDF_Pion_impl(a2, x, Mosaic::Constant::MTAU2/Energy/Energy, BR ) ;  }
    
    inline double Helisity_Angle_diff_impl(double px1, double py1, double pz1, double px23, double py23, double pz23){ return px1 * px23 + py1 * py23 + pz1 * pz23 ;; }
    inline double Helisity_Angle_diff(double px1, double py1, double pz1, double px2, double py2, double pz2, double px3, double py3, double pz3){ return Helisity_Angle_diff_impl(px1, py1, pz1, py2*pz3 - pz2*py3, pz2*px3 - px2*pz3, px2*py3 - py2*px3); }
    inline double a1_HelisityAngle_impl_Check(double v)                         { return (v < -1.0) ? -1.0 : (v > 1.0) ? 1.0 : v; }
    inline double a1_HelisityAngle_impl(double lll, double E, double VisM2, double an)        { return a1_HelisityAngle_impl_Check( 8.0*VisM2*an/E/lll ) ; }
    inline double a1_HelisityAngle_Lambda_impl(double lll){ return (lll > 0) ? lll : 0.0; }
    inline double a1_HelisityAngle_Lambda(double l1, double l2, double l3){ return -Lambda( (l1 > 0) ? l1 : 0.0,  (l2 > 0) ? l2 : 0.0,  (l3 > 0) ? l3 : 0.0); }
    inline double a1_HelisityAngle(double E, double VisM2, double px1, double py1, double pz1, double e1, double px2, double py2, double pz2, double e2, double px3, double py3, double pz3, double e3){ 
      return a1_HelisityAngle_impl(std::sqrt(a1_HelisityAngle_Lambda(
                                                                     Lambda(VisM2, InvMass2(px1+px2, py1+py2, pz1+pz2, e1+e2), Mosaic::Constant::MPI2),
                                                                     Lambda(VisM2, InvMass2(px2+px3, py2+py3, pz2+pz3, e2+e3), Mosaic::Constant::MPI2),
                                                                     Lambda(VisM2, InvMass2(px3+px1, py3+py1, pz3+pz1, e3+e1), Mosaic::Constant::MPI2)
                                                                     )
                                             ),
                                   E, VisM2, Helisity_Angle_diff(px1, py1, pz1, px2, py2, pz2, px3, py3, pz3)); 
    }
    inline double a1_HelisityAngle(double E, double px1, double py1, double pz1, double e1, double px2, double py2, double pz2, double e2, double px3, double py3, double pz3, double e3){ 
      return a1_HelisityAngle(E, InvMass2(px1+px2+px3, py1+py2+py3, pz1+pz2+pz3, e1+e2+e3), px1, py1, pz1, e1,  px2, py2, pz2, e2, px3, py3, pz3, e3);
    }
    
    
    // For Z->tautau ME Value
    inline double PDF_Spin1(std::pair<double, double>&& L1, std::pair<double, double>&& L2, double SinTheta2, double CosSumPhi12){
      return CheckProb(  ((1.0 + SinTheta2) * (  Mosaic::Constant::gTauPlus2  * L1.first * L2.second + Mosaic::Constant::gTauMinus2 * L1.second * L2.first ) + 4.0 * Mosaic::Constant::gTauPM * SinTheta2 * sqrt(L1.first*L1.second * L2.first*L2.second) * CosSumPhi12)  );
    }
    inline double PDF_Spin0_CPEven(std::pair<double, double>&& L1, std::pair<double, double>&& L2, double CosdPhi){
      return CheckProb( ( L1.first * L2.second + L1.second * L2.first - 2.0 * CosdPhi * sqrt(L1.first * L1.second * L2.first * L2.second) ));
    }
    inline double PDF_Spin0_CPOdd(std::pair<double, double>&& L1, std::pair<double, double>&& L2, double CosdPhi){
      return CheckProb( ( L1.first * L2.second + L1.second * L2.first + 2.0 * CosdPhi * sqrt(L1.first * L1.second * L2.first * L2.second) )  );
    }
    
    inline double Prob_Resol_impl(double p, double invsigma2) { return ( p < -25.0) ? -2e+10 : p + 0.5 + 0.5*log(invsigma2) + Mosaic::Constant::LOG_INV_PI2; }
    inline double Prob_Resol(double dx, double invsigma2)     { return Prob_Resol_impl( - std::pow(dx,2)*invsigma2 , invsigma2); }
    
    // Boost Back into CM system
    inline FourVector&& BoostBackVector(double px, double py, double pz, double e ) { return std::move( Mosaic::FourVector(-px/e, -py/e, -pz/e, 1.0, 0.0) ); }
    inline FourVector&& BoostBackVector(const FourVector& tau1, const FourVector& tau2 )                                                 { return std::move( BoostBackVector(tau1.px()+tau2.px(), tau1.py()+tau2.py(), tau1.pz()+tau2.pz(), tau1.e()+tau2.e() ) ); }
    inline FourVector&& BoostBackVector(const FourVector& mis1, const FourVector& mis2, const FourVector& vis1, const FourVector& vis2)  {
      return std::move( BoostBackVector(mis1.px()+mis2.px()+vis1.px()+vis2.px() , mis1.py()+mis2.py()+vis1.py()+vis2.py() , mis1.pz()+mis2.pz()+vis1.pz()+vis2.pz() , mis1.e()+mis2.e()+vis1.e()+vis2.e() ) ); 
    }
    
    inline void BoostBack2_impl2(FourVector&& b, FourVector& tau1, FourVector& tau2, FourVector& vis1, FourVector& vis2, const double& g, const double& g2){ tau1.Boost(b, g, g2); tau2.Boost(b, g, g2); vis1.Boost(b, g, g2); vis2.Boost(b, g, g2); }
    inline void BoostBack2_impl2(FourVector&& b, FourVector& tau1, FourVector& tau2, FourVector& vis1, FourVector& vis2, const double& g)                  { BoostBack2_impl2(std::move(b), tau1, tau2, vis1, vis2, g, (b.p2() > 0.0) ? (g - 1.0)/b.p2() : 0.0); }
    inline void BoostBack2_impl (FourVector&& b, FourVector& tau1, FourVector& tau2, FourVector& vis1, FourVector& vis2)                                   { BoostBack2_impl2(std::move(b), tau1, tau2, vis1, vis2, pow(1.0 - b.p2(), -0.5)); }
    inline void BoostBack2      (                FourVector& tau1, FourVector& tau2, FourVector& vis1, FourVector& vis2)                                   { BoostBack2_impl ( std::move(BoostBackVector(tau1, tau2)), tau1, tau2, vis1, vis2); }
    
    inline void BoostBack1_impl2(FourVector&& b, FourVector& vis1, FourVector& vis2, const double& g, const double& g2)                                    { vis1.Boost(b, g, g2); vis2.Boost(b, g, g2); }
    inline void BoostBack1_impl2(FourVector&& b, FourVector& vis1, FourVector& vis2, const double& g)                                                      { BoostBack1_impl2(std::move(b), vis1, vis2, g, (b.p2() > 0.0) ? (g - 1.0)/b.p2() : 0.0); }
    inline void BoostBack1_impl (FourVector&& b, FourVector& vis1, FourVector& vis2)                                                                       { BoostBack1_impl2(std::move(b), vis1, vis2, pow(1.0 - b.p2(), -0.5)); }
    inline void BoostBack1      (                FourVector& mis1, FourVector& mis2 , FourVector& vis1, FourVector& vis2)                                  { BoostBack1_impl ( std::move(BoostBackVector(mis1, mis2, vis1, vis2)), vis1, vis2 ); }
    
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
    inline double Median(std::vector<double> Vs){
      std::sort(std::begin(Vs), std::end(Vs), [](const double& a, const double& b) { return a > b; } );
      int size = Vs.size();
      if(size == 0) return -1;
      return (size % 2 == 0) ? (Vs[size / 2 - 1] + Vs[size / 2]) / 2.0 : Vs[size / 2];
    }
    
    // MCMC Outpu Diagonics, this is not exactly correct, but faster and approximately correct
    inline double gamma(const std::vector< SamplingPoint >& Chain, int Lag, int nBurnIn, int sizeOfVector){
      int sizeLag  = (sizeOfVector - nBurnIn - Lag);
      double mean  = std::accumulate( std::begin(Chain)+Lag+nBurnIn, std::end(Chain), 0.0, [](double lhs, const SamplingPoint& rhs) { return lhs + rhs.mass(); } )/sizeLag;
      return std::inner_product( 
                                std::begin(Chain)+nBurnIn + Lag,  
                                std::end(Chain), 
                                std::begin(Chain)+nBurnIn, 
                                0.0, 
                                [](double lhs, double rhs){ return lhs * rhs ; }, 
                                [](const SamplingPoint& v1, const SamplingPoint& v2 ){ return v1.mass() * v2.mass(); }
                                 )/sizeLag - mean*mean;
    }
    inline double ESS(const std::vector< SamplingPoint >& Chain, int nBurnIn, int sizeOfVector){
      int thisMaxLag = sizeOfVector - nBurnIn <= Mosaic::Constant::MaxLag ? sizeOfVector - nBurnIn : Mosaic::Constant::MaxLag;
      std::vector<double> gammaStat(thisMaxLag, 0.0);
      gammaStat.at(0) = gamma(Chain, 0, nBurnIn, sizeOfVector);
      gammaStat.at(1) = gamma(Chain, 1, nBurnIn, sizeOfVector);
      double varStat = gammaStat.at(0);
      for(int Lag = 2; Lag < thisMaxLag; Lag+=2){
        gammaStat[Lag  ] = gamma(Chain, Lag,   nBurnIn, sizeOfVector);
        gammaStat[Lag+1] = gamma(Chain, Lag+1, nBurnIn, sizeOfVector);
        
        double s = gammaStat.at(Lag-1) + gammaStat.at(Lag) ;
        if( s > 0.0 && gammaStat.at(Lag) > 0.02 ) varStat += 2.0 * s;
        else break;
      }
      return sizeOfVector*gammaStat.at(0)/varStat;
    }
    
    inline int GetACT(std::vector< SamplingPoint >& Chain){
      auto gamma0 = gamma(Chain, 0, 0, Chain.size());
      double CL95 = 2.0/sqrt( Chain.size() );
      for(int Lag = 1; Lag < Mosaic::Constant::MaxACT; ++Lag){
        if( std::fabs( gamma(Chain, Lag, 0, Chain.size())/gamma0 ) < CL95) return Lag;
      }
      return -1;
    }
    template<typename Lmd>
      inline double OptimalBin(const std::vector< SamplingPoint > &Vs, double Min, double Max, Lmd lambda){
      int MinBin  = 10;
      double MinC = 777777.7777;
      int nMaxBin = fabs(Max - Min)/ 100;// -->  0.1 GeV
      int nMinBin = fabs(Max - Min)/5000;// -->  5.0 GeV 
      if( Min >= 0.0 && Max >= 0.0 && Min < 1.0 && Max < 1.00001){
        nMaxBin = (Max - Min)/ 0.01;// -->  X12  = 0.01
        nMinBin = (Max - Min)/ 0.10;  // -->  X12 = 0.1
      }
      
      if( nMaxBin <   100) nMaxBin =   100;
      if( nMinBin <    10) nMinBin =    10;
      if( nMaxBin >   250) nMaxBin =   250;
      if( nMinBin >    50) nMinBin =    50;
      //std::cout <<" Min / Max is "<< Min << " / "<< Max <<" ---> " << nMinBin << ", " << nMaxBin << std::endl;
      
      for(int nBin = nMinBin; nBin < nMaxBin; ++nBin) {
        std::vector<int> hist(nBin, 0.0);
        auto width = fabs(Max - Min)/nBin; 
        std::for_each(std::begin(Vs), std::end(Vs), [&] (const SamplingPoint & V) { ++hist.at( int ( (lambda(V) - Min)/width )) ; } );
        double mean(0.0), var(0.0);
        std::for_each(std::begin(hist), std::end(hist), [&]( const int& val ) { mean += val; var += val*val; } );
        mean /= nBin;
        var   = sqrt(var / nBin - mean * mean)/nBin;
        
        auto c     = (2.0*mean - var)/ width/width ;
        if (c <= MinC){ 
          MinC = c; 
          MinBin = nBin; 
        }
      }
      return MinBin;
    }
    template<typename Lmd>
    inline std::pair<double, double> Mode1(const std::vector< SamplingPoint >& Vs, Lmd lambda){
      if(Vs.size() == 0) return std::make_pair(0.0, 0.0);
      auto MinItr   = std::min_element(std::begin(Vs), std::end(Vs), [&](const SamplingPoint& lhs, const SamplingPoint& rhs ){ return lambda(lhs) < lambda(rhs); }  ) ;
      auto MaxItr   = std::max_element(std::begin(Vs), std::end(Vs), [&](const SamplingPoint& lhs, const SamplingPoint& rhs ){ return lambda(lhs) < lambda(rhs); }  ) ;
      auto Max      = lambda(*MaxItr) * 1.00001;
      auto Min      = lambda(*MinItr) * 0.99999;
      //std::cout <<" Min / Max is "<< Min <<" / " << Max << std::endl;
      if( lambda(*MaxItr) <= 0.0 && lambda(*MinItr) <= 0.0 ){
        Max = lambda(*MaxItr) * 0.99999;
        Min = lambda(*MinItr) * 1.00001;
      }
      if( fabs(Min) < 1.0e-04 ) Min = -0.1;
      if( fabs(Max) < 1.0e-04 ) Max = +0.1;
      
      //std::cout <<" ---> Min / Max is "<< Min <<" / " << Max << std::endl;
      
      auto nOptBin  = OptimalBin(Vs, Min, Max, lambda);
      auto Width    = (Max - Min)/nOptBin;
      std::vector<double> Hist(nOptBin, 0);
      std::vector<double> Hist_weight(nOptBin, 0);
      std::for_each(std::begin(Vs), std::end(Vs), [&] (const SamplingPoint& V) { Hist.at( int ( (lambda(V)- Min)/Width )) += 1.0; Hist_weight.at( int ( (lambda(V)- Min)/Width )) += V.prob(); } );
      return  std::make_pair( (std::distance(std::begin(Hist), std::max_element(std::begin(Hist), std::end(Hist)  ) ) + 0.5) * Width + Min, 
                              (std::distance(std::begin(Hist_weight), std::max_element(std::begin(Hist_weight), std::end(Hist_weight)  ) ) + 0.5) * Width + Min  );
      
    }
  
    template<typename Lmd>
    inline std::pair< std::pair<double, double>, std::pair<double, double> > Mean1(const std::vector< SamplingPoint >& Vs, Lmd lambda, double sumOf_Weight){
      double nominal  = 0.0;
      double weighted = 0.0;
      double nominal2  = 0.0;
      double weighted2 = 0.0;
      double x = 0.0;
      std::for_each(std::begin(Vs), std::end(Vs), [&]( const SamplingPoint& V) { 
          x = lambda(V); 
          nominal   += x;
          weighted  += x*V.prob(); 
          nominal2  += x*x;
          weighted2 += x*x*V.prob(); 
        } );
      double mean   = nominal/Vs.size();
      double meanw  = weighted/sumOf_Weight;
      double sigma  = sqrt( (nominal2  - 2.0 * nominal  * mean  + mean  * mean  * Vs.size())/Vs.size() );
      double sigmaw = sqrt( (weighted2 - 2.0 * weighted * meanw + meanw * meanw * sumOf_Weight)/sumOf_Weight );
      
      return std::make_pair( std::make_pair( mean, meanw ), std::make_pair( sigma, sigmaw )  ) ;
    }
    template<typename Lmd>
    inline std::pair<double, double> Medi1(std::vector< SamplingPoint >& Vs, Lmd lambda, double sumOf_Weight){
      int size = Vs.size();
      if(size == 0) return std::make_pair(0.0, 0.0);
      std::sort(std::begin(Vs), std::end(Vs), [&](const SamplingPoint& a, const SamplingPoint& b) { return lambda(a) > lambda(b); } );
      double this_sumOf_weight = 0.0;
      double half_sumOf_weight = 0.5*sumOf_Weight;
      double this_target_value = 0.0;
      for(auto& p : Vs){
        this_sumOf_weight += p.prob();
        if( half_sumOf_weight <= this_sumOf_weight) {
          this_target_value = lambda(p);
          break;
        }
      }
      
      return std::make_pair( (size % 2 == 0) ? ( lambda(Vs[size / 2 - 1]) + lambda(Vs[size / 2]) ) / 2.0 : lambda(Vs[size / 2]),   this_target_value);
    }
    //--------------------------------------------------------------------------------------------------------------//
    // Geweke Method, but Not exactly same.
    inline std::tuple<double, double, int> doGewekeCheck(const std::vector< SamplingPoint >& Chain){
      int s (Chain.size()), nB(int(s/2));
      std::vector<int> BurnIns = { int(s*0.10),int(s*0.20),int(s*0.30),int(s*0.40) };
      
      double meanB(0.0), varB(0.0);
      std::for_each(std::begin(Chain)+nB, std::end(Chain), [&]( const SamplingPoint& val ) { meanB += val.mass(); varB += pow(val.mass(), 2); } );
      meanB /= nB;
      varB   = sqrt(varB / nB - meanB * meanB);
      
      int nOptBurnIn = -1;
      for(auto& nBurnIn : BurnIns){ 
        double meanA(0.0), varA(0.0);
        std::for_each(std::begin(Chain), std::begin(Chain)+nBurnIn, [&]( const SamplingPoint& val ) { meanA += val.mass(); varA += pow(val.mass(), 2); } );
        meanA /= s - nBurnIn;
        varA   = sqrt(varA / nBurnIn - meanA * meanA);
        //std::cout <<" nBurnIn / meanA / varA is "<< nBurnIn <<" / "<< meanA << " / "<< varA << " ---> " << fabs((meanA - meanB)/sqrt(varA*varA + varB*varB)) << std::endl;
        if( fabs((meanA - meanB)/sqrt( varA*varA + varB*varB)) < 2.0 ){ // This assumes the AR(0) which means white noise.
          nOptBurnIn = nBurnIn;
          break;
        }
      }
      if(nOptBurnIn > 0){
        double mean(0.0), var(0.0);
        int nSize = s - nOptBurnIn;
        std::for_each(std::begin(Chain)+nOptBurnIn, std::end(Chain), [&]( const SamplingPoint& val ) { mean += val.mass(); var += pow(val.mass(), 2); } );
        mean /= nSize;
        var   = (var  - mean * mean * nSize); // This is OK, This is for Gelman & Rubin Chaeck
        return std::make_tuple(mean, var, nSize);
      }
      return std::make_tuple(-777.0, -777.0, -777.0);
    }
    template<int N_CHAIN> 
    inline double doGelmanRubin(std::array< std::tuple<double, double, int>, N_CHAIN > mean_vars){
      if ( std::any_of(std::begin(mean_vars), std::end(mean_vars), [](std::tuple<double, double, int> tpl) { return std::get<2>(tpl) == 0; }  ) ) return false;
      
      auto min_tpl = *(std::min_element(std::begin(mean_vars), std::end(mean_vars), [&](const std::tuple<double, double, int>& lhs, const std::tuple<double, double, int>& rhs ){ return std::get<2>(lhs) < std::get<2>(rhs); } ) );
      int nStep    = std::get<2>(min_tpl);
      
      double M = std::accumulate(std::begin(mean_vars), std::end(mean_vars), 0.0, [ ](double lhs, const std::tuple<double, double, int>& rhs) { return lhs + std::get<0>(rhs); } )/ N_CHAIN;
      double W = std::accumulate(std::begin(mean_vars), std::end(mean_vars), 0.0, [ ](double lhs, const std::tuple<double, double, int>& rhs) { return lhs + std::get<1>(rhs); } )/(N_CHAIN * (nStep - 1));
      double B = std::accumulate(std::begin(mean_vars), std::end(mean_vars), 0.0, [&](double lhs, const std::tuple<double, double, int>& rhs) { return lhs + pow(M - std::get<0>(rhs), 2);  } )*nStep/(N_CHAIN-1.0);
      double R = sqrt( (1.0 - 1.0/nStep) + B/nStep/W);
      //std::cout <<" Check Gelman Rubin R is  "  << R << " B / nStep / W is " << B <<" / " << nStep << " / " << W  << std::endl;
      return fabs(R-1.0) ;
    }
    /*
    inline bool doGelmanRubin( double W, double M, double B, int nStep){ return fabs( sqrt( (1.0 - 1.0/nStep) + B/nStep/W) -1.0) < threshold; }
    */
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++/++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++/++++++++++++++++++++++++++++++++++++//
    inline void set_v_seed(std::mt19937& Engine, std::uniform_real_distribution<double>& Uniform, std::array<std::uint_least32_t, Mosaic::Constant::SeedLength>& v_seed, uint_<Mosaic::Constant::SeedLength> ){
      MOSAIC_UNUSED_VARIABLE(Engine);
      MOSAIC_UNUSED_VARIABLE(Uniform);
      MOSAIC_UNUSED_VARIABLE(v_seed);
    }
    template<size_t I> inline void set_v_seed(std::mt19937& Engine, std::uniform_real_distribution<double>& Uniform, std::array<std::uint_least32_t, Mosaic::Constant::SeedLength>& v_seed, uint_<I>  ){
      std::get<I>( v_seed ) = ( (std::uint_least32_t) fabs( Uniform(Engine)*2015 ) );
      set_v_seed(Engine, Uniform, v_seed, uint_<I+1>() );
    }
    
    inline std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> get_seed_seq(std::mt19937& Engine, std::uniform_real_distribution<double>& Uniform ){
      std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> v_seed;
      set_v_seed(Engine, Uniform, v_seed, uint_<0>() );
      return v_seed;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
    inline bool isPanTauBetter(Mosaic::ParticleType mode, double pt){
      return 
        (   mode == Mosaic::ParticleType::mode_1p0n)  ? pt < Mosaic::Constant::PtVal_1p0n 
        : ( mode == Mosaic::ParticleType::mode_1p1n)  ? pt < Mosaic::Constant::PtVal_1p1n 
        : ( mode == Mosaic::ParticleType::mode_1pXn)  ? pt < Mosaic::Constant::PtVal_1pXn 
        : ( mode == Mosaic::ParticleType::mode_3p0n)  ? pt < Mosaic::Constant::PtVal_3p0n 
        : ( mode == Mosaic::ParticleType::mode_3pXn)  ? pt < Mosaic::Constant::PtVal_3pXn : true;
    }
  }
}

#endif // MOSAIC_ALGORITHMMANAGER_H
