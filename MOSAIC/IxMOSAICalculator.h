// Dear emacs, this is -*- c++ -*- 
/*
 * IxMOSAICalculator
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_IXMOSAICALCULATOR_H
#define MOSAIC_IXMOSAICALCULATOR_H

#include "PATInterfaces/CorrectionCode.h"
#include "AsgTools/IAsgTool.h"

#include "xAODBase/IParticle.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingET.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODTau/TauJet.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"

// fwd declarations
class MOSAICalculator;

class IxMOSAICalculator :  virtual public asg::IAsgTool {
  // Declare the interface that the class provides
  ASG_TOOL_INTERFACE(IxMOSAICalculator) 
  
 public:
  // 
  virtual StatusCode Clear() = 0;
  virtual StatusCode SetObj(const xAOD::JetContainer             * xJets  ) = 0;
  virtual StatusCode SetTruthMET(const xAOD::MissingET           * xMET   ) = 0;
  virtual StatusCode SetTruthMET(const xAOD::MissingETContainer  * xMET   ) = 0;
  virtual StatusCode SetObj(const xAOD::MissingET                * xMET   ) = 0;
  virtual StatusCode SetObj(const xAOD::MissingETContainer       * xMET   ) = 0;
  virtual StatusCode SetObj(int iTau, const xAOD::TauJet         * xTau   ) = 0;
  virtual StatusCode SetObj(int iTau, const xAOD::Electron       * xElec  ) = 0;
  virtual StatusCode SetObj(int iTau, const xAOD::Muon           * xMuon  ) = 0;
  virtual StatusCode SetObj(int iTau, const xAOD::TruthParticle  * xTruth ) = 0;
  
  //template<typename First, typename... Rest> void Set(const First * first, const Rest*... rest ) = 0;
  //template<typename T1, typename T2> 
  //virtual StatusCode SetObjects(const T1 * xObj1, const T2 * xObj2, const xAOD::MissingET *xMET, const xAOD::JetContainer* xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::IParticle        * xObj1, const xAOD::IParticle       * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::TruthParticle    * xObj1, const xAOD::TruthParticle   * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets) = 0;
                                                                                                                                                                             
  virtual StatusCode SetObjects(const xAOD::IParticle        * xObj1, const xAOD::IParticle       * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  virtual StatusCode SetObjects(const xAOD::TruthParticle    * xObj1, const xAOD::TruthParticle   * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets) = 0;
  
  virtual StatusCode run_Z() = 0;
  virtual StatusCode run_H() = 0;
  virtual StatusCode run_A() = 0;
  virtual double mass_Z( ) const = 0;
  virtual double mass_H( ) const = 0;
  virtual double mass_A( ) const = 0;
  /*
  virtual TLorentzVector missing1_Z() const = 0;
  virtual TLorentzVector missing2_Z() const = 0;
  virtual TLorentzVector missing1_H() const = 0;
  virtual TLorentzVector missing2_H() const = 0;
  virtual TLorentzVector missing1_A() const = 0;
  virtual TLorentzVector missing2_A() const = 0;
  */
};

#endif // MOSAIC_IXMOSAICALCULATOR_H
