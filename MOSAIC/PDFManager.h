// Dear emacs, this is -*- c++ -*- 
/*
 * PDFManager 
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_PDFMAGAGER_H
#define MOSAIC_PDFMAGAGER_H


#include <vector>

#include "MOSAIC/AlgorithmManager.h"
#include "MOSAIC/ResolModel.h"
#include "MOSAIC/IOManager.h"
#include "MOSAIC/Constant.h"
#include "MOSAIC/Objects.h"

#ifdef MOSAIC_USE_DEVELOP
#include "LHAPDF/LHAPDF.h"
#endif

namespace Mosaic {
  
  class PDF_Spin1 {
  public:
    PDF_Spin1();
    ~PDF_Spin1();
    double Prob_Reso_impl(std::pair<double, double>&& L1, std::pair<double, double>&& L2, FourVector&& Tau1, FourVector&& Tau2, FourVector&& Vis1, FourVector&& Vis2) const ;
    double Prob_Reso     (std::pair<double, double>&& L1, std::pair<double, double>&& L2, FourVector&& Mis1, FourVector&& Mis2, FourVector&& Vis1, FourVector&& Vis2) const ;
  };
  
  class PDF_Spin0_CPEven {
  public:
    PDF_Spin0_CPEven();
    ~PDF_Spin0_CPEven();
    double Prob_Reso(std::pair<double, double>&& L1, std::pair<double, double>&& L2, FourVector&& Mis1, FourVector&& Mis2, FourVector&& Vis1, FourVector&& Vis2) const ;
  };
  class PDF_Spin0_CPOdd {
  public:
    PDF_Spin0_CPOdd();
    ~PDF_Spin0_CPOdd();
    double Prob_Reso(std::pair<double, double>&& L1, std::pair<double, double>&& L2, FourVector&& Mis1, FourVector&& Mis2, FourVector&& Vis1, FourVector&& Vis2) const ;
  };
  
  class PDF_Lep {
  public:
    PDF_Lep(double _a2, double _BR);
    ~PDF_Lep();
    std::pair<double, double> Prob(double X, double Energy, double MisM) ;
    void SetLocalBin(const double Y) ;
    
  private:
    const double a2;
    const double BR;
    double localBin;
  };
  
  class PDF_Pion {
  public:
    PDF_Pion(double _a2, double _BR);
    ~PDF_Pion();
    std::pair<double, double> Prob(double X, double Energy, double MisM)  ;
    void SetLocalBin(const double Y) ;
  private:
    const double a2;
    const double BR;
    double localBin;
  };
  
  
  class PDF_Spline {
  public:
    PDF_Spline( double _lowX, double _dX,double _lowY, double _dY, double _BR, std::initializer_list< SplineSet > _MEP_SplineSet, std::initializer_list< SplineSet > _MEM_SplineSet);
    ~PDF_Spline();
    double BranchingRatio()    const ;
    std::pair<double, double> Prob(double X, double Energy, double MisM) ;
    int    BinX(const double X)      const ;
    int    BinY_impl(const int s)    const ;
    int    BinY(const double Y)      const ;
    void SetLocalBin(const double Y) ;
    
  private:
    const double lowX;
    const double dX;
    const double lowY;
    const double dY;
    const double BR;
    const std::vector< SplineSet > MEP_SplineSet;
    const std::vector< SplineSet > MEM_SplineSet;
    const int SetSize;
    int localBin;
  };
#ifdef MOSAIC_USE_DEVELOP  
  class PDF_LHAPDF {
  public:
    PDF_LHAPDF( std::string _pdfSetName );
    ~PDF_LHAPDF();
    
    double get_gg_logWeight(double x1, double x2, double q2);
    //double get_qg_logWeight(double x1, double x2, double q2);
    //double get_qq_logWeight(double x1, double x2, double q2);
    
  private:
    std::string pdfSetName;
    LHAPDF::PDF *pdf_LHAPDF;
  };
#endif  
  template <typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2> class PDFManager {
  public:
#ifdef MOSAIC_USE_DEVELOP
    PDFManager( InputManager * inputManager, PDF1* pdf1, PDF2* pdf2, PDFDecayME* pDFDecayME, ResolModel_ETMiss* pdfmet, ResolModel_Vis1 * vis1Model, ResolModel_Vis2 * vis2Model, PDF_LHAPDF * pdf_LHAPDF );
#else
    PDFManager( InputManager * inputManager, PDF1* pdf1, PDF2* pdf2, PDFDecayME* pDFDecayME, ResolModel_ETMiss* pdfmet, ResolModel_Vis1 * vis1Model, ResolModel_Vis2 * vis2Model);
#endif
    ~PDFManager();

    template<typename MCMCTau1, typename MCMCTau2> SamplingPoint Prob(MCMCTau1 * Tau1, MCMCTau2 * Tau2, Mosaic::MCMCRecoilSystem * recoilSystem, Mosaic::LikelihoodType::Default );
#ifdef MOSAIC_USE_DEVELOP
    template<typename MCMCTau1, typename MCMCTau2> SamplingPoint Prob(MCMCTau1 * Tau1, MCMCTau2 * Tau2, Mosaic::MCMCRecoilSystem * recoilSystem, Mosaic::LikelihoodType::MEOnly );
    template<typename MCMCTau1, typename MCMCTau2> SamplingPoint Prob(MCMCTau1 * Tau1, MCMCTau2 * Tau2, Mosaic::MCMCRecoilSystem * recoilSystem, Mosaic::LikelihoodType::METOnly );
    template<typename MCMCTau1, typename MCMCTau2> SamplingPoint Prob(MCMCTau1 * Tau1, MCMCTau2 * Tau2, Mosaic::MCMCRecoilSystem * recoilSystem, Mosaic::LikelihoodType::PlusVisResoltion );
    template<typename MCMCTau1, typename MCMCTau2> SamplingPoint Prob(MCMCTau1 * Tau1, MCMCTau2 * Tau2, Mosaic::MCMCRecoilSystem * recoilSystem, Mosaic::LikelihoodType::RecoilSystem );
    template<typename MCMCTau1, typename MCMCTau2> SamplingPoint Prob(MCMCTau1 * Tau1, MCMCTau2 * Tau2, Mosaic::MCMCRecoilSystem * recoilSystem, Mosaic::LikelihoodType::PartDF_LHAPDF );
#endif    
    SamplingPoint Prob_impl(double X1, double X2, double M1, double M2, double P1, double P2);
    SamplingPoint Prob_impl(double X1, double X2, double M1, double M2, double P1, double P2, double R1, double R2);
    SamplingPoint Prob_impl(double X1, double X2, double M1, double M2, double P1, double P2, double recoilDeltaPx, double recoilDeltaPy, double recoilProb);
    SamplingPoint Prob_impl_MEOnly(double X1, double X2, double M1, double M2, double P1, double P2);
    SamplingPoint Prob_impl_METOnly(double X1, double X2, double M1, double M2, double P1, double P2);
    SamplingPoint Prob_impl_PDF(double X1, double X2, double M1, double M2, double P1, double P2);
    
    
    const FourVector pVis1, pVis2;
    const double PxVis1,  PyVis1,  PzVis1,  EVis1,  PVis1,  MassVis1, PtVis1;
    const double PxVis2,  PyVis2,  PzVis2,  EVis2,  PVis2,  MassVis2, PtVis2;
    const double PxVis12, PyVis12, PzVis12, EVis12, PVis12, MassVis12;
    const double CM_1, CM_2;
    const Angle visPhi1, visPhi2, visTheta1, visTheta2;
    const AngleSet AA1, AA2; 
    
    
    PDF1              *pDF1;
    PDF2              *pDF2;
    PDFDecayME        *pdfDecayME;
#ifdef MOSAIC_USE_DEVELOP
    PDF_LHAPDF        *pdfLHAPDF;
#endif
    ResolModel_Vis1   *Vis1Model;
    ResolModel_Vis2   *Vis2Model;
    ResolModel_ETMiss *ETMissModel;
    
  };
    
}// namespace Mosaic

#include "MOSAIC/PDFManager.icc"
  
#endif // MOSAIC_PDFMAGAGER_H
  
