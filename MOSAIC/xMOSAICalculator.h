// Dear emacs, this is -*- c++ -*- 
/*
 * xMOSAICalculator
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_XMOSAICALCULATOR_H
#define MOSAIC_XMOSAICALCULATOR_H

#include "MOSAIC/IxMOSAICalculator.h"
#include "MOSAIC/MOSAICalculator.h"

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"

//#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
//#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"

class xMOSAICalculator : virtual public IxMOSAICalculator, public asg::AsgTool {
  /// Proper constructor for Athena 
  ASG_TOOL_CLASS(xMOSAICalculator, IxMOSAICalculator)
  
  public:
  
  
  // Default constructor
  xMOSAICalculator(const std::string& name );
  // Copy constructor for reflex in Athena
  xMOSAICalculator(const xMOSAICalculator& other);
  /// virtual destructor  
  virtual ~xMOSAICalculator() { ; }
  
  /// Initialize the tool 
  virtual StatusCode initialize();
  virtual StatusCode Clear();
  virtual StatusCode SetObj(const xAOD::JetContainer             * xJets  );
  virtual StatusCode SetTruthMET(const xAOD::MissingET           * xMET   );
  virtual StatusCode SetTruthMET(const xAOD::MissingETContainer  * xMET   );
  virtual StatusCode SetObj(const xAOD::MissingET                * xMET   );
  virtual StatusCode SetObj(const xAOD::MissingETContainer       * xMET   );
  virtual StatusCode SetObj(int iTau, const xAOD::TauJet         * xTau   );
  virtual StatusCode SetObj(int iTau, const xAOD::Electron       * xElec  );
  virtual StatusCode SetObj(int iTau, const xAOD::Muon           * xMuon  );
  virtual StatusCode SetObj(int iTau, const xAOD::TruthParticle  * xTruth );
  
  //template<typename First, typename... Rest> void Set(const First * first, const Rest*... rest );
  //template<typename T1, typename T2> 
  //virtual StatusCode SetObjects(const T1 * xObj1, const T2 * xObj2, const xAOD::MissingET *xMET, const xAOD::JetContainer* xJets);
  virtual StatusCode SetObjects(const xAOD::IParticle        * xObj1, const xAOD::IParticle       * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::TruthParticle    * xObj1, const xAOD::TruthParticle   * xObj2, const xAOD::MissingETContainer  * xMET, const xAOD::JetContainer  * xJets);
                                                                                                                                                                             
  virtual StatusCode SetObjects(const xAOD::IParticle        * xObj1, const xAOD::IParticle       * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Electron         * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::Muon             * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::Electron        * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::Muon            * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::TauJet           * xObj1, const xAOD::TauJet          * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  virtual StatusCode SetObjects(const xAOD::TruthParticle    * xObj1, const xAOD::TruthParticle   * xObj2, const xAOD::MissingET           * xMET, const xAOD::JetContainer  * xJets);
  
  virtual StatusCode run_Z() { return m_mosaic->Run_Mosaic_Z() ? StatusCode::SUCCESS : StatusCode::FAILURE;  }
  virtual StatusCode run_H() { return m_mosaic->Run_Mosaic_H() ? StatusCode::SUCCESS : StatusCode::FAILURE;  }
  virtual StatusCode run_A() { return m_mosaic->Run_Mosaic_A() ? StatusCode::SUCCESS : StatusCode::FAILURE;  }
  virtual double mass_Z() const { return m_mosaic->Mass_Z(); }
  virtual double mass_H() const { return m_mosaic->Mass_H(); }
  virtual double mass_A() const { return m_mosaic->Mass_A(); }
  /*
  virtual TLorentzVector missing1_Z() const { ; auto mis = m_mosaic->output; return TLorentzVector(); }
  virtual TLorentzVector missing2_Z() const 
  virtual TLorentzVector missing1_H() const 
  virtual TLorentzVector missing2_H() const 
  virtual TLorentzVector missing1_A() const 
  virtual TLorentzVector missing2_A() const 
  */
  
  MOSAICalculator * m_mosaic;
 private:
  bool isOK_retrieve_muon;
  bool isOK_retrieve_elec;
  bool isOK_retrieve_tau;
  //ToolHandle< CP::MuonCalibrationAndSmearingTool   > m_muonResolutionTool;
  //ToolHandle< CP::EgammaCalibrationAndSmearingTool > m_elecResolutionTool;
  
  std::string       m_calibType;
  std::string       m_METType;
  std::string       m_LLHType;
  std::string       m_LHAPDF_Type;
  std::string       m_MET_Final_Name;
  std::string       m_MET_SoftTerm_Name;
  std::string       m_RunMode;
  std::string       m_JVTKey;
  
  double            m_eta1;
  double            m_eta2;
  double            m_phi1;
  double            m_phi2;
  
  bool              m_isUseBetter_MET;
  bool              m_isDoAllVar;
  bool              m_isForceNJetInclusive;
  bool              m_isTruthStudy;
  int               m_RandomSeed;
  double            m_DeltaMETThresold;
  double            m_METProbScale;
  //double            m_ResolutionStudyValue;
  
};

//#include "MOSAIC/xMOSAICalculator.icc"

#endif // MOSAIC_XMOSAICALCULATOR_H
