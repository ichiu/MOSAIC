// Dear emacs, this is -*- c++ -*- 
/*
 * @file: ResolModel.h
 * @author: Masahiro Morinaga
 */

#ifndef MOSAIC_RESOLMODEL_H
#define MOSAIC_RESOLMODEL_H

#include "MOSAIC/AlgorithmManager.h"
#include "MOSAIC/Constant.h"

namespace Mosaic {
  
  class ResolModel_ETMiss {
  public:
    ResolModel_ETMiss(double obsEx, double obsEy, double obsSumEt, double resolScale, double deltaThresold, double probScale, Mosaic::MET_nJet njet, Mosaic::MET_Type metType, Mosaic::CalibType calibtype);
    ~ResolModel_ETMiss() ;
    
    inline double Prob_EtMiss_impl(double prob)                 { 
      //std::cout <<"   prob is " << prob << std::endl; 
      return ( prob < DeltaThresold2 ) ? -1e+10 : prob * ProbScale; }
    inline double Prob_EtMiss_impl(double probX, double probY)  { 
      //std::cout <<"  probX / ptobY is "<< probX << " / " << probY << std::endl; 
      return Prob_EtMiss_impl( AlgorithmManager::Prob_impl(probX) + AlgorithmManager::Prob_impl(probY) - Mosaic::Constant::LOG_PI2 ) ; }
    inline double Prob_EtMiss(double dEx2, double dEy2) { 
      //std::cout << " dEx/y is " << sqrt(dEx2) <<" / " << sqrt(dEy2) << std::endl;
      return Prob_EtMiss_impl( Coeff_Sigma1 * exp( Minus_Inv_Sigma1_2_2 * dEx2 ) + Coeff_Sigma2 * exp( Minus_Inv_Sigma2_2_2 * dEx2 ), 
                               Coeff_Sigma1 * exp( Minus_Inv_Sigma1_2_2 * dEy2 ) + Coeff_Sigma2 * exp( Minus_Inv_Sigma2_2_2 * dEy2 ) ); 
    }
    inline double Prob(double Ex, double Ey, double recoilDeltaPx, double recoilDeltaPy) { return Prob_EtMiss( pow(ObsEx - Ex/1000. - recoilDeltaPx/1000., 2), pow(ObsEy - Ey/1000. - recoilDeltaPy/1000., 2) ) ; }
    inline double Prob(double Ex, double Ey) { return Prob_EtMiss( pow(ObsEx - Ex/1000., 2), pow(ObsEy - Ey/1000., 2) ) ; }
    
    const Mosaic::MET_Type METType;
    const Mosaic::MET_nJet nJet;
    const Mosaic::CalibType calibType;
    const double ObsEx, ObsEy, ObsSumEt, ObsEt, ResolScale, Significance;
    const double Sigma1, Sigma2, Fraction;
    const double Minus_Inv_Sigma1_2_2, Minus_Inv_Sigma2_2_2, Coeff_Sigma1, Coeff_Sigma2;
    const double MaxProb, Norm, LogNorm, DeltaThresold2, ProbScale;
  }; // ResolModel_ETMiss
  
  class ResolModel_Muon {
  public:
    ResolModel_Muon(Mosaic::FourVector vis, double sigma);
    ~ResolModel_Muon();
    
    inline FourVector getShiftedVector_impl(double rSigma_p1){ return FourVector( Px*rSigma_p1, Py*rSigma_p1, Pz*rSigma_p1, sqrt(Constant::MMU2 + P2*rSigma_p1*rSigma_p1), Constant::MMU2 ); }
    inline FourVector getShiftedVector(double r){ return getShiftedVector_impl(  1.0 + r * SigmaOverPt); }
    inline double Prob(double r) { return - 0.5 * r * r - logSigma - Mosaic::Constant::LOG_PI2; }
    
  private:
    const double Px, Py, Pz, E, P2;
    const double Sigma, SigmaOverPt, logSigma;
  }; // ResolModel_Muon

  class ResolModel_Electron {
  public:
    ResolModel_Electron(Mosaic::FourVector vis, double sigma);
    ~ResolModel_Electron();
    
    inline FourVector getShiftedVector_impl(double rSigma_p1, double rSigma_P){ return FourVector( Px*rSigma_P, Py*rSigma_P, Pz*rSigma_P, E*rSigma_p1, Constant::MEL2 ); }
    inline FourVector getShiftedVector(double r){ return getShiftedVector_impl( 1.0 + r * SigmaOverE, sqrt( 1.0 + (2.0*EOverP + r*SigmaOverP)*r*SigmaOverP ) ); }
    inline double Prob(double r) { return - 0.5 * r * r - logSigma - Mosaic::Constant::LOG_PI2; }
    
  private:
    const double Px, Py, Pz, E, EOverP;
    const double Sigma, SigmaOverE, SigmaOverP, logSigma;
  }; // ResolModel_Electron
  
  class ResolModel_Tau {
  public:
    ResolModel_Tau(Mosaic::FourVector vis, double sigma);
    ~ResolModel_Tau();
    
    inline FourVector getShiftedVector_impl(double rSigma_p1, double rSigma_P){ return FourVector( Px*rSigma_P, Py*rSigma_P, Pz*rSigma_P, E*rSigma_p1, Mass2 ); }
    inline FourVector getShiftedVector(double r){ return getShiftedVector_impl( 1.0 + r * SigmaOverE, sqrt( 1.0 + (2.0*EOverP + r*SigmaOverP)*r*SigmaOverP ) ); }
    inline double Prob(double r) { return - 0.5 * r * r - logSigma - Mosaic::Constant::LOG_PI2; }
    
  private:
    const double Px, Py, Pz, E, EOverP, Mass2;
    const double Sigma, SigmaOverE, SigmaOverP, logSigma;
  }; // ResolModel_Tau
  

}// namespace Mosaic

#endif // MOSAIC_RESOLMODEL_H
