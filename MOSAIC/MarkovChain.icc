// Dear emacs, this is -*- c++ -*- 
/*
 * MarkovChain
 * Author : Masahiro Morinaga 
 * Developers : Masahiro Morinaga
 */

#ifndef MOSAIC_MARCOVCHAIN_H
# error Do Not include this file directly, include MarkovChain.h instead
#endif


template <typename Lambda>           void Mosaic::unroller(Mosaic::uint_<0>, Lambda lmd) { MOSAIC_UNUSED_VARIABLE(lmd); }
template <size_t N, typename Lambda> void Mosaic::unroller(Mosaic::uint_<N>, Lambda lmd) { lmd(); unroller(Mosaic::uint_<N-1>(), lmd); }


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ MarkovChain ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::MarkovChain( std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> seed, Mosaic::InputManager *InputManager, Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pDFManager ) :
  LLCandidate( 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1e+10, -1e+10 )
{
  pdfManager   = pDFManager;
  std::seed_seq seq( std::begin(seed), std::end(seed) );
  Engine       = new std::mt19937( seq );
  Uniform      = new std::uniform_real_distribution<double> ( 0.0, 1.0 ) ;
  LLs.clear();
  //std::vector< Mosaic::SamplingPoint > (LLs).swap(LLs); 
  
  
  
  LLs.reserve( Mosaic::Constant::N_MIN_STEP_PerChain * 2 );
  std::vector< std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs1{
    { Mosaic::AlgorithmManager::get_seed_seq(*Engine, *Uniform), Mosaic::AlgorithmManager::get_seed_seq(*Engine, *Uniform), Mosaic::AlgorithmManager::get_seed_seq(*Engine, *Uniform), Mosaic::AlgorithmManager::get_seed_seq(*Engine, *Uniform) }
  };
  std::vector< std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs2{
    { Mosaic::AlgorithmManager::get_seed_seq(*Engine, *Uniform), Mosaic::AlgorithmManager::get_seed_seq(*Engine, *Uniform), Mosaic::AlgorithmManager::get_seed_seq(*Engine, *Uniform), Mosaic::AlgorithmManager::get_seed_seq(*Engine, *Uniform) }
  };
  
  
  Tau1     = new MCMCTau1(UniRndm(),  UniRndm() * Mosaic::Constant::MTAU, UniRndm() * Mosaic::Constant::PI2, InputManager->vis1.IsEle() ? Mosaic::Constant::MEL : Mosaic::Constant::MMU, InputManager->Vis1().e(), seqs1 );
  Tau2     = new MCMCTau2(UniRndm(),  UniRndm() * Mosaic::Constant::MTAU, UniRndm() * Mosaic::Constant::PI2, InputManager->vis2.IsEle() ? Mosaic::Constant::MEL : Mosaic::Constant::MMU, InputManager->Vis2().e(), seqs2 );
  
  const auto recoilJets = InputManager->Get_RecoilJets();
  std::vector< std::array<std::uint_least32_t, Mosaic::Constant::SeedLength> > seqs_forJets; seqs_forJets.clear();
  for(int i = 0, size = recoilJets->size(); i < size; ++i)
    seqs_forJets.emplace_back(  Mosaic::AlgorithmManager::get_seed_seq( *Engine, *Uniform) );
  recoilSystem = new Mosaic::MCMCRecoilSystem( recoilJets, seqs_forJets );
  
  nStep    = nButStep = nOKStep  = nAccept  = 0;
  //LLCandidate = std::move( Mosaic::SamplingPoint( 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1e+10, -1e+10 ) );
}
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::~MarkovChain(){
  LLs.clear();
  //std::vector< Mosaic::SamplingPoint >(LLs).swap(LLs);
  delete Tau1;
  delete Tau2;
  delete Engine;
  delete Uniform;
  delete recoilSystem;
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
template<size_t N_LOOP> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::process( Mosaic::uint_<N_LOOP> ){ unroller( Mosaic::uint_<N_LOOP>(), [this]() { process_impl(); } ); }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::process_impl(){
  Sampling( LLHType() );
  auto&& LLPropose = pdfManager->Prob(Tau1, Tau2, recoilSystem, LLHType());
  //std::cout <<" Mass / Prob " << LLPropose.mass() << " / " << LLPropose.prob() << std::endl;
  ++nStep;
  if( LLPropose.prob() <= -2e+10 ||  LLPropose.mass() != LLPropose.mass() ){ ++nButStep;  return; }
  ++nOKStep;
  nButStep = 0;
  //if ( nAccept == 0 ) 
  //std::cout <<" Proposed Prob is " << LLPropose.prob() << "( "<< LLPropose.probME() << " and " << LLPropose.probMET() << " and " << LLPropose.probResol() << " ) " << std::endl; 
  
  //if ( nAccept == 1 ) { std::cout <<"-------------------------------------------" << std::endl; }
  if (  UniRndm()   <=  exp( LLPropose.prob() - LLCandidate.prob() ) ){
    Update( LLHType() );
    LLCandidate = std::move(LLPropose);
    ++nAccept;
  }
  LLs.emplace_back( Mosaic::SamplingPoint(LLCandidate.x1(), LLCandidate.x2(), LLCandidate.px_mis1(),  LLCandidate.py_mis1(),  LLCandidate.pz_mis1(),  LLCandidate.px_mis2(),  LLCandidate.py_mis2(),  LLCandidate.pz_mis2(), 
                                          LLCandidate.mVis(), LLCandidate.mass(), exp(LLCandidate.prob())) );
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::init(){
  Mosaic::SamplingPoint tmp_cadidate{1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1e+10, -1e+10 };
  //std::cout <<"-------------------------------------------------------------------------"<<std::endl;
  bool isOK = false;
  for(int iInit = 0; iInit < Mosaic::Constant::N_InitTry; ++iInit){
    ReInit( LLHType() );
    auto LLPropose = pdfManager->Prob(Tau1, Tau2, recoilSystem, LLHType());
    //std::cout <<" Mass is " << LLPropose.mass() << "GeV / Prob is " << LLPropose.prob() <<"  ----------------->  " << LLPropose.probMET() << "(MET) * " << LLPropose.probME() <<"(ME)" << std::endl;
    if( LLPropose.prob() > -1e+10 &&  LLPropose.mass() == LLPropose.mass() ){
      //tmp_cadidate = LLPropose;
      
      if( ( LLPropose.prob() > Mosaic::Constant::InitalValueThresoldForSamplingLow ) && ( LLPropose.prob() < Mosaic::Constant::InitalValueThresoldForSamplingHigh ) ){ 
        Update( LLHType() );
        LLCandidate = std::move(LLPropose);
        isOK = true;
        break;
      }else if( tmp_cadidate.prob() < LLPropose.prob() ) {
        tmp_cadidate = std::move( LLPropose );
      }
    }
  }
  if( ! isOK ) {
    if( tmp_cadidate.mass() > 0.0 ) LLCandidate = std::move( tmp_cadidate );
  }
  
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Sampling( Mosaic::LikelihoodType::Default ){ Tau1->Sampling(); Tau2->Sampling(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Update( Mosaic::LikelihoodType::Default ){ Tau1->Update(); Tau2->Update(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::ReInit( Mosaic::LikelihoodType::Default ){ Tau1->ReInit(); Tau2->ReInit(); }

#ifdef MOSAIC_USE_DEVELOP

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Sampling( Mosaic::LikelihoodType::MEOnly ){ Tau1->Sampling(); Tau2->Sampling(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Update( Mosaic::LikelihoodType::MEOnly ){ Tau1->Update(); Tau2->Update(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::ReInit( Mosaic::LikelihoodType::MEOnly ){ Tau1->ReInit(); Tau2->ReInit(); }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Sampling( Mosaic::LikelihoodType::METOnly ){ Tau1->Sampling(); Tau2->Sampling(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Update( Mosaic::LikelihoodType::METOnly ){ Tau1->Update(); Tau2->Update(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::ReInit( Mosaic::LikelihoodType::METOnly ){ Tau1->ReInit(); Tau2->ReInit(); }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Sampling( Mosaic::LikelihoodType::PlusVisResoltion ){ Tau1->Sampling(); Tau2->Sampling(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Sampling( Mosaic::LikelihoodType::RecoilSystem ){ Tau1->Sampling(); Tau2->Sampling(); recoilSystem->Sampling(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Sampling( Mosaic::LikelihoodType::PartDF_LHAPDF ){ Tau1->Sampling(); Tau2->Sampling(); }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Update( Mosaic::LikelihoodType::PlusVisResoltion ){ Tau1->Update(); Tau2->Update(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Update( Mosaic::LikelihoodType::RecoilSystem ){ Tau1->Update(); Tau2->Update(); recoilSystem->Update(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Update( Mosaic::LikelihoodType::PartDF_LHAPDF ){ Tau1->Update(); Tau2->Update(); }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::ReInit( Mosaic::LikelihoodType::PlusVisResoltion ){ Tau1->ReInit(); Tau2->ReInit(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::ReInit( Mosaic::LikelihoodType::RecoilSystem ){ Tau1->ReInit(); Tau2->ReInit(); recoilSystem->ReInit(); }
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::ReInit( Mosaic::LikelihoodType::PartDF_LHAPDF ){ Tau1->ReInit(); Tau2->ReInit(); }

#endif


  
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
int    Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Get_nAccept()   const { return nAccept; }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
bool   Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::IsNotEmpty()    const { 
  //std::cout <<" nStep / nButStep / nOKStep / nAccept : " <<  nStep << " / " << nButStep << " / " << nOKStep << " / "<< nAccept << std::endl;
  return nAccept > 0; }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
void   Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::Clear()               { 
  std::vector< Mosaic::SamplingPoint >( std::move( LLs ) ).swap(LLs);
  LLs.clear(); 
  nStep = nButStep = nOKStep = nAccept = 0;
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
double Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::UniRndm()       const { return (*Uniform)(*Engine); }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
double Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::GetAccepRatet() const { return double(nAccept)/double(nOKStep); }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
std::vector< Mosaic::SamplingPoint >&& Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::results( Mosaic::RunningMode::Nominal ){
  int s = LLs.size();
  std::array<double, 6> BurnIns { { 0.01, 0.10, 0.20, 0.30 } };
  std::vector<double> ESSs;
  for(auto& nBurnIn : BurnIns) ESSs.push_back( Mosaic::AlgorithmManager::ESS(LLs, nBurnIn * s, s) );
  int OptBurnIn = BurnIns.at( std::distance(std::begin(ESSs), std::max_element(std::begin(ESSs), std::end(ESSs)) ) ) * s;
  LLs.erase( std::begin(LLs), std::begin(LLs)+OptBurnIn );
  return std::move( LLs );
}
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode> 
std::vector< Mosaic::SamplingPoint >&& Mosaic::MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>::results( Mosaic::RunningMode::Fast ){
  int OptBurnIn = int(LLs.size() * 0.25);
  //std::cout <<" LLs.size()   is " << LLs.size() << std::endl;
  LLs.erase( std::begin(LLs), std::begin(LLs)+OptBurnIn );
  //std::cout <<"   LLs.size() is " << LLs.size() << std::endl;
  return std::move( LLs );
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ MarkovChainSet ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::MarkovChainSet(Mosaic::InputManager *InputManager, Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pDFManager ) {
  Chains         = new std::array<MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode> *, N_CHAIN>;
  setChain(InputManager, pDFManager, Mosaic::uint_<0>() );
  m_isConverge = false;
  nCurrentStep = 0;
}
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::~MarkovChainSet(){ 
  clear( Mosaic::uint_<0>() ); 
  delete Chains; 
}
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::clear( Mosaic::uint_<N_CHAIN> ){ ; }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
template<size_t I>  
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::clear( Mosaic::uint_<I> ) { 
  delete std::get<I>( *Chains ); 
  clear( Mosaic::uint_<I+1>() ); 
} 

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN> 
template<size_t I> 
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::setChain(Mosaic::InputManager *InputManager, Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pDFManager, Mosaic::uint_<I>){
  std::get< I >(*Chains) = new MarkovChain<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode>( InputManager->get_seed_seq(), InputManager, pDFManager);
  setChain(InputManager, pDFManager, Mosaic::uint_<I+1>() );
}
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
template<size_t I>  
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::isNotEmpty_impl( Mosaic::uint_<I> ) { 
  m_isConverge *= std::get<I>( *Chains )->IsNotEmpty(); 
  isNotEmpty_impl( Mosaic::uint_<I+1>() ); 
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
template<size_t I>  
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::results( Mosaic::uint_<I>, std::vector< Mosaic::SamplingPoint>& ret){ 
  auto&& r = std::get<I>( *Chains )->results( RunMode() );
  ret.insert( std::end( ret ), std::make_move_iterator( std::begin(r) ), std::make_move_iterator( std::end(r) ) );
  std::get<I>( *Chains )->Clear();
  results( Mosaic::uint_<I+1>(), ret ); 
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN> 
template<size_t I>  
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::makeGelmanInput( Mosaic::uint_<I>,  std::array< std::tuple<double, double, int>, N_CHAIN >& mean_vars ){
  std::get<I>(mean_vars) = std::move( Mosaic::AlgorithmManager::doGewekeCheck( std::get<I>( *Chains )->LLs ) );
  //std::cout << " mean / var is " << std::get<0>(std::get<I>(mean_vars)) <<  " / " << std::get<1>(std::get<I>(mean_vars)) << std::endl;
  makeGelmanInput( Mosaic::uint_<I+1>(), mean_vars );
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
template<size_t I> 
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::process_addt_impl( Mosaic::uint_<I> ){ 
  unroller( Mosaic::uint_< Mosaic::Constant::N_Additional_Step_250   >(), [&]() { std::get<I>( *Chains )->process( Mosaic::uint_<250>() ); } ); process_addt_impl( Mosaic::uint_<I+1>() ); 
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
template<size_t I>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::process_main_impl( Mosaic::uint_<I> ){ 
  unroller( Mosaic::uint_< Mosaic::Constant::N_MIN_STEP_PerChain_250 >(), [&]() { std::get<I>( *Chains )->process( Mosaic::uint_<250>() ); } ); process_main_impl( Mosaic::uint_<I+1>() ); 
}


template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::setChain(Mosaic::InputManager *InputManager, Mosaic::PDFManager<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2> * pDFManager, Mosaic::uint_<N_CHAIN>){ 
  MOSAIC_UNUSED_VARIABLE(InputManager); MOSAIC_UNUSED_VARIABLE(pDFManager) ; 
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::isNotEmpty_impl( Mosaic::uint_<N_CHAIN> ) { ; }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::results( Mosaic::uint_<N_CHAIN>, std::vector< Mosaic::SamplingPoint>& ret){ 
  MOSAIC_UNUSED_VARIABLE(ret); 
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::makeGelmanInput( Mosaic::uint_<N_CHAIN>,  std::array< std::tuple<double, double, int>, N_CHAIN >& mean_vars){ 
  MOSAIC_UNUSED_VARIABLE(mean_vars); 
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
std::vector< Mosaic::SamplingPoint > Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::Results( ){
  std::vector< Mosaic::SamplingPoint > ret;
  ret.reserve( N_CHAIN  );
  results( Mosaic::uint_<0>(), ret ); 
  ret.erase(std::remove_if(std::begin(ret), std::end(ret), [](const Mosaic::SamplingPoint& p) -> bool { return p.mass() < 0.0; } ), std::end(ret));
  //std::cout <<" Results Size is "<< ret.size() << std::endl;
  return std::move( ret );
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
double Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::IsConverge()    {
  std::array< std::tuple<double, double, int>, N_CHAIN > mean_vars ;
  makeGelmanInput( Mosaic::uint_<0>(), mean_vars );
  return Mosaic::AlgorithmManager::doGelmanRubin<N_CHAIN>( mean_vars );
}
    
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::process_addt_impl( Mosaic::uint_<N_CHAIN> ){ ; }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::process_main_impl( Mosaic::uint_<N_CHAIN> ){ ; }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
bool Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::process_additional(){ 
  double current_R = IsConverge() ;
  if( current_R < 0.01 ) return true;
  for(int iAddtionalCheck = 0; iAddtionalCheck < Mosaic::Constant::N_Additional_Check; ++iAddtionalCheck){
    process_addt_impl( Mosaic::uint_<0>() );
    current_R =  IsConverge() ;
    if( current_R < 0.01 ) return true;
  }
  if( current_R < 0.20 ) return true;
  return false;
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
bool Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::process_main(){ 
  process_main_impl( Mosaic::uint_<0>() );
  m_isConverge = true;
  isNotEmpty_impl( Mosaic::uint_<0>() );
  return m_isConverge;
}

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::init_impl( Mosaic::uint_<N_CHAIN> ){  ; }

template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
template<size_t I>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::init_impl( Mosaic::uint_<I> ){ 
  std::get<I>( *Chains )->init();
  init_impl( Mosaic::uint_<I+1>() ); 
}
template<typename PDFDecayME, typename PDF1, typename PDF2, typename ResolModel_Vis1, typename ResolModel_Vis2, typename MCMCTau1, typename MCMCTau2, typename LLHType, typename RunMode, size_t N_CHAIN>
void Mosaic::MarkovChainSet<PDFDecayME, PDF1, PDF2, ResolModel_Vis1, ResolModel_Vis2, MCMCTau1, MCMCTau2, LLHType, RunMode, N_CHAIN>::init(){ 
  init_impl( Mosaic::uint_<0>() );
}
