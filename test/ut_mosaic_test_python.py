#!/usr/bin/env python
#
# $Id$
#
#

## C/C++ style main function
import sys, math

def main():
    """
    Main function (c++ style)
    """

    # The name of the application
    import os
    APP_NAME = os.path.basename(__file__).rstrip('.py')
    print APP_NAME
    # Set up a logger object
    import logging
    logger = logging.getLogger(APP_NAME)
    logger.setLevel(logging.INFO)
    hdlr = logging.StreamHandler(sys.stdout)
    frmt = logging.Formatter('%(name)-14s%(levelname)8s %(message)s')
    hdlr.setFormatter(frmt)
    logger.addHandler(hdlr)

    # Setup the environment
    import ROOT
    if ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C'):
        logger.error("Couldn't load the RootCore packages")
        return 1
    
    if ROOT.xAOD.Init(APP_NAME).isFailure():
        logger.error('Failed to call xAOD::Init(...)')
        return 1
    
    # MissingMassTool import and setup
    from ROOT import xMOSAICalculator
    mosaic = xMOSAICalculator('mosaic')

    # Sanity checks of the properties - no need to setup since 
    # default values are specified in the c++ constructor
    mosaic.setProperty('std::string')('CalibType',        'MC15_50ns_v1').ignore()
    mosaic.setProperty('std::string')('METType',          'MET_MPT').ignore()
    mosaic.setProperty('std::string')('LikelihoodType',   'Default').ignore()
    mosaic.setProperty('std::string')('JVTKey',           'Jvt').ignore()
    mosaic.setProperty('std::string')('RunMode',          'Nominal').ignore()
    mosaic.setProperty(bool)('UseBetter_MET',             True).ignore()
    

    sc = mosaic.initialize()
    if sc.isFailure():
        logger.error('Failed to initialize the tool')
        return 1
    
    # Create a transient tree from a test file
    InputFileName = "/afs/cern.ch/atlas/project/PAT/xAODs/r6630/mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.recon.AOD.e3601_s2576_s2132_r6630_tid05358802_00/AOD.05358802._004299.pool.root.1"
    InputFileName = '/home/morinaga/xAOD/p2419/mc15_13TeV:mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_HIGG4D2.e3601_s2576_s2132_r6765_r6282_p2419/DAOD_HIGG4D2.06502362._000001.pool.root.1 '
    ifile = ROOT.TFile.Open(InputFileName, 'read')
    if not ifile:
        logger.error("Could not open input file: %s" % InputFileName)
        return 1
    
    tree = ROOT.xAOD.MakeTransientTree(ifile)
    if not tree:
        logger.error("Failed to make transient tree from file %s" % InputFileName)
        return 1
    logger.info('All tests succeeded')
    #tree.Show(1)
    n_Events_Process = 0

    etaSel = lambda feta : feta < 1.37 or (feta > 1.57 and feta < 2.47)
    for iev, evt in enumerate(tree):
        if n_Events_Process > 10: break
        
        logger.info('## Event %s ##' % iev)

        els, mus, taus = [], [], []
        
        for iObj, electron in enumerate(evt.Electrons):
            if not math.fabs(electron.charge()) == 1                            : continue
            if electron.pt()/1000.  < 20.0                                      : continue
            if not etaSel( math.fabs(electron.eta()) )                          : continue
            if not electron.passSelection( ROOT.xAOD.EgammaParameters.LHTight ) : continue
            els.append(iObj)
            pass
        
        for iObj, muon in enumerate(evt.Muons):
            if not (math.fabs(muon.charge()) == 1)    : continue
            if muon.pt()/1000.  < 20.0                : continue
            if math.fabs(muon.eta()) > 2.4            : continue
            if muon.quality() < ROOT.xAOD.Muon.Medium : continue
            if not muon.passesIDCuts()                : continue
            mus.append(iObj)
            pass
        for iObj, Tau in enumerate(evt.TauJets):
            if not (math.fabs(Tau.charge()) == 1)                        : continue
            if Tau.pt()/1000 < 20.0                                      : continue
            if not (Tau.nTracks() == 1 or Tau.nTracks() == 3 )           : continue
            if not etaSel( math.fabs(Tau.eta()))                         : continue
            if not Tau.isTau(ROOT.xAOD.TauJetParameters.JetBDTSigMedium) : continue
            taus.append(iObj);
            pass
        if not ( len(taus) + len(els) + len(mus) == 2 ) : continue
     
        tauObj1 = None
        tauObj2 = None
        if len(taus) == 0 :
            if    len(els) == 2 and len(mus) == 0 : tauObj1, tauObj1 = evt.Electrons[els[0]], evt.Electrons[els[1]]
            elif  len(els) == 1 and len(mus) == 1 : tauObj1, tauObj1 = evt.Electrons[els[0]], evt.Muons[mus[0]]
            elif  len(els) == 0 and len(mus) == 2 : tauObj1, tauObj1 = evt.Muons[els[0]], evt.Muons[mus[1]]
            pass
        elif len(taus) == 1:
            tauObj1 = evt.TauJets[taus[0]]
            if   len(els) == 1 and len(mus) == 0 : tauObj2 = evt.Electrons[els[0]]
            elif len(els) == 0 and len(mus) == 1 : tauObj2 = evt.Muons[mus[0]]
            pass
        elif len(taus) == 2 : tauObj1, tauObj2 = evt.TauJets[taus[0]], evt.TauJets[taus[1]]
     
        n_Events_Process += 1
        Jets = evt.AntiKt4LCTopoJets
        MET = list(evt.MET_Reference_AntiKt4LCTopo)[-1]
        mosaic.SetObjects(tauObj1, tauObj2, MET, Jets ).ignore()
        if mosaic.run_Z().isSuccess() : logger.info(' ---------------------------------> MOSAIC Z Returns  %s GeV' % (mosaic.mass_Z()/1000.) )
        if mosaic.run_H().isSuccess() : logger.info(' ---------------------------------> MOSAIC H Returns  %s GeV' % (mosaic.mass_H()/1000.) )
        if mosaic.run_A().isSuccess() : logger.info(' ---------------------------------> MOSAIC A Returns  %s GeV' % (mosaic.mass_A()/1000.) )
        pass
    
    # Return gracefully
    return 0


if __name__ == '__main__':
    sys.exit(main())


