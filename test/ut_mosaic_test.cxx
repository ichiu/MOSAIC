// System include(s):
#include <memory>
#include <set>
int main() {
  return 0;
}
  /*

// Dear emacs, this is -*- c++ -*-

// System include(s):
#include <memory>
#include <set>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TTree.h>
#include <TSystem.h>

// Core EDM include(s):
#include "AthContainers/AuxElement.h"
#include "AthContainers/DataVector.h"

// EDM includes
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
// Local include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODRootAccess/tools/Message.h"
#include "MOSAIC/xMOSAICalculator.h"

// Error checking macro
#define CHECK( ARG )							\
  do {                                                                  \
    const bool result = ARG;						\
    if(!result) {							\
      ::Error(APP_NAME, "Failed to execute: \"%s\"",			\
	      #ARG );							\
      return 1;								\
    }									\
  } while( false )

int main() {

   // Get the name of the application:
   const char* APP_NAME = "ut_mosaic_test";

   // Initialise the environment:
   RETURN_CHECK( APP_NAME, xAOD::Init( APP_NAME ) );

   // Initialize the tool
   xMOSAICalculator mosaic("mosaic");
   CHECK( mosaic.setProperty("CalibType",        "MC15_50ns_v1" )) ;
   CHECK( mosaic.setProperty("METType",          "MET_MPT")) ;
   CHECK( mosaic.setProperty("LikelihoodType",   "Default" )) ;
   CHECK( mosaic.setProperty("JVTKey",           "Jvt"  )) ;
   CHECK( mosaic.setProperty("RunMode",          "Nominal" )) ;
   CHECK( mosaic.setProperty("UseBetter_MET",     true  )) ;
   
   CHECK( mosaic.initialize() );

   // Connect an input file to the event:
   static const char* InputFileName ="/afs/cern.ch/atlas/project/PAT/xAODs/r6630/mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.recon.AOD.e3601_s2576_s2132_r6630_tid05358802_00/AOD.05358802._004299.pool.root.1";
   std::unique_ptr< ::TFile > ifile( ::TFile::Open( InputFileName, "READ" ) );
   
   if( ! ifile.get() ) {
     ::Error( APP_NAME, XAOD_MESSAGE( "File %s couldn't be opened..." ), InputFileName );
     return 1;
   }

   // Create the TEvent object
   xAOD::TEvent event(xAOD::TEvent::kClassAccess);
   CHECK( event.readFrom(ifile.get()) );

   int n_Events_Process(0);
   Long64_t entries = event.getEntries();
   for (Long64_t entry = 0; entry < entries; entry++) {
     if ( n_Events_Process > 10) break;
     
     event.getEntry(entry);

     // retrieve the EDM objects

     const xAOD::ElectronContainer  * m_Elecs = 0;
     const xAOD::MuonContainer      * m_Muons = 0;
     const xAOD::TauJetContainer    * m_TauJets = 0;
     CHECK(event.retrieve(m_Elecs,    "Electrons"));
     CHECK(event.retrieve(m_Muons,    "Muons"));
     CHECK(event.retrieve(m_TauJets,  "TauJets"));
     std::vector<int> els, mus, taus;
     // Check Objects Selection
     auto etaSel = [](float feta) { return feta < 1.37 || ( feta > 1.52 && feta < 2.47); };
     int iObj = -1;
     for(const auto& electron : *m_Elecs){
       ++iObj;
       if( fabs(electron->charge()) != 1) continue;
       if( electron->pt()/1000.  < 20.0 )   continue;
       if( ! etaSel(fabs(electron->eta()) ) )  continue;
       if( ! electron->passSelection( xAOD::EgammaParameters::SelectionMenu::LHTight ) ) continue;
       els.push_back(iObj);
     }
     iObj = -1;
     for(const auto& muon : *m_Muons){
       ++iObj;
       if( fabs(muon->charge()) != 1) continue;
       if( muon->pt()/1000.  < 20.0 )   continue;
       if( fabs(muon->eta()) > 2.4 )   continue;
       if( muon->quality() < xAOD::Muon::Medium) continue;
       if( ! muon->passesIDCuts() ) continue;
       mus.push_back(iObj);
     }
     iObj = -1;
     for(const auto& Tau : *m_TauJets){
       ++iObj;
       if( fabs(Tau->charge()) != 1) continue;
       if( Tau->pt()/1000 < 20.0 ) continue;
       if( ! (Tau->nTracks() == 1 || Tau->nTracks() == 3 )) continue;
       if( ! etaSel(fabs(Tau->eta())) ) continue;
       if( ! Tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetBDTSigMedium) ) continue;
       taus.push_back(iObj);
     }

     if( taus.size() + els.size() + mus.size() != 2 ) continue;
     
     const xAOD::IParticle * tauObj1 = nullptr;
     const xAOD::IParticle * tauObj2 = nullptr;
     
     if( taus.size() == 0 ){
       if     ( els.size() == 2 && mus.size() == 0 ) {
         tauObj1 = m_Elecs->at( els.at(0) );
         tauObj2 = m_Elecs->at( els.at(1) );
       }else if( els.size() == 1 && mus.size() == 1 ) {
         tauObj1 = m_Elecs->at( els.at(0) );
         tauObj2 = m_Muons->at( mus.at(0) );
       }else if( els.size() == 0 && mus.size() == 2 ) {
         tauObj1 = m_Muons->at( mus.at(0) );
         tauObj2 = m_Muons->at( mus.at(1) );
       }
     }else if( taus.size() == 1){
       tauObj1 = m_TauJets->at(0);
       if     ( els.size() == 1 && mus.size() == 0 ) tauObj2 = m_Elecs->at( els.at(0) );
       else if( els.size() == 0 && mus.size() == 1 ) tauObj2 = m_Muons->at( mus.at(0) );
       
     }else if( taus.size() == 2){
       tauObj1 = m_TauJets->at( taus.at(0) );
       tauObj2 = m_TauJets->at( taus.at(1) );
     }
     if( tauObj1 == nullptr || tauObj2 == nullptr ) continue;
     
     
     const xAOD::JetContainer *m_Jets = nullptr;
     const xAOD::MissingETContainer *m_METs = nullptr;
     CHECK(event.retrieve(m_Jets, "AntiKt4LCTopoJets"));
     CHECK(event.retrieve(m_METs, "MET_Reference_AntiKt4LCTopo"));
     const xAOD::MissingET *m_MET = m_METs->at(m_METs->size() - 1);
     
     ++n_Events_Process;
     
     mosaic.Clear();
     mosaic.SetObjects( tauObj1, tauObj2, m_MET, m_Jets );
     ::Info(APP_NAME, "%d Events processed", (int)entry);
     
     if( mosaic.run_Z().isSuccess() ) ::Info(APP_NAME, "MOSAIC Z returns %f GeV", mosaic.mass_Z()/1000.);
     if( mosaic.run_H().isSuccess() ) ::Info(APP_NAME, "MOSAIC H returns %f GeV", mosaic.mass_H()/1000.);
     if( mosaic.run_A().isSuccess() ) ::Info(APP_NAME, "MOSAIC A returns %f GeV", mosaic.mass_A()/1000.);
   }

   return 0;
}
   */
